# EXECUTABLES -----------------------------------------------------------------
XXD = xxd
AR = ar #zig ar
CC = gcc #zig cc
GDB = gdb

# FLAGS -----------------------------------------------------------------------
CFLAGS = -std=c11 -Wpedantic -D_REENTRANT
debug: CFLAGS += -ggdb -O0
release: CFLAGS += -s -Ofast -DNDEBUG

# LDFLAGS = -rdynamic
# LIBS = -lm -ldl

# PAHTS -----------------------------------------------------------------------
SRC = src
BIN = bin
OBJ = $(BIN)/obj

# VARS ------------------------------------------------------------------------
HEADERS := $(wildcard $(SRC)/*.h)
SOURCES := $(wildcard $(SRC)/aoc_*.c)
# OBJECTS := $(patsubst $(SRC)/aoc_%.c, $(OBJ)/aoc_%.o, $(SOURCES))
BINARIES := $(patsubst $(SRC)/aoc_%.c, $(BIN)/aoc_%, $(SOURCES))

TEST_SOURCES := $(wildcard $(SRC)/*_test.c)
TEST_BINARIES := $(patsubst $(SRC)/%_test.c, $(BIN)/%_test, $(TEST_SOURCES))

# GOALS -----------------------------------------------------------------------

.PHONY: clean # run without files changed

clean:
	rm -rf $(BIN)

$(OBJ)/aoc_%.o: $(SRC)/aoc_%.c $(HEADERS); @mkdir -p $(OBJ)
	$(CC) $(CFLAGS) -c $< -o $@

$(BIN)/aoc_%: $(OBJ)/aoc_%.o $(HEADERS); @mkdir -p $(BIN)
	$(CC) $(CFLAGS) $< -o $@

$(BIN)/%_test: $(SRC)/%_test.c $(HEADERS); @mkdir -p $(BIN)
	$(CC) $(CFLAGS) $< -o $@

debug: $(BINARIES) $(TEST_BINARIES)
release: $(BINARIES) $(TEST_BINARIES)
