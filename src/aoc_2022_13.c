#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

enum msg_type {
  MSG_NUMBER = 0,
  MSG_LIST   = 1,
};

struct msg_packet {
  enum msg_type type;

  union {
    // MSG_NUMBER
    int number;
    // MSG_LIST
    struct {
      int size;
      const struct msg_packet *list;
    };
  };
};

static void print_message(const struct msg_packet *msg) {
  if (msg->type == MSG_NUMBER) {
    printf("%d", msg->number);
    return;
  }
  printf("[");
  for(int i=0; i<msg->size; i++) {
    if (i > 0) {
      printf(",");
    }
    print_message(&msg->list[i]);
  }
  printf("]");
}

static int compare_messages(const struct msg_packet *left,
                            const struct msg_packet *right) {
  if ((left->type == MSG_NUMBER) & (right->type == MSG_NUMBER)) {
    return (left->number < right->number) - (left->number > right->number);
  }
  if (left->type == MSG_NUMBER) {
    return compare_messages(&(struct msg_packet) {
        .type = MSG_LIST,
        .size = 1,
        .list = left,
      }, right);
  }
  if (right->type == MSG_NUMBER) {
    return compare_messages(left, &(struct msg_packet) {
        .type = MSG_LIST,
        .size = 1,
        .list = right,
      });
  }
  for(int i=0; i<left->size; i++) {
    if (i >= right->size) {
      return -1; // right ran out if items
    }
    int res = compare_messages(&left->list[i], &right->list[i]);
    if (res != 0) {
      return res;
    }
  }
  return left->size < right->size; // left ran out if items
}

static int parse_message(const char *str, int max_packets,
                         struct msg_packet packets[static (max_packets)],
                         int parents[static (max_packets)]) {

  int num_packets = 0, parent = -1;
  bool number;

  for(int i=0; str[i] != 0; i++) {
    char c = str[i];

    if (isspace(c) | (c == ',')) {
      number = false;
    } else if (c == '[') {
      number = false;
      if (num_packets >= max_packets) {
          fprintf(stderr, "Error: Exceeded packet limit %d.\n", max_packets);
          return -1;
      }
      packets[num_packets] = (struct msg_packet) {
        .type = MSG_LIST,
      };
      if (parent >= 0) {
        assert(packets[parent].type == MSG_LIST);
        packets[parent].size++;
      }
      parents[num_packets] = parent;
      parent = num_packets++;
    } else if (c == ']') {
      if (parent < 0) {
          fprintf(stderr, "Error: Missing closing bracket.\n");
          return -1;
      }
      number = false;
      parent = parents[parent];
    } else if ((c >= '0') & (c <= '9')) {
      if (number) {
        struct msg_packet *pkg = &packets[num_packets - 1];
        assert(pkg->type == MSG_NUMBER);
        pkg->number = (pkg->number * 10) + (c - '0');
      } else if (num_packets < max_packets) {
        number = true;
        packets[num_packets] = (struct msg_packet) {
          .type = MSG_NUMBER,
          .number = (c - '0'),
        };
        assert(packets[parent].type == MSG_LIST);
        packets[parent].size++;
        parents[num_packets] = parent;
        num_packets++;
      } else {
          fprintf(stderr, "Error: Exceeded packet limit %d.\n", max_packets);
          return -1;
      }
    } else {
      fprintf(stderr, "Error: Invalid message format '%s'.\n", str + i);
      return -1;
    }
  }

  //TODO(micha): improve block
  for(int i=num_packets-1; i>0; i--) {
    int max = parents[i], j = i;
    for(int k=i-1; k>0; k--) {
      if ((parents[k] > max)) {
        max = parents[k];
        j = k;
      }
    }
    if (j < i) {
      {
        int tmp = parents[i];
        parents[i] = parents[j];
        parents[j] = tmp;
        for(int k=i+1; k<num_packets; k++) {
          if(parents[k] == i) {
            parents[k] = j;
          } else if(parents[k] == j) {
            parents[k] = i;
          }
        }
      }
      {
        struct msg_packet tmp = packets[i];
        packets[i] = packets[j];
        packets[j] = tmp;
      }
    }
    if (packets[parents[i]].type == MSG_LIST) {
      packets[parents[i]].list = &packets[i];
    };
  }

  return num_packets;
}

static int read_message(int max_packets,
                        struct msg_packet packets[static (max_packets)],
                        int parents[static (max_packets)]) {
  static const int N = 512;
  char buffer[N+1];
  buffer[N] = 0;
  int read = scanf("%511s\n", buffer);
  if (read == EOF) {
    return 0;
  }
  if (read == 0) {
    fprintf(stderr, "Error: Expected reading message.\n");
    return -1;
  }
  return parse_message(buffer, max_packets, packets, parents);
}

void *CTX;
static int compare_messages_user(const void *a, const void *b) {
  struct msg_packet *packets = CTX;
  const int l = *(const int*) a;
  const int r = *(const int*) b;
  return -compare_messages(&packets[l], &packets[r]);
}

int run_01(int max_packets,
           struct msg_packet packets[static (max_packets)],
           int parents[static (max_packets)]) {

  int score = 0, pair = 0;

  int num_packets[2];
  while ((num_packets[0] = read_message(max_packets, packets, parents)) != 0) {
    if (num_packets[0] < 0) {
      return EXIT_FAILURE;
    }

    num_packets[1] = read_message(max_packets - num_packets[0], packets + num_packets[0], parents);
    if (num_packets[1] < 0) {
      return EXIT_FAILURE;
    }
    if (num_packets[1] == 0) {
      fprintf(stderr, "Error: Expected messages in pairs.\n");
      return EXIT_FAILURE;
    }

    pair++;
    int cmp = compare_messages(packets, &packets[num_packets[0]]);
    score += pair * (cmp > 0);
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int run_02(int max_packets,
           struct msg_packet packets[static (max_packets)],
           int parents[static (max_packets)],
           int max_messages,
           int messages[static (1+max_messages)]) {

  int num_messages = 0;
  messages[num_messages] = 0;

  int num_packets;
  while ((num_packets = read_message(max_packets - messages[num_messages],
                                    packets + messages[num_messages],
                                    parents)) != 0) {
    if (num_packets < 0) {
      return EXIT_FAILURE;
    }
    if (num_messages >= max_messages) {
      fprintf(stderr, "ERROR: Message limit exceeded %d\n", max_messages);
      return EXIT_FAILURE;
    }
    ++num_messages;
    messages[num_messages] = messages[num_messages-1] + num_packets;
  }

  if (num_messages+2 >= max_messages) {
      fprintf(stderr, "ERROR: Message limit exceeded %d\n", max_messages);
      return EXIT_FAILURE;
  }

  static const char* dividers[2] = { "[[2]]", "[[6]]" };
  for(int i=0; i<2; i++) {
    num_packets = parse_message(dividers[i], max_packets - messages[num_messages],
                                      packets + messages[num_messages],
                                      parents);
    if (num_packets < 0) {
      return EXIT_FAILURE;
    }
    ++num_messages;
    messages[num_messages] = messages[num_messages-1] + num_packets;
  }

  // sort and find dividers
  int values[2] = { messages[num_messages - 2], messages[num_messages - 1] };

  CTX = packets; // NOTE(micha): `qsort_r/_s` are terrible cross platform
  qsort(messages, num_messages, sizeof(int),
          compare_messages_user);

  int score = 1;
  for(int i=0; i<num_messages; i++) {
    if ((messages[i] == values[0]) | (messages[i] == values[1])) {
      score *= (i+1);
    }
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_13(bool first) {
  const int MAX_MESSAGES = 303;
  const int MAX_PACKETS = 32*1024;
  struct msg_packet *packets/*[MAX_PACKETS]*/; // 36KB
  int *parents/*[MAX_PACKETS]*/;               // 12KB
  int *messages/*[1+MAX_MESSAGES]*/;           // ~12KB
  { // free'd at EXIT
    void *arena = malloc(MAX_PACKETS * (sizeof(struct msg_packet) + sizeof(int))
                       + (MAX_MESSAGES + 1) * sizeof(int));
    packets = (struct msg_packet*) arena;
    parents = (int*) (packets + MAX_PACKETS);
    messages = (int*) (parents + MAX_PACKETS);
  }

  return first ? run_01(MAX_PACKETS, packets, parents)
               : run_02(MAX_PACKETS, packets, parents, MAX_MESSAGES, messages);
}

#define AOC_RUN_DAY aoc_run_2022_day_13
#include "main.c"
