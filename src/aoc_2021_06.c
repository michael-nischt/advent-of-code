#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool read_spawn_counts(int span_latency, long spawn_counts[span_latency]) {
  memset(spawn_counts, 0, sizeof(long) * span_latency);

  unsigned int number;
  char sep;
  while (scanf("%ud", &number) == 1) {
    if (number > span_latency) {
      fprintf(stderr, "Error: spawn latency limit exceeded\n");
      return false;
    }
    spawn_counts[number]++;
    if (feof(stdin)) {
      return true;
    }
    if ((scanf("%c", &sep) != 1) || (sep != ',')) {
      if (isspace(sep)) {
        break;
      }
      fprintf(stderr, "Error: spawn count sequence format error\n");
      return false;
    }
  }

  return true;
}

static int run(int days, int spawn_rate, int spawn_delay, long spawn_counts[spawn_rate + spawn_delay]) {
  int span_latency = spawn_rate + spawn_delay;
  if (!read_spawn_counts(span_latency, spawn_counts)) {
    return EXIT_FAILURE;
  }

  for (int off = 0, day = 1; day <= days; day++) {
    long create = spawn_counts[off % span_latency];
    spawn_counts[off % span_latency] = 0;
    spawn_counts[(off + spawn_rate) % span_latency] += create;
    spawn_counts[(off + span_latency) % span_latency] += create;
    off++;
  }

  long sum = 0;
  for (int i = 0; i < span_latency; i++) {
    sum += spawn_counts[i];
  }
  printf("%ld\n", sum);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_06(bool first) {
  const int SPAWN_RATE = 7, SPAWN_DELAY = 2;
  long counts[SPAWN_RATE + SPAWN_DELAY]; // 72 B
  return first ? run(80 /*days*/, SPAWN_RATE, SPAWN_DELAY, counts) : run(256 /*days*/, SPAWN_RATE, SPAWN_DELAY, counts);
}

#define AOC_RUN_DAY aoc_run_2021_day_06
#include "main.c"
