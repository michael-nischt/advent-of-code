#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HEAP_IMPL
#include "heap.h"

typedef struct borrow_size {
  int hallway, rooms, depth;
} borrow_size_t;

typedef struct borrow {
  borrow_size_t size;
  int *entrances; /*[rooms]*/ // sorted indices into the hallway
  int *costs;                 /*[rooms]*/
} borrow_t;

typedef struct cost {
  long energy;
  long distance; // distance to move each in the hallway to the matching home entrance
} cost_t;

inline static cost_t *get_cost_ptr(void *mem) { return mem; }

inline static uint8_t *get_map_ptr(void *mem) {
  uint8_t *map = mem;
  map += sizeof(cost_t);
  return map;
}

inline static int cmp_costs(const cost_t *a, const cost_t *b) {
  // adding the distance is a heuristic which doesn't invalidate the solution
  // but partially sorts the entries in the priority queue to skip duplicate states
  return 10 * ((a->energy > b->energy) - (a->energy < b->energy)) +
         ((a->distance > b->distance) - (a->distance < b->distance));
}
inline static int cmp_cost_ptrs(const void *a, const void *b) { return cmp_costs(a, b); }

inline static int count_borrow_locations(const borrow_size_t *size) {
  return size->hallway + size->rooms * size->depth;
}

inline static uint8_t get_borrow_hallway_at(const borrow_size_t *size, const uint8_t *map, int index) {
  return map[index];
}
inline static void set_borrow_hallway_at(const borrow_size_t *size, uint8_t *map, int index, uint8_t type) {
  map[index] = type;
}

inline static uint8_t get_borrow_room_at(const borrow_size_t *size, const uint8_t *map, uint8_t room, int depth) {
  return map[size->hallway + size->depth * room + depth];
}
inline static void set_borrow_room_at(const borrow_size_t *size, uint8_t *map, uint8_t room, int depth, uint8_t type) {
  map[size->hallway + size->depth * room + depth] = type;
}

static int count_free_at_borrow_room(const borrow_size_t *size, const uint8_t *map, uint8_t room) {
  int free;
  for (free = 0; free < size->depth; free++) {
    if (get_borrow_room_at(size, map, room, free) != 0) {
      break;
    }
  }
  return free;
}
static int count_home_at_borrow_room(const borrow_size_t *size, const uint8_t *map, uint8_t room) {
  int depth = size->depth, type = room + 1, valid;
  for (valid = 0; valid < size->depth; valid++) {
    if (get_borrow_room_at(size, map, room, --depth) != type) {
      break;
    }
  }
  return valid;
}

inline static bool is_borrow_size_valid(const borrow_size_t *size) {
  return (size->rooms > 0) & (size->depth > 0) | (size->hallway > 0);
}
static bool is_borrow_valid(const borrow_t *borrow) {
  if (!is_borrow_size_valid(&borrow->size)) {
    return false;
  }

  // validate entrances
  for (int room = 0; room < borrow->size.rooms; room++) {
    // check if entrances are out of bounds
    if ((borrow->entrances[room] < 0) | (borrow->entrances[room] >= borrow->size.hallway)) {
      return false;
    }
    // check if entrances are sorted
    if (room > 0) {
      if (borrow->entrances[room - 1] >= borrow->entrances[room]) {
        return false;
      }
    }
  }

  // validate costs
  for (int room = 0; room < borrow->size.rooms; room++) {
    if (borrow->costs[room] <= 0) {
      return false;
    }
  }

  return true;
}
static bool is_borrow_map_valid(const borrow_t *borrow, const uint8_t *map) {
  // entrances must be free
  for (int room = 0; room < borrow->size.rooms; room++) {
    if (get_borrow_hallway_at(&borrow->size, map, borrow->entrances[room]) != 0) {
      return false; // not allowed to wait at an entrance
    }
  }

  // all slots above an empty room slot must be empty as well
  for (int room = 0; room < borrow->size.rooms; room++) {
    int resident;
    for (resident = 0; resident < borrow->size.depth; resident++) {
      if (get_borrow_room_at(&borrow->size, map, room, borrow->size.depth - resident - 1) == 0) {
        break;
      }
    }
    for (int d = resident; d < borrow->size.depth; d++) {
      if (get_borrow_room_at(&borrow->size, map, room, borrow->size.depth - resident - 1) != 0) {
        return false;
      }
    }
  }

  // account for all types and identify invalid
  int counts[borrow->size.rooms];
  for (int room = 0; room < borrow->size.rooms; room++) {
    counts[room] = 0;
  }
  for (int h = 0; h < borrow->size.hallway; h++) {
    int entry = get_borrow_hallway_at(&borrow->size, map, h);
    if (entry-- > 0) {
      if (entry >= borrow->size.rooms) {
        return false; // invalid type
      }
      counts[entry]++;
    }
  }
  for (int room = 0; room < borrow->size.rooms; room++) {
    for (int depth = 0; depth < borrow->size.depth; depth++) {
      int entry = get_borrow_room_at(&borrow->size, map, room, depth);
      if (entry-- > 0) {
        if (entry >= borrow->size.rooms) {
          return false; // invalid type
        }
        counts[entry]++;
      }
    }
  }
  for (int room = 0; room < borrow->size.rooms; room++) {
    if (counts[room] != borrow->size.depth) {
      return false;
    }
  }

  return true;
}

static void print_borrow_map(const borrow_t *borrow, const uint8_t *map) {
  // top wall
  for (int i = -2; i < borrow->size.hallway; i++) {
    printf("#");
  }
  printf("\n");
  // hallway
  printf("#");
  for (int i = 0; i < borrow->size.hallway; i++) {
    uint8_t type = get_borrow_hallway_at(&borrow->size, map, i);
    if (type-- == 0) {
      printf(".");
    } else {
      printf("%c", 'A' + type);
    }
  }
  printf("#\n");
  // rooms
  int from = borrow->entrances[0];
  int to = borrow->entrances[borrow->size.rooms - 1];
  for (int d = 0; d < borrow->size.depth; d++) {
    printf(d == 0 ? "#" : " ");

    for (int i = 1; i < from; i++) {
      printf(d == 0 ? "#" : " ");
    }
    printf("#");

    int room = 0;
    room = borrow->size.rooms > room ? room : -1;
    for (int i = from; i <= to; i++) {
      if (i == borrow->entrances[room]) {
        uint8_t type = get_borrow_room_at(&borrow->size, map, room++, d);
        if (type-- == 0) {
          printf(".");
        } else {
          printf("%c", 'A' + type);
        }
      } else {
        printf("#");
      }
    }

    printf("#");
    for (int i = borrow->size.hallway - to; (d == 0) & (i > 1); i--) {
      printf("#");
    }
    printf("\n");
  }
  // bottom
  for (int i = 0; i < from; i++) {
    printf(" ");
  }
  for (int i = 2 + to - from; i >= 0; i--) {
    printf("#");
  }
  printf("\n");
}

static bool is_borrow_solution(const borrow_size_t *size, const uint8_t *map) {
  for (uint8_t room = 0; room < size->rooms; room++) {
    for (int depth = 0; depth < size->depth; depth++) {
      int type = get_borrow_room_at(size, map, room, depth);
      if (type != (room + 1)) {
        return false;
      }
    }
  }
  return true;
}

static bool is_borrow_hallway_clear(const borrow_size_t *size, const uint8_t *map, int from, int to) {
  for (int h = from + 1; h <= to; h++) {
    if (get_borrow_hallway_at(size, map, h) != 0) {
      return false;
    }
  }
  for (int h = to; h < from; h++) {
    if (get_borrow_hallway_at(size, map, h) != 0) {
      return false;
    }
  }
  return true;
}

static long measure_borrow_hallway_home_dist(const borrow_t *borrow, uint8_t *map) {
  long distance = 0;
  for (int h = 0; h < borrow->size.hallway; h++) {
    uint8_t room = get_borrow_hallway_at(&borrow->size, map, h);
    if (room-- == 0) {
      continue;
    }
    int entrance = borrow->entrances[room];
    if (entrance < h) {
      distance += (h - entrance) * borrow->costs[room];
    } else if (entrance > h) {
      distance += (entrance - h) * borrow->costs[room];
    }
  }
  return distance;
}

static int clear_borrow_hallway(const borrow_t *borrow, uint8_t *map, cost_t *cost) {
  long cleared = 0;

  for (int h = 0; h < borrow->size.hallway; h++) {
    uint8_t room = get_borrow_hallway_at(&borrow->size, map, h);
    if (room-- == 0) {
      continue;
    }
    int to = borrow->entrances[room];
    if (!is_borrow_hallway_clear(&borrow->size, map, h, to)) {
      continue;
    }
    int free = count_free_at_borrow_room(&borrow->size, map, room);
    if (free == 0) {
      continue;
    }
    int home = count_home_at_borrow_room(&borrow->size, map, room);
    if (home + free == borrow->size.depth) {
      set_borrow_hallway_at(&borrow->size, map, h, 0);
      set_borrow_room_at(&borrow->size, map, room, free - 1, room + 1);
      cost->energy += (((to > h) ? (to - h) : (h - to)) + free) * borrow->costs[room];
      cleared++;
    }
  }

  if (cleared > 0) {
    cost->distance = measure_borrow_hallway_home_dist(borrow, map);
  }
  return cleared;
}

static long organize_borrow(const borrow_t *borrow, struct heap *heap) {
  uint8_t current[heap->block], next[heap->block];
  cost_t *current_cost = get_cost_ptr(current), *next_cost = get_cost_ptr(next);
  uint8_t *current_map = get_map_ptr(current), *next_map = get_map_ptr(next);

  // rooms indices to mark as cleared
  uint8_t rooms[borrow->size.rooms];
  for (uint8_t room = 0; room < borrow->size.rooms; room++) {
    rooms[room] = room;
  }

  while (heap_pop(heap, current)) {
    // scan for duplicate states following
    // the cost function and clearing order makes this possible most of the time
    while (heap_peek(heap, next)) {
      if (memcmp(current + sizeof(cost_t), next + sizeof(cost_t), heap->block - sizeof(cost_t)) == 0) {
        heap_pop(heap, next);
      }
      break;
    }

    int cleared = clear_borrow_hallway(borrow, current_map, current_cost);
    if (cleared > 0) {
      if (heap_push(heap, current) < 0) {
        fprintf(stderr, "Error: heap buffer exceeded\n");
        return -1;
      }
      continue;
    }

    if (is_borrow_solution(&borrow->size, current_map)) {
      return current_cost->energy;
    }

    // we want to clear by type first to identify (more) duplicates after the 'pop'
    // instead of clearing room by room
    uint8_t num_rooms = borrow->size.rooms;
    for (uint8_t type = 1; type <= borrow->size.rooms; type++) {
      for (uint8_t r = 0; r < num_rooms; r++) {
        uint8_t room = rooms[r];
        int free = count_free_at_borrow_room(&borrow->size, current_map, room);
        if (free == borrow->size.depth) {
          continue; // none one here to move out
        }
        if (get_borrow_room_at(&borrow->size, current_map, room, free) != type) {
          continue;
        }
        int home = count_home_at_borrow_room(&borrow->size, current_map, room);
        if (free + home == borrow->size.depth) {
          continue; // all here are at home
        }

        // room to clear found: swap with last to mark as cleared
        rooms[r--] = rooms[--num_rooms];
        rooms[num_rooms] = room;

        int from = borrow->entrances[room];
        uint16_t room_check = 0;
        for (int h = 0; h < borrow->size.hallway; h++) {
          bool entrance = borrow->entrances[room_check] == h;
          room_check += entrance;
          if (entrance) {
            continue;
          }
          if (!is_borrow_hallway_clear(&borrow->size, current_map, from, h)) {
            continue;
          }

          // derive next map & cost based om current with one moved out of `room` to the h(allway)
          memcpy(next, current, heap->block);
          uint8_t type = get_borrow_room_at(&borrow->size, next_map, room, free);
          set_borrow_room_at(&borrow->size, next_map, room, free, 0);
          set_borrow_hallway_at(&borrow->size, next_map, h, type);
          next_cost->energy += (((from > h) ? (from - h) : (h - from)) + free + 1) * borrow->costs[type - 1];
          next_cost->distance = measure_borrow_hallway_home_dist(borrow, next_map);

          if (heap_push(heap, next) < 0) {
            fprintf(stderr, "Error: heap memory limit exceeded\n");
            return -1;
          }
        }
      }
    }
  }

  fprintf(stderr, "Error: no solution found\n");
  return -1;
}

static bool read_borrow_map(borrow_t *borrow,
                            uint8_t map[borrow->size.hallway + borrow->size.rooms * borrow->size.depth], bool inject) {
  if (!is_borrow_size_valid(&borrow->size)) {
    fprintf(stderr, "Error: borrow maximum size is invalid\n");
    return false;
  }

  borrow_size_t max_size = borrow->size;
  borrow->size.hallway = 0;
  borrow->size.rooms = 0;
  borrow->size.depth = 0;
  memset(borrow->entrances, 0, sizeof(int) * max_size.rooms);
  memset(map, 0, sizeof(uint8_t) * count_borrow_locations(&max_size));
  { // setup costs
    int cost = 1;
    for (int i = 0; i < max_size.rooms; i++) {
      borrow->costs[i] = cost;
      cost *= 10;
    }
  }

  // limit line buffer overflow with "%Xs" with X = max_size.hallway + 3
  char line[max_size.hallway + 3];
  char fmt[32];
  {
    fmt[0] = '%';
    sprintf(fmt + 1, "%d", max_size.hallway + 3);
    strcpy(fmt + strlen(fmt), "s\n");
  }

  int n;
  // top wall
  if (scanf(fmt, line) != 1) {
    fprintf(stderr, "Error: borrow format width limit exceeded (top wall)\n");
    return false;
  }
  n = strlen(line);
  if (n < 3) {
    fprintf(stderr, "Error: borrow format is invalid (top wall)\n");
    return false;
  }
  for (int i = 0; i < n; i++) {
    if (line[i] != '#') {
      fprintf(stderr, "Error: borrow format is invalid (no top wall)\n");
      return false;
    }
  }
  borrow->size.hallway = n - 2;
  // hallway
  if (scanf(fmt, line) != 1) {
    fprintf(stderr, "Error: borrow format width limit exceeded (hallway)\n");
    return false;
  }
  n = strlen(line);
  if (n != borrow->size.hallway + 2) {
    fprintf(stderr, "Error: borrow format is invalid (hallway)\n");
    return false;
  }
  for (int i = 0; i < n; i++) {
    if ((i == 0) | (i + 1 == n)) { // must be a wall
      if (line[i] != '#') {
        fprintf(stderr, "Error: borrow format is invalid (hallway walls)\n");
        return false;
      }
      continue;
    }
    if (line[i] == '.') {
      set_borrow_hallway_at(&max_size, map, i - 1, 0);
    } else {
      set_borrow_hallway_at(&max_size, map, i - 1, line[i] - 'A' + 1);
    }
  }
  // first of rooms locations
  if (scanf(fmt, line) != 1) {
    fprintf(stderr, "Error: borrow format width limit exceeded (at depth=0)\n");
    return false;
  }
  n = strlen(line);
  if (n != borrow->size.hallway + 2) {
    fprintf(stderr, "Error: borrow format is invalid (at depth=0)\n");
    return false;
  }
  for (int i = 0; i < n; i++) {
    if (line[i] == '#') {
      continue;
    }
    if ((i == 0) | (i + 1 == n)) { // must be a wall
      fprintf(stderr, "Error: borrow format is invalid (at depth=0)\n");
      return false;
    }
    if (borrow->size.rooms >= max_size.rooms) {
      fprintf(stderr, "Error: borrow room limit exceeded (at depth=0)\n");
      return false;
    }
    borrow->entrances[borrow->size.rooms] = i - 1;
    if (line[i] == '.') {
      set_borrow_room_at(&max_size, map, borrow->size.rooms++, 0, 0);
    } else {
      set_borrow_room_at(&max_size, map, borrow->size.rooms++, 0, line[i] - 'A' + 1);
    }
  }
  borrow->size.depth++;
  // inject walls
  if (inject) {
    if (borrow->size.rooms != 4) {
      fprintf(stderr, "Error: borrow injection only supported with 4 rooms\n");
      return false;
    }
    if (borrow->size.depth + 2 >= max_size.depth) {
      fprintf(stderr, "Error: borrow injection limit exceeded (at depth=%d)\n", borrow->size.depth);
      return false;
    }
    // depth = 1
    set_borrow_room_at(&max_size, map, 0, borrow->size.depth, 4); // 'D'
    set_borrow_room_at(&max_size, map, 1, borrow->size.depth, 3); // 'C'
    set_borrow_room_at(&max_size, map, 2, borrow->size.depth, 2); // 'B'
    set_borrow_room_at(&max_size, map, 3, borrow->size.depth, 1); // 'A'
    borrow->size.depth++;
    // depth = 2
    set_borrow_room_at(&max_size, map, 0, borrow->size.depth, 4); // 'D'
    set_borrow_room_at(&max_size, map, 1, borrow->size.depth, 2); // 'B'
    set_borrow_room_at(&max_size, map, 2, borrow->size.depth, 1); // 'A'
    set_borrow_room_at(&max_size, map, 3, borrow->size.depth, 3); // 'C'
    borrow->size.depth++;
  }
  // rooms and final wall
  while ((n = scanf(fmt, line)) != EOF) {
    if (n != 1) {
      fprintf(stderr, "Error: borrow format width limit exceeded (at depth=%d)\n", borrow->size.depth);
      return false;
    }
    n = strlen(line);
    if (n != borrow->size.rooms * 2 + 1) {
      fprintf(stderr, "Error: borrow format is invalid (at depth=%d)\n", borrow->size.depth);
      return false;
    }

    int level_rooms = 0;
    for (int room = 0, i = 0; i < n; i++) {
      if (line[i] == '#') {
        continue;
      }
      if (i % 2 == 0) { // must be a wall
        fprintf(stderr, "Error: borrow format is invalid (at depth=%d)\n", borrow->size.depth);
        return false;
      }
      if (level_rooms >= borrow->size.rooms) {
        fprintf(stderr, "Error: borrow room limit exceeded (at depth=%d)\n", borrow->size.depth);
        return false;
      }
      if (borrow->size.depth >= max_size.depth) {
        fprintf(stderr, "Error: borrow depth limit exceeded (at depth=%d)\n", borrow->size.depth);
        return false;
      }
      if (line[i] == '.') {
        set_borrow_room_at(&max_size, map, level_rooms++, borrow->size.depth, 0);
      } else {
        set_borrow_room_at(&max_size, map, level_rooms++, borrow->size.depth, line[i] - 'A' + 1);
      }
    }
    if (level_rooms == 0) {
      break; // bottom walls
    }
    if (level_rooms != borrow->size.rooms) {
      fprintf(stderr, "Error: borrow room mismatch (at depth=%d)\n", borrow->size.depth);
      return false;
    }
    borrow->size.depth++;
  }

  if (!is_borrow_valid(borrow)) {
    fprintf(stderr, "Error: borrow is invalid\n");
    return false;
  }

  for (int h = 0; h < borrow->size.hallway; h++) {
    set_borrow_hallway_at(&borrow->size, map, h, get_borrow_hallway_at(&max_size, map, h));
  }
  for (int room = 0; room < borrow->size.rooms; room++) {
    for (int d = 0; d < borrow->size.depth; d++) {
      set_borrow_room_at(&borrow->size, map, room, d, get_borrow_room_at(&max_size, map, room, d));
    }
  }

  if (!is_borrow_map_valid(borrow, map)) {
    fprintf(stderr, "Error: borrow map is invalid\n");
    return false;
  }
  return true;
}

int run(borrow_size_t max_size, size_t mem_size, void *memory, bool inject) {

  int locations = count_borrow_locations(&max_size);
  uint8_t map[locations];

  int entrances[max_size.rooms];
  int costs[max_size.rooms];
  borrow_t borrow = {
      .size = max_size,
      .entrances = entrances,
      .costs = costs,
  };
  if (!read_borrow_map(&borrow, map, inject)) {
    return EXIT_FAILURE;
  }

  struct heap heap = {
      .size = 0,
      .block = sizeof(cost_t) + sizeof(uint8_t) * locations,
      .capacity = mem_size,
      .memory = memory,
      .cmp = cmp_cost_ptrs,
  };
  if (mem_size < heap.block) {
    fprintf(stderr, "Error: memory limit exceeded\n");
    return EXIT_FAILURE;
  }
  memset(memory, 0, sizeof(cost_t));
  memcpy(get_map_ptr(memory), map, locations);

  if (heap_push(&heap, memory) < 0) {
    fprintf(stderr, "Error: heap memory limit exceeded\n");
    return EXIT_FAILURE;
  }

  printf("%ld\n", organize_borrow(&borrow, &heap));
  return EXIT_FAILURE;
}

int aoc_run_2021_day_23(bool first) {
  const int MAX_HALLWAY = 20; // 11;
  const int MAX_ROOMS = 8;    // 4;
  const int MAX_DEPTH = 8;    // 4;

  const size_t MEM_SIZE = 1024 * 1024 * 8; // 8MB is enough the AoC inputs; others might need more
  void *memory = malloc(MEM_SIZE);
  if (memory == NULL) {
    fprintf(stderr, "Error: arena memory allocation failed\n");
    return EXIT_FAILURE;
  }

  borrow_size_t max_size = {.hallway = MAX_HALLWAY, .rooms = MAX_ROOMS, .depth = MAX_DEPTH};
  return run(max_size, MEM_SIZE, memory, !first /*inject*/);
}

#define AOC_RUN_DAY aoc_run_2021_day_23
#include "main.c"
