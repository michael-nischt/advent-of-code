#ifndef PART_H
#define PART_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef PART_STATIC
#define PART_API static
#else
#define PART_API extern
#endif

// partition an array numbers such as:
// all numbers[<k] are leq to numbers[k]
// all numbers[>k] are qeq to numbers[k]
PART_API void qpart(void *base, size_t nitems, size_t size, size_t at, int (*cmp)(const void *, const void *));

#ifdef PART_IMPL

// Based on Hoare Partitioning
// complexity: O(n) average-case, O(n^2) worst-case
// as found in:
// https://people.inf.ethz.ch/wirth/AD.pdf
// Algorithms and Data Structures
// N. Wirth 1985
// 2.3.4 Finding the Median
//
// with smart pivot selection it becomes O(n) worst-case
// see: https://en.wikipedia.org/wiki/Median_of_medians
PART_API void qpart(void *base, size_t nitems, size_t size, size_t at, int (*cmp)(const void *, const void *)) {
  uint8_t pivot[size], tmp[size], *items = (uint8_t *)base;

  uint8_t *i, *j;
  uint8_t *k = items + size * at;
  uint8_t *l = items;
  uint8_t *h = items + size * (nitems - 1);
  while (l < h) {
    memcpy(pivot, k, size); // can be a different pivot than `k` within `items[l:h]`
    i = l;
    j = h;

    do {
      while (cmp(i, pivot) < 0) {
        i += size;
      }
      while (cmp(pivot, j) < 0) {
        j -= size;
      }
      if (i <= j) {
        { // swap
          memcpy(tmp, i, size);
          memcpy(i, j, size);
          memcpy(j, tmp, size);
        }
        i += size;
        j -= size;
      }
    } while (i <= j);

    if (j < k) {
      l = i;
    }
    if (k < i) {
      h = j;
    }
  }
}

#endif // PART_IMPL

#ifdef __cplusplus
}
#endif

#endif // PART_H
