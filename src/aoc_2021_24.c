#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool match_inp(const char cmd[4], char r) {
  char fmt[] = "CMD %c\n", c;
  strncpy(fmt, cmd, 3);
  if ((scanf(fmt, &c) != 1) || (c != r)) {
    fprintf(stderr, "Error: expected instruction '%s %c'\n", cmd, r);
    return false;
  }
  return true;
}

static bool match_op_var(const char cmd[4], char r, char var) {
  char fmt[] = "CMD r %c\n", c;
  strncpy(fmt, cmd, 3);
  fmt[4] = r;
  if ((scanf(fmt, &c) != 1) || (c != var)) {
    fprintf(stderr, "Error: expected instruction '%s %c %c'\n", cmd, r, var);
    return false;
  }
  return true;
}

static bool match_op_val(const char cmd[4], char r, int val) {
  char fmt[] = "CMD r %d\n";
  int i;
  strncpy(fmt, cmd, 3);
  fmt[4] = r;
  if ((scanf(fmt, &i) != 1) || (i != val)) {
    fprintf(stderr, "Error: expected instruction '%s %c %d'\n", cmd, r, val);
    return false;
  }
  return true;
}

static bool read_op_val(const char cmd[4], char r, int *val) {
  char fmt[] = "CMD r %d\n";
  strncpy(fmt, cmd, 3);
  fmt[4] = r;
  if ((scanf(fmt, val) != 1)) {
    fprintf(stderr, "Error: expected instruction '%s %c $VAL'\n", cmd, r);
    return false;
  }
  return true;
}

static int run(bool largest) {
#define TRY(expr)        \
  if (!(expr)) {         \
    return EXIT_FAILURE; \
  }

  typedef struct state {
    int j, add;
  } state_t;

  const int N = 14;
  state_t states[N];
  uint8_t inputs[N];

  int n = 0;
  for (int i = 0; i < N; i++) {
    // 1. assumption: data is repetitive except for 3 variables
    // (z' is the previous value of z, which is initially 0)
    int div, chk, add;
    TRY(match_inp("inp", 'w'));         // w = inp;
    TRY(match_op_val("mul", 'x', 0));   // x = 0;
    TRY(match_op_var("add", 'x', 'z')); // x = z';
    TRY(match_op_val("mod", 'x', 26));  // x = z' % 26;
    TRY(read_op_val("div", 'z', &div)); // z = z' / div;
    TRY(read_op_val("add", 'x', &chk)); // x = z' % 26 + chk
    TRY(match_op_var("eql", 'x', 'w')); // x = z' % 26 + chk == inp;
    TRY(match_op_val("eql", 'x', 0));   // x = z' % 26 + chk != inp;
    TRY(match_op_val("mul", 'y', 0));   // y = 0;
    TRY(match_op_val("add", 'y', 25));  // y = 25;
    TRY(match_op_var("mul", 'y', 'x')); // y = 25 * (z' % 26 + chk != inp);
    TRY(match_op_val("add", 'y', 1));   // y = (25 * (z' % 26 + chk != inp)) + 1;
    TRY(match_op_var("mul", 'z', 'y')); // z = (z' / div) * (25 * (z' % 26 + chk != inp) + 1);
    TRY(match_op_val("mul", 'y', 0));   // y = 0;
    TRY(match_op_var("add", 'y', 'w')); // y = inp;
    TRY(read_op_val("add", 'y', &add)); // y = inp + add;
    TRY(match_op_var("mul", 'y', 'x')); // y = (inp + add) * (z' % 26 + chk != inp);
    TRY(match_op_var("add", 'z', 'y')); // yields to:
    // z = ((z' / div) * (25 * (z' % 26 + chk != inp)) + 1) + (inp + add) * (z' % 26 + chk != inp);
    //
    // if (z' % 26 + chk != inp) then:
    //   z = (z' / div) * 26 + (inp + add);
    // else:
    //   z = z' / div;

    // 2. assumption: variable domains are limited
    // 2.1: (div == 1) | (div == 26)
    if ((div != 1) & (div != 26)) {
      fprintf(stderr, "Error: expected division of 1 or 26 (base)\n");
      return EXIT_FAILURE;
    }
    // --> 4 possible cases
    //
    // if (div == 1) then:
    //   if (z' % 26 + chk != inp) then:
    //     z = z' * 26 + (inp + add);
    //   else:
    //     z = z';
    // else: # if (div == 26) then:
    //   if (z' % 26 + chk != inp) then:
    //     z = (z' / 26) * 26 + (inp + add);
    //   else:
    //     z = z' / 26;

    // 2.2: if (div == 1) then: (chk > 9)
    if ((div == 1) & (chk <= 9)) {
      fprintf(stderr, "Error: expected larger addition than any possible input in cases without division\n");
      return EXIT_FAILURE;
    }
    // (z' % 26 + chk != inp) is guaranteed to be true for (div == 1) since (chk > 9).
    // --> 3 possible cases
    //
    // if (div == 1) then:
    //   z = z' * 26 + (inp + add);
    // else: # if (div == 26) then:
    //   if (z' % 26 + chk != inp) then:
    //     z = (z' / 26) * 26 + (inp + add);
    //   else:
    //     z = z' / 26;

    // 2.3: (add >= 0)
    if (add < 0) {
      fprintf(stderr, "Error: expected positive addition\n");
      return EXIT_FAILURE;
    }
    // --> z is never negative (all inp are range [1,9])

    // 2.4: there are as many cases of (a == 1) as (a == 26) and
    // at no point more of the second type the preceding the first.
    if ((div != 1) & (n == 0)) {
      fprintf(stderr, "Error: more divisions than not\n");
      return EXIT_FAILURE;
    }
    // z == 0 can only true if each division reduces he number by a (base) magitude
    // since without division it always increases by a (base) magitude
    // --> 2 possible cases and 1 input constraint
    //
    // if (div == 1) then:
    //   z = z' * 26 + (inp + add);
    // else: # if (div == 26) then:
    //   z = z' / 26;
    //
    // inp(uts) are only valid if (z' % 26 + chk == inp)

    // 3. maximizing / minimizing inputs
    // valid input constraint: z' % 26 + chk[i] == inp[i]
    // with: z' % 26 == inp[j] + add[j]
    // and: j being the magnitude increase to be decreased
    // -->
    // inp[j] + add[j] + chk[i] == inp[i]

    // given:
    // 1 <= inp[i] <= 9
    // 1 <= inp[j] <= 9
    // -->
    // 1 <= inp[i] - (add[j] + chk[i]) <= 9
    // 1 <= inp[j] + (add[j] + chk[i]) <= 9

    // additionally, for largest possible number:
    // inp[i] <= 9 + (add[j] + chk[i])
    // inp[j] <= 9 - (add[j] + chk[i])

    // additionally, for smallest possible number:
    // 1 + (add[j] + chk[i]) <= inp[i]
    // 1 - (add[j] - chk[i]) <= inp[j]

    if (div == 1) {
      states[n++] = (state_t){.j = i, .add = add};
    } else {
      state_t s = states[--n];
      int d = s.add + chk;
      int d_neg = d * (d < 0);
      int d_pos = d * (d > 0);
      if (largest) {
        inputs[i] = 9 + d_neg;
        inputs[s.j] = 9 - d_pos;
      } else {
        inputs[i] = 1 + d_pos;
        inputs[s.j] = 1 - d_neg;
      }
    }
  }

  if (n != 0) {
    fprintf(stderr, "Error: expected equal number of divission than not\n");
    return EXIT_FAILURE;
  }

  long model_number = 0;
  for (int i = 0; i < 14; i++) {
    model_number *= 10;
    model_number += inputs[i];
  }

  printf("%ld\n", model_number);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_24(bool first) { return run(first /*largest instead of smallest*/); }

#define AOC_RUN_DAY aoc_run_2021_day_24
#include "main.c"
