#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define PART_IMPL
#include "part.h"

inline static int cmp_longs(long a, long b) { return (a > b) - (a < b); }
inline static int cmp_long_ptrs(const void *a, const void *b) {
  return cmp_longs(*((const long *)a), *((const long *)b));
}

static long median(int n, long numbers[n]) {
  // partition numbers such as:
  // all numbers[<k] are leq to numbers[k]
  // all numbers[>k] are qeq to numbers[k]
  int k = (n & 1) ? (n / 2) : (n / 2 - 1);
  qpart(numbers, n, sizeof(long), k, cmp_long_ptrs);
  return numbers[k];
}

// returns the mismatched bracket (1-based index)
// or the number of missing closing brackets (negative number value)
static int validate_line_syntax(const char *line, char *stack) {
  int n = 0;

  for (int i = 0; line[i] != '\0'; i++) {
    switch (line[i]) {
    case '(':
    case '[':
    case '{':
    case '<':
      stack[n++] = line[i];
      break;
    case ')':
      if (stack[--n] != '(') {
        return 1;
      }
      break;
    case ']':
      if (stack[--n] != '[') {
        return 2;
      }
      break;
    case '}':
      if (stack[--n] != '{') {
        return 3;
      }
      break;
    case '>':
      if (stack[--n] != '<') {
        return 4;
      }
      break;
    }
  }

  return -n;
}

static int read_line(int line_size, char line[1 + line_size]) {
  line[line_size] = '\n';
  if (fgets(line, 1 + line_size, stdin) == NULL) {
    return 0;
  }
  if (line[line_size] != '\n') {
    fprintf(stderr, "Error: line character limit exceeded\n");
    return -1;
  }
  for (int i = 0; line[i] != '\0'; i++) {
    if (isspace(line[i])) {
      line[i] = '\0';
      break;
    }
    if ((line[i] != '(') & (line[i] != ')') & (line[i] != '[') & (line[i] != ']') & (line[i] != '{') &
        (line[i] != '}') & (line[i] != '<') & (line[i] != '>')) {
      fprintf(stderr, "Error: invalid line character\n");
      return -1;
    }
  }
  return +1;
}

static int run_01(int line_size, char line[1 + line_size], char stack[line_size]) {
  int errors[5] = {0};

  int read;
  while ((read = read_line(line_size, line)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    int err_type = validate_line_syntax(line, stack);
    err_type *= (err_type >= 0);
    errors[err_type]++;
  }

  printf("%ld\n", errors[1] * 3L + errors[2] * 57L + errors[3] * 1197L + errors[4] * 25137L);
  return EXIT_SUCCESS;
}

static int run_02(int line_size, char line[1 + line_size], char stack[line_size], int max_lines,
                  long scores[max_lines]) {
  int num_lines = 0;
  int read;
  while ((read = read_line(line_size, line)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    int missing = -validate_line_syntax(line, stack);
    if (missing <= 0) {
      continue;
    }

    if (num_lines >= max_lines) {
      fprintf(stderr, "Error: line limit exceeded\n");
      return EXIT_FAILURE;
    }

    long score = 0;
    for (int i = missing - 1; i >= 0; i--) {
      switch (stack[i]) {
      case '(':
        score = score * 5 + 1;
        break;
      case '[':
        score = score * 5 + 2;
        break;
      case '{':
        score = score * 5 + 3;
        break;
      case '<':
        score = score * 5 + 4;
        break;
      }
    }

    scores[num_lines++] = score;
  }

  printf("%ld\n", median(num_lines, scores));
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_10(bool first) {
  const int MAX_CHARS = 255;
  const int MAX_LINES = 256;
  char line[1 + MAX_CHARS]; // 256 B
  char stack[MAX_CHARS];    // 255 B
  long scores[MAX_LINES];   // 2 KB

  return first ? run_01(MAX_CHARS, line, stack) : run_02(MAX_CHARS, line, stack, MAX_LINES, scores);
}

#define AOC_RUN_DAY aoc_run_2021_day_10
#include "main.c"
