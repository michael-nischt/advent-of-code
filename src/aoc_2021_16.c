#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct bitstream {
  FILE *input;
  long counter;
  int buffer;
} bitstream_t;

inline static int read_bitstream_bit(bitstream_t *stream) {
  static const int BITS = 4;
  int c;
  int bit = stream->counter % BITS;
  if (bit == 0) {
    if ((stream->buffer = fgetc(stream->input)) == EOF) {
      return -1;
    }
    if ((stream->buffer >= 'A') & (stream->buffer <= 'F')) {
      stream->buffer = stream->buffer - 'A' + 10;
    } else if ((stream->buffer >= '0') & (stream->buffer <= '9')) {
      stream->buffer = stream->buffer - '0';
    } else {
      return -1;
    }
  }
  return 1 & (stream->buffer >> (BITS - (stream->counter++ % BITS + 1)));
}

static int read_bitstream_number(bitstream_t *stream, int bits) {
  int number = 0, bit;
  for (int i = bits - 1; i >= 0; i--) {
    if ((bit = read_bitstream_bit(stream)) < 0) {
      return -1;
    }
    number |= bit << i;
  }
  return number;
}

static long read_bitstream_literal(bitstream_t *stream) {
  static const int BITS = 4;
  long number = 0;
  int next, block;
  do {
    if ((next = read_bitstream_bit(stream)) < 0) {
      return -1;
    }
    if ((block = read_bitstream_number(stream, 4)) < 0) {
      return -1;
    }
    number = (number << BITS) | (block);
  } while (next != 0);

  return number;
}

static bool read_bitstream_header(bitstream_t *stream, int *version, int *type) {
  if ((*version = read_bitstream_number(stream, 3)) < 0) {
    return false; // EOF
  }
  *type = read_bitstream_number(stream, 3);
  return true;
}

static long sum_bitstream_versions(bitstream_t *stream) {
  int version, type;
  if (!read_bitstream_header(stream, &version, &type)) {
    return 0; // EOF
  }

  if (type == 4) { // literal package
    read_bitstream_literal(stream);
    return version;
  }

  long sum = version;

  // operator package
  int len = read_bitstream_bit(stream);

  long bits = 0;
  if (len == 0) {
    bits = read_bitstream_number(stream, 15);
  } else {
    len = read_bitstream_number(stream, 11);
  }

  long result = 0;

  bits += stream->counter;
  for (int i = 0; (i < len) | (stream->counter < bits); i++) {
    int at = stream->counter;

    sum += sum_bitstream_versions(stream);

    if (stream->counter <= at) {
      break; // unexpected EOF
    }
  }
  return sum;
}

inline static long long_min(long a, long b) { return a * (a <= b) + b * (a > b); }
inline static long long_max(long a, long b) { return a * (a >= b) + b * (a < b); }

static long evaluate_bitstream(bitstream_t *stream) {
  int version, type;
  if (!read_bitstream_header(stream, &version, &type)) {
    return 0; // EOF
  }
  if (type == 4) { // literal package
    return read_bitstream_literal(stream);
  }

  // operator package
  int len = read_bitstream_bit(stream);

  long bits = 0;
  if (len == 0) {
    bits = read_bitstream_number(stream, 15);
  } else {
    len = read_bitstream_number(stream, 11);
  }

  long result = 0;

  bits += stream->counter;
  for (int i = 0; (i < len) | (stream->counter < bits); i++) {
    int at = stream->counter;

    if (i == 0) {
      result = evaluate_bitstream(stream);
      continue;
    }
    switch (type) {
    case 0:
      result += evaluate_bitstream(stream);
      break;
    case 1:
      result *= evaluate_bitstream(stream);
      break;
    case 2:
      result = long_min(result, evaluate_bitstream(stream));
      break;
    case 3:
      result = long_max(result, evaluate_bitstream(stream));
      break;
    case 5:
      result = result > evaluate_bitstream(stream);
      break;
    case 6:
      result = result < evaluate_bitstream(stream);
      break;
    case 7:
      result = result == evaluate_bitstream(stream);
      break;
    }

    if (stream->counter <= at) {
      break; // unexpected EOF
    }
  }

  return result;
}

static int run01(bitstream_t *stream) {
  long result = sum_bitstream_versions(stream);
  if (result < 0) {
    return EXIT_FAILURE;
  }
  printf("%ld\n", result);
  return EXIT_SUCCESS;
}

static int run02(bitstream_t *stream) {
  long result = evaluate_bitstream(stream);
  if (result < 0) {
    return EXIT_FAILURE;
  }
  printf("%ld\n", result);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_16(bool first) {
  bitstream_t stream = {.input = stdin};

  return first ? run01(&stream) : run02(&stream);
}

#define AOC_RUN_DAY aoc_run_2021_day_16
#include "main.c"
