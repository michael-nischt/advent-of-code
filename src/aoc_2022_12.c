#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>

#define HEAP_IMPL
#include "heap.h"


struct node {
  int index, priority;
};

inline static int cmp_nodes(const struct node *a, const struct node *b) {
  return ((a->priority > b->priority) - (a->priority < b->priority));
}

inline static int cmp_node_ptrs(const void *a, const void *b) { return cmp_nodes(a, b); }


static void print_image(int width, int height, const uint8_t image[width * height], int start, int end) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++, i++) {
      uint8_t c = image[i];
      if (i == end) {
        c = 'E';
      } else if (i == start) {
        c = 'S';
      } else {
        c += 'a';
      }
      printf("%c", c);
    }
    printf("\n");
  }
}

static inline int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

static bool hill_climbing_dijkstra(int width, int height,
                                   uint8_t image[static (1+width*height)],
                                   int distances[static (width*height)],
                                   struct node nodes[static (width*height)],
                                   int from) {

  static const int links[4][2] = {{0, -1}, {0, +1}, {-1, 0}, {+1, 0}};

  int n = width*height;
  for (int i=0; i<n; i++) {
    distances[i] = INT_MAX;
  }
  distances[from] = 0;

  nodes[0] = (struct node) { .index = from, .priority = 1 };
  struct heap heap = {
      .size = 1,
      .block = sizeof(struct node),
      .capacity = sizeof(struct node) * n,
      .memory = nodes,
      .cmp = cmp_node_ptrs,
  };

  struct node current;
  while (heap_pop(&heap, &current)) {
    int x = current.index % width;
    int y = current.index / width;
    int s = distances[current.index] + 1;

    int index, distance;
    for (int l = 0; l < 4; l++) {
      index = image_index(width, height, x + links[l][0], y + links[l][1]);
      if ((image[index] + 1) < image[current.index]) {
        continue; // can't ascend
      }
      if (s >= distances[index]) {
        continue; // shorter path exists
      }
      distances[index] = s;
      if (heap_push(&heap, &(struct node){.index = index, .priority = s}) >= heap.size) {
        return false;
      }
    }
  }

  return true;
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height, int *start, int *end) {
  int image_size = 0;
  *width = 0;
  *height = 0;
  *start = max_image_size;
  *end = max_image_size;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if (((c >= 'a') & (c <= 'z')) | (c == 'S') | (c == 'E')) {
      space = false;
      if (image_size >= max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      if (c == 'S') {
        *start = image_size;
        c = 'a';
      } else if (c == 'E') {
        *end = image_size;
        c = 'z';
      }
      image[image_size++] = c - 'a';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run_01(int image_size, uint8_t image[static (1+image_size)],
                  int steps[static (image_size)],
                  struct node nodes[static (image_size)]) {

  int width, height, start, end;
  if ((image_size = read_image(image_size, image, &width, &height, &start, &end)) < 0) {
    return EXIT_FAILURE;
  }
  image[image_size] = 0xFF; // mark outside as unreachable

  hill_climbing_dijkstra(width, height, image, steps, nodes, end);

  int score = steps[start];
  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02(int image_size, uint8_t image[static (1+image_size)],
                  int steps[static (image_size)],
                  struct node nodes[static (image_size)]) {

  int width, height, start, end;
  if ((image_size = read_image(image_size, image, &width, &height, &start, &end)) < 0) {
    return EXIT_FAILURE;
  }
  image[image_size] = 0xFF; // mark outside as unreachable

  hill_climbing_dijkstra(width, height, image, steps, nodes, end);

  int min = INT_MAX;
  for(int i=0; i<image_size; i++) {
    if (image[i] != 0) {
      continue; // not a possible start
    }
    if (steps[i] < min) {
      min = steps[i];
    }
  }

  printf("%d\n", min);
  return EXIT_SUCCESS;
}


int aoc_run_2022_day_12(bool first) {
  const int MAX_IMAGE_SIZE = 144 * 144;
  uint8_t *image /*[1+MAX_IMAGE_SIZE]*/;    // <  21 KB
  int *steps /*[MAX_IMAGE_SIZE]*/;          // <  128 KB
  struct node *nodes /*[MAX_IMAGE_SIZE]*/;  // <  256 KB
  { // free'd at EXIT
    void *arena = malloc(1+MAX_IMAGE_SIZE + 3 *sizeof(int) * MAX_IMAGE_SIZE);
    image = (uint8_t*) arena;
    steps = (int*) (image + MAX_IMAGE_SIZE);
    nodes = (struct node*) (steps + MAX_IMAGE_SIZE);
  }
  return first ? run_01(MAX_IMAGE_SIZE, image, steps, nodes)
               : run_02(MAX_IMAGE_SIZE, image, steps, nodes);
}

#define AOC_RUN_DAY aoc_run_2022_day_12
#include "main.c"
