#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static int run_01() {
  int increased = 0;

  int last = INT_MAX, current, read;
  while ((read = scanf("%d", &current)) != EOF) {
    if (read != 1) {
      fprintf(stderr, "Error: expected measurement integer\n");
      return EXIT_FAILURE;
    }
    increased += (last < current);
    last = current;
  }

  printf("%d\n", increased);
  return EXIT_SUCCESS;
}

static int run_02() {
  int increased = 0;

  int last = INT_MAX;
  int window[3] = {INT_MAX / 3, INT_MAX / 3, INT_MAX / 3};

  int index = 0, read;
  while ((read = scanf("%d", &window[index++])) != EOF) {
    if (read != 1) {
      fprintf(stderr, "Error: expected measurement integer\n");
      return EXIT_FAILURE;
    }
    int current = window[0] + window[1] + window[2];
    increased += (last < current);
    last = current;
    index %= 3;
  }

  printf("%d\n", increased);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_01(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2021_day_01
#include "main.c"
