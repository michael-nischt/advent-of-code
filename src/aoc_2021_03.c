#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static int rate_codes(int bit, int num_codes, int codes[num_codes]) {
  int rate = 0;
  for (int i = 0; i < num_codes; i++) {
    int val = codes[i];
    int set = (val >> bit) & 1;
    rate += set * 2 - 1;
  }
  return rate;
}

static int filter_codes(int bit, int cmp, int num_codes, int codes[num_codes]) {
  for (int i = 0; i < num_codes; i++) {
    int val = codes[i];
    int set = (val >> bit) & 1;
    if (set != cmp) {
      codes[i--] = codes[--num_codes];
      codes[num_codes] = val;
    }
  }
  return num_codes;
}

static int parse_code_bits(const char *code, int *number) {
  *number = 0;
  int bits;
  for (bits = 0; code[bits] != '\0'; bits++) {
    if ((code[bits] < '0') | (code[bits] > '1')) {
      return -1;
    }
    *number <<= 1;
    *number |= code[bits] != '0';
  }
  return bits;
}

static int read_codes(int *max_bits, int max_codes, int codes[max_codes]) {
  *max_bits = 0;
  int num_codes = 0;

  const int N = 16; // support for 15 bits + space for'\0'
  char code[N + 1]; // + space for '\n' as overrun marker
  code[N] = '\n';   // marker for buffer overflow
  int read;
  while ((read = scanf("%16s\n", code)) != EOF) {
    if (read != 1) {
      fprintf(stderr, "Error: expected bindary code\n");
      return EXIT_FAILURE;
    }
    if (code[N] != '\n') {
      fprintf(stderr, "Error: code has more than 15 bits\n");
      return -1;
    }
    if (num_codes >= max_codes) {
      fprintf(stderr, "Error: code limit exceeded\n");
      return -1;
    }
    int bits = parse_code_bits(code, &codes[num_codes++]);
    if (bits < 0) {
      fprintf(stderr, "Error: invalid bindary code\n");
      return EXIT_FAILURE;
    }
    if (bits > *max_bits) {
      *max_bits = bits;
    }
  }

  return num_codes;
}

static int run_01(int num_codes, int codes[num_codes]) {
  int bits;
  if ((num_codes = read_codes(&bits, num_codes, codes)) < 0) {
    return EXIT_FAILURE;
  }

  int gamma = 0, epsilon = 0;

  for (int bit = bits - 1; bit >= 0; bit--) {
    int delta = rate_codes(bit, num_codes, codes);
    gamma |= (delta >= 0) << bit;
    epsilon |= (delta < 0) << bit;
  }

  printf("%d\n", gamma * epsilon);
  return EXIT_SUCCESS;
}

static int run_02(int num_codes, int codes[num_codes]) {
  int bits;
  if ((num_codes = read_codes(&bits, num_codes, codes)) < 0) {
    return EXIT_FAILURE;
  }

  int oxygen = num_codes;
  for (int bit = bits - 1; oxygen > 1 && bit >= 0; bit--) {
    int delta = rate_codes(bit, oxygen, codes);
    oxygen = filter_codes(bit, delta >= 0, oxygen, codes);
  }
  oxygen = oxygen > 0 ? codes[0] : 0;

  int scrubber = num_codes;
  for (int bit = bits - 1; scrubber > 1 && bit >= 0; bit--) {
    int delta = rate_codes(bit, scrubber, codes);
    scrubber = filter_codes(bit, delta < 0, scrubber, codes);
  }
  scrubber = scrubber > 0 ? codes[0] : 0;

  printf("%d\n", oxygen * scrubber);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_03(bool first) {
  const int MAX_CODES = 1024;
  int codes[MAX_CODES]; // 4 KB

  return first ? run_01(MAX_CODES, codes) : run_02(MAX_CODES, codes);
}

#define AOC_RUN_DAY aoc_run_2021_day_03
#include "main.c"
