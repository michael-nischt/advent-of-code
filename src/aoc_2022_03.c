#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>


static int read_rucksack(int capacity, char rucksack[static capacity]) {
  rucksack[capacity - 1] = 0;
  if (fgets(rucksack, capacity, stdin) == NULL) {
    return 0;
  }
  if (rucksack[capacity - 1] != 0) {
    fprintf(stderr, "Error: expected rucksack with less than %d items\n", capacity);
    return -1;
  }
  int items = strlen(rucksack); // includes '\0' termination
  if ((items % 2) != 1) {
    fprintf(stderr, "Error: expected rucksack with even number of items\n");
    return -1;
  }
  return items - 1;
}

static int item_priority(char item) {
    if ((item >= 'a') & ((item <= 'z'))) {
      return 1 + (item - 'a');
    }
    if ((item >= 'A') & ((item <= 'Z'))) {
      return 27 + (item - 'A');
    }
    return 0; // should never happen
}

static uint64_t rucksack_items(int items, char rucksack[static items]) {
  uint64_t item_set = 0;
  for(int i=0; i<items; i++) {
    int priority = item_priority(rucksack[i]);
    item_set |= ((uint64_t) 1) << priority;
  }
  return item_set;
}

static int rucksack_priority(int items, char rucksack[static items]) {
  int comp = items / 2;
  uint64_t item_set = rucksack_items(comp, rucksack);
  for(int i=comp; i<items; i++) {
    int priority = item_priority(rucksack[i]);
    uint64_t bit = ((uint64_t) 1) << priority;
    if ((item_set & bit) != 0) {
      return priority;
    }
  }
  return 0;
}

static int run_01() {
  char rucksack[64];
  int items;
  int score = 0;

  while ((items = read_rucksack(sizeof(rucksack), rucksack)) != 0) {
    if (items < 0) {
      return EXIT_FAILURE;
    }
    score += rucksack_priority(items, rucksack);
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02() {
  static const int N = 3;
  char rucksack[64];
  uint64_t shared = UINT64_MAX;
  int items, c = 0;
  int score = 0;

  while ((items = read_rucksack(sizeof(rucksack), rucksack)) != 0) {
    if (items < 0) {
      return EXIT_FAILURE;
    }

    shared &= rucksack_items(items, rucksack);

    c++;
    if (c != N) {
      continue;
    }
    // group complete
    for(int priority=0; priority<64; priority++) {
      uint64_t bit = ((uint64_t) 1) << priority;
      if ((shared & bit) != 0) {
        score += priority;
        break;
      }
    }
    c %= N;
    shared = UINT64_MAX;
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_03(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_03
#include "main.c"
