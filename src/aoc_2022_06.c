#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>

static int find_marker(int n) {
  char buffer[n];
  int result = 0, i = 0, c;
  // read until cspace or EOF
  while((c = fgetc(stdin)) != EOF) {
    if (isspace(c)) {
      break;
    }
    if ((c < 'a') | (c > 'z')) {
      fprintf(stderr, "Error: expected stream with characters [a-z].\n");
      return -1;
    }

    buffer[i++ % n] = (char) (c - 'a');
    if ((i < n) | (result != 0)) {
      continue;
    }

    bool marker = true;
    uint32_t bits = 0;
    for (int j=0; marker & (j<n); j++)  {
      uint32_t bit = 1 << buffer[j];
      marker &= (bits & bit) == 0;
      bits |= bit;
    }

    if (marker) {
      result = i;
    }
  }
  return result;
}

static int run(int n) {
  int marker;
  while((marker = find_marker(n)) != 0) {
    if (marker < 0) {
      return EXIT_FAILURE;
    }
    printf("%d\n", marker);
  }
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_06(bool first) { return first ? run(4) : run(14); }

#define AOC_RUN_DAY aoc_run_2022_day_06
#include "main.c"
