#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static void print_image(int width, int height, uint8_t image[width * height]) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      printf("%d", image[i++]);
    }
    printf("\n");
  }
}

static inline int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

inline static bool spread(int x, int y, int width, int height, uint8_t image[(width * height) + 1], int n,
                          int stack[width * height]) {
  int i = image_index(width, height, x, y);
  stack[n] = i;
  image[i]++;
  image[i] *= i < (width * height); // clear outside
  return (image[i] == 10);
}

static int simulate_step(int width, int height, uint8_t image[1 + (width * height)], int flashed[width * height]) {
  int num_flashed = 0;

  for (int size = width * height, i = 0; i < size; i++) {
    image[i] *= (image[i] < 10);   // clear > 9
    image[i]++;                    // start at 1
    flashed[num_flashed] = i;      // set the last flash to this
    num_flashed += image[i] == 10; // but only increment count if it did
  }

  for (int i = 0; i < num_flashed; i++) {
    int x = flashed[i] % width;
    int y = flashed[i] / width;
    num_flashed += spread(x - 1, y - 1, width, height, image, num_flashed, flashed);
    num_flashed += spread(x, y - 1, width, height, image, num_flashed, flashed);
    num_flashed += spread(x + 1, y - 1, width, height, image, num_flashed, flashed);
    num_flashed += spread(x - 1, y, width, height, image, num_flashed, flashed);
    num_flashed += spread(x, y, width, height, image, num_flashed, flashed);
    num_flashed += spread(x + 1, y, width, height, image, num_flashed, flashed);
    num_flashed += spread(x - 1, y + 1, width, height, image, num_flashed, flashed);
    num_flashed += spread(x, y + 1, width, height, image, num_flashed, flashed);
    num_flashed += spread(x + 1, y + 1, width, height, image, num_flashed, flashed);
  }

  return num_flashed;
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height) {
  int image_size = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c >= '0') & (c <= '9')) {
      space = false;
      if (image_size == max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      image[image_size++] = c - '0';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run_01(int image_size, uint8_t image[1 + image_size], int flashed[image_size], int steps) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int total_flashes = 0;
  for (int i = 0; i < steps; i++) {
    total_flashes += simulate_step(width, height, image, flashed);
  }

  printf("%d\n", total_flashes);
  return EXIT_SUCCESS;
}

static int run_02(int image_size, uint8_t image[1 + image_size], int flashed[image_size]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int step = 0;
  while (++step) {
    int num_flashes = simulate_step(width, height, image, flashed);
    if (num_flashes == image_size) {
      break;
    }
  }

  printf("%d\n", step);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_11(bool first) {
  const int IMAGE_MAX_WIDTH = 10, IMAGE_MAX_HEIGHT = 10;
  uint8_t image[1 + IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT]; // 404 B
  int flashed[IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT];       // 400 B

  return first ? run_01(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image, flashed, 100 /*steps*/)
               : run_02(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image, flashed);
}

#define AOC_RUN_DAY aoc_run_2021_day_11
#include "main.c"
