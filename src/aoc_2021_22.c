#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct vec3 {
  int x, y, z;
} vec3_t;

typedef struct cuboid {
  vec3_t min, max;
} cuboid_t;

inline static long measure_cuboid_area(const cuboid_t *cuboid) {
  long area = 1;
  area *= 1 + (cuboid->max.x - cuboid->min.x);
  area *= 1 + (cuboid->max.y - cuboid->min.y);
  area *= 1 + (cuboid->max.z - cuboid->min.z);
  return area;
}

inline static bool is_cuboid_inside_other(const cuboid_t *a, const cuboid_t *b) {
  return (b->min.x <= a->min.x) & (b->min.y <= a->min.y) & (b->min.z <= a->min.z) & (a->max.x <= b->max.x) &
         (a->max.y <= b->max.y) & (a->max.z <= b->max.z);
}

inline static bool are_cuboids_intersecting(const cuboid_t *a, const cuboid_t *b) {
  return (a->min.x <= b->max.x) & (a->min.y <= b->max.y) & (a->min.z <= b->max.z) & (b->min.x <= a->max.x) &
         (b->min.y <= a->max.y) & (b->min.z <= a->max.z);
}

static int slice_cuboid(cuboid_t *block, const cuboid_t *slicer, int max_cuboids, cuboid_t cuboids[max_cuboids]) {
  if (!are_cuboids_intersecting(block, slicer)) {
    if (max_cuboids < 1) {
      return -1;
    }
    cuboids[0] = *block;
    return 1;
  }
  int added = 0;
  if (block->min.x < slicer->min.x) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].max.x = slicer->min.x - 1;
    block->min.x = slicer->min.x;
    added++;
  }
  if (block->max.x > slicer->max.x) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].min.x = slicer->max.x + 1;
    block->max.x = slicer->max.x;
    added++;
  }
  if (block->min.y < slicer->min.y) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].max.y = slicer->min.y - 1;
    block->min.y = slicer->min.y;
    added++;
  }
  if (block->max.y > slicer->max.y) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].min.y = slicer->max.y + 1;
    block->max.y = slicer->max.y;
    added++;
  }
  if (block->min.z < slicer->min.z) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].max.z = slicer->min.z - 1;
    block->min.z = slicer->min.z;
    added++;
  }
  if (block->max.z > slicer->max.z) {
    if (added >= max_cuboids) {
      return -1;
    }
    cuboids[added] = *block;
    cuboids[added].min.z = slicer->max.z + 1;
    block->max.z = slicer->max.z;
    added++;
  }
  return added;
}

static void print_cuboid_state(bool state, const cuboid_t *cuboids) {
  printf("%s x=%d..%d,y=%d..%d,z=%d..%d\n", state ? "on" : "off", cuboids->min.x, cuboids->max.x, cuboids->min.y,
         cuboids->max.y, cuboids->min.z, cuboids->max.z);
}

static int read_cuboid_state(bool *state, cuboid_t *cuboid) {
  char s[4];
  int read = scanf("%4s x=%d..%d,y=%d..%d,z=%d..%d\n", s, &cuboid->min.x, &cuboid->max.x, &cuboid->min.y,
                   &cuboid->max.y, &cuboid->min.z, &cuboid->max.z);
  if (read == EOF) {
    return 0;
  }
  if (read != 7) {
    fprintf(stderr, "Error: invalid cuboid state format\n");
    return -1;
  }
  if (strcmp(s, "on") == 0) {
    *state = true;
    return +1;
  }
  if (strcmp(s, "off") == 0) {
    *state = false;
    return +1;
  }
  fprintf(stderr, "Error: invalid state format\n");
  return -1;
}

static long run(int max_cuboids, cuboid_t cuboids[max_cuboids], const cuboid_t *initialize) {
  int num_cuboids = 0;

  bool state;
  cuboid_t cuboid;
  int read;
  while ((read = read_cuboid_state(&state, &cuboid)) > 0) {
    if ((initialize != NULL) && !is_cuboid_inside_other(&cuboid, initialize)) {
      continue;
    }

    int added = 0;
    cuboid_t *slices;
    for (int i = 0; i < num_cuboids; i++) {
      int sliced = slice_cuboid(&cuboids[i], &cuboid, max_cuboids - sliced, &cuboids[num_cuboids + added]);
      if (sliced < 0) {
        fprintf(stderr, "Error: cuboid limit exceeded\n");
        return -1;
      }
      added += sliced;
    }

    for (int i = 0; i < added; i++) {
      cuboids[i] = cuboids[num_cuboids + i];
    }
    num_cuboids = added;

    if (state) {
      if (num_cuboids >= max_cuboids) {
        fprintf(stderr, "Error: cuboid limit exceeded\n");
        return -1;
      }
      cuboids[num_cuboids++] = cuboid;
    }
  }
  if (read < 0) {
    return EXIT_FAILURE;
  }

  long area = 0;
  for (int i = 0; i < num_cuboids; i++) {
    area += measure_cuboid_area(&cuboids[i]);
  }

  printf("%ld\n", area);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_22(bool first) {
  const int MAX_CUBOIDS = 8192;
  cuboid_t *cuboids /*[MAX_CUBOIDS]*/;              // 192 KB
  cuboids = malloc(sizeof(cuboid_t) * MAX_CUBOIDS); // free'd at EXIT

  cuboid_t init = {.min = {-50, -50, -50}, .max = {+50, +50, +50}};
  return first ? run(MAX_CUBOIDS, cuboids, &init) : run(MAX_CUBOIDS, cuboids, NULL);
}

#define AOC_RUN_DAY aoc_run_2021_day_22
#include "main.c"
