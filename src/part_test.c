#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define PART_IMPL
#include "part.h"

inline static int cmp_ints(int a, int b) { return (a > b) - (a < b); }
inline static int cmp_int_ptrs(const void *a, const void *b) { return cmp_ints(*((const int *)a), *((const int *)b)); }

static bool qpart_test_at_ints(int k, int n, int *items /*[n]*/) {
  qpart(items, n, sizeof(int), k, cmp_int_ptrs);
  for (int i = 0; i < n; i++) {
    int cmp = cmp_ints(items[i], items[k]);
    if (i < k) {
      if (cmp > 0) {
        return false;
      }
    } else if (i > k) {
      if (cmp < 0) {
        return false;
      }
    } else {
      if (cmp != 0) {
        return false;
      }
    }
  }
  return true;
}

static bool qpart_test_ints(int n, int *items /*[n]*/) {
  for (int i = 0; i < n; i++) {
    if (!qpart_test_at_ints(i, n, items)) {
      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[argc + 1]) {
  int run = 0, success = 0;

  // odd array length
  run++;
  success += qpart_test_ints(5, (int[5]){7, 3, 4, 5, 6});

  // even array length
  run++;
  success += qpart_test_ints(6, (int[6]){7, 3, 4, 5, 6, 2});

  printf("%d / %d tests succeeded\n", success, run);
  return success == run ? EXIT_SUCCESS : EXIT_FAILURE;
}
