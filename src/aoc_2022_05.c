#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>


struct cargo_stack {
  int size;
  char supply[128];
};

struct procedure_step {
  int amount, from, to;
};

static int revert_cargo_stack(struct cargo_stack *stack) {
  for(int n=stack->size, i=0; i<--n; i++) {
    char tmp = stack->supply[i];
    stack->supply[i] = stack->supply[n];
    stack->supply[n] = tmp;
  }
}

static int read_cargo_stacks(int max_stacks,
                             struct cargo_stack stacks[static(max_stacks)]) {
  // set stack sizes to `0`
  for(int i=0; i<max_stacks; i++) {
    stacks[i].size = 0;
  }

  const int N = 1 + 4*max_stacks;
  char buffer[N+1];
  buffer[N] = 0;

  int height = 0;
  while (height < sizeof(stacks[0].supply)) {
    char *line = buffer;
    if (fgets(line, N, stdin) == NULL) { // EOF
      fprintf(stderr, "Error: expected stack ids after slices.\n");
      return -1;
    }
    int line_len = strlen(line);
    if (line[line_len-1] != '\n') {
      fprintf(stderr, "Error: expected stack input line with less than %d columns\n", N);
      return -1;
    }

    if (line_len < 2) { // empty line at the end
      int num_stacks = max_stacks;
      while(stacks[num_stacks-1].size == 0) {
        num_stacks--;
      }
      for(int i=0; i<num_stacks; i++) {
        revert_cargo_stack(&stacks[i]);
      }
      return num_stacks;
    } else if (line[1] != '1') { // not the final stack `id` line
      for (int i=0; (line_len > 3) && isspace(line[3]); i++) {
        if (sscanf(line, "[%c]", &(stacks[i].supply[stacks[i].size])) != 0) {
          stacks[i].size++;
        } else if (stacks[i].size != 0) {
          fprintf(stderr, "Error: stack can't have empty spaces in between\n");
          return -1;
        }
        line += 4;
        line_len -= 4;
      }
    }

    height++;
  }

  fprintf(stderr, "Error: maximum stack size exceeded.\n");
  return -1;
}

static int read_procedure_step(int num_stacks, struct procedure_step *step) {
  int read = scanf("move %u from %u to %u\n",
                   (unsigned int*) &step->amount,
                   (unsigned int*) &step->from,
                   (unsigned int*) &step->to);
  if (read == EOF) {
    return 0;
  }
  if (read != 3) {
    fprintf(stderr, "Error: invalid procedure_step.\n");
    return -1;
  }
  step->from--;
  step->to--;

  if (step->amount < 0) {
    fprintf(stderr, "Error: negative amount in procedure step.\n");
    return -1;
  }
  if ((step->from< 0) | (step->from>= num_stacks)) {
    fprintf(stderr, "Error: from stack out of range in procedure step.\n");
    return -1;
  }
  if ((step->to < 0) | (step->to >= num_stacks)) {
    fprintf(stderr, "Error: to stack out of range in procedure step.\n");
    return -1;
  }

  return 1;
}

static int move_cargo(const struct procedure_step *step,
                      struct cargo_stack *stacks) {
  if (stacks[step->from].size < step->amount) {
    fprintf(stderr, "Error: Stack underflow while moving creates.\n");
    return -1;
  }
  if (stacks[step->to].size + step->amount >= sizeof(stacks[step->to].supply)) {
    fprintf(stderr, "Error: Stack overflow while moving creates.\n");
    return +1;
  }

  stacks[step->from].size -= step->amount;
  for(int i=0; i<step->amount; i++) {
    stacks[step->to].supply[stacks[step->to].size++] =
      stacks[step->from].supply[stacks[step->from].size + i];
  }
  return 0;
}

static void print_cargo_stacks(int num_stacks,
                               const struct cargo_stack stacks[static(num_stacks)]) {
  int height = 0;
  for(int i=0; i<num_stacks; i++) {
    if (height < stacks[i].size) {
      height = stacks[i].size;
    }
  }

  for(int h=height-1; h>=0; h--) {
    int i=0;
    while(i<num_stacks) {
      if (h < stacks[i].size) {
        printf("[%c]", stacks[i].supply[h]);
      } else {
        printf("   ");
      }
      printf(((++i) == num_stacks) ? "\n" : " ");
    }
  }
}

static int run(bool multi) {
  static const int MAX_STACK_COUNT = 9;
  struct cargo_stack stacks[MAX_STACK_COUNT];

  int num_stacks = read_cargo_stacks(MAX_STACK_COUNT, stacks);
  if (num_stacks <       0) {
    return EXIT_FAILURE;
  }

  struct procedure_step step;
  int result;
  while((result = read_procedure_step(num_stacks, &step)) != 0) {
    if (result < 0) {
      return EXIT_FAILURE;
    }

    int moves;
    if (multi) { // move all together
      moves = 1;
    } else { // move each individually
      moves = step.amount;
      step.amount = 1;
    }

    for(int i=0; i<moves; i++) {
      int res = move_cargo(&step, stacks);
      if (res != 0) {
        return res;
      }
    }
  }

  // top of the stacks
  for(int i=0; i<num_stacks; i++) {
    if (stacks[i].size > 0) {
      printf("%c", stacks[i].supply[stacks[i].size - 1]);
    } else {
      printf(" ");
    }
  }
  printf("\n");
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_05(bool first) { return first ? run(false) : run(true); }

#define AOC_RUN_DAY aoc_run_2022_day_05
#include "main.c"
