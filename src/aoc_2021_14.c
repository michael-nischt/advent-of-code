#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int read_template(int n, char str[1 + n]) {
  str[n] = '\n';
  if (fgets(str, 1 + n, stdin) == NULL) {
    return 0;
  }
  if (str[n] != '\n') {
    fprintf(stderr, "Error: template character limit exceeded\n");
    return -1;
  }
  for (n = 0; str[n] != 0; n++) {
    if (isspace(str[n])) {
      str[n] = '\0';
      break;
    }
    if ((str[n] < 'A') | (str[n] > 'Z')) {
      fprintf(stderr, "Error: Invalid template symbol\n");
      return -1;
    }
  }
  return n;
}

static int read_rules(char min, char max, char rules[(1 + max - min) * (1 + max - min)]) {
  int num_elems = 1 + max - min;
  memset(rules, '\0', sizeof(char) * num_elems * num_elems);

  char left, right, inside;
  int read;
  while ((read = scanf("\n%c%c -> %c", &left, &right, &inside)) != EOF) {
    if (read != 3) {
      fprintf(stderr, "Error: invalid rule format\n");
      return -1;
    }
    if ((left < min) | (left > max) | (right < min) | (right > max) | (inside < min) | (inside > max)) {
      fprintf(stderr, "Error: invalid rule symbol\n");
      return -1;
    }

    int ndx = num_elems * (right - min) + (left - min);
    if (rules[ndx] != '\0') {
      fprintf(stderr, "Error: duplicate rule\n");
      return -1; // duplicate rule
    }
    rules[ndx] = inside - min;
  }

  return num_elems;
}

static int run(int len_template, char template[1 + len_template], char min, char max,
               char rules[(1 + max - min) * (1 + max - min)],
               long counts[(1 + max - min) + 2 * (1 + max - min) * (1 + max - min)], int steps) {
  if ((len_template = read_template(len_template, template)) <= 0) {
    return len_template < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
  }
  int num_symbols;
  if ((num_symbols = read_rules(min, max, rules)) < 0) {
    return EXIT_FAILURE;
  }

  size_t num_rules = (num_symbols * num_symbols);
  memset(counts, 0, sizeof(long) * (num_symbols + num_rules));
  if (len_template > 0) {
    counts[template[0] - min]++;
  }
  for (int i = 1; i < len_template; i++) {
    int l = template[i - 1] - min;
    int r = template[i] - min;
    counts[r]++;
    int pair = num_symbols * r + l;
    counts[num_symbols + pair]++;
  }

  long *copy = &counts[num_symbols + num_rules];
  for (int step = 0; step < steps; step++) {
    memcpy(copy, &counts[num_symbols], sizeof(long) * num_rules); // don't count newly inserted until the next step
    for (int i = 0; i < num_rules; i++) {
      int l = (i % num_symbols);
      int r = (i / num_symbols);
      int pair = num_symbols * r + l;
      long cnt = copy[pair];

      int m = rules[pair];
      int l_pair = num_symbols * m + l;
      int r_pair = num_symbols * r + m;

      counts[m] += cnt;
      counts[num_symbols + pair] -= cnt;
      counts[num_symbols + l_pair] += cnt;
      counts[num_symbols + r_pair] += cnt;
    }
  }

  long min_symbol = LONG_MAX, max_symbol = LONG_MIN;
  for (int i = 0; i < num_symbols; i++) {
    if (counts[i] == 0) {
      continue;
    }
    if (counts[i] < min_symbol) {
      min_symbol = counts[i];
    }
    if (counts[i] > max_symbol) {
      max_symbol = counts[i];
    }
  }
  printf("%ld\n", max_symbol - min_symbol);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_14(bool first) {
  const int MAX_TEMPLATE = 32;
  const char MIN_SYMBOL = 'A', MAX_SYMBOL = 'Z';
  const int NUM_SYMBOLS = 1 + MAX_SYMBOL - MIN_SYMBOL;
  const int NUM_RULES = NUM_SYMBOLS * NUM_SYMBOLS;
  char template[1 + MAX_TEMPLATE];          //    33 B
  char rules[NUM_RULES];                    //   676 B
  long counts[NUM_SYMBOLS + 2 * NUM_RULES]; // 11024 B

  return first ? run(MAX_TEMPLATE, template, MIN_SYMBOL, MAX_SYMBOL, rules, counts, 10 /*steps*/)
               : run(MAX_TEMPLATE, template, MIN_SYMBOL, MAX_SYMBOL, rules, counts, 40 /*steps*/);
}

#define AOC_RUN_DAY aoc_run_2021_day_14
#include "main.c"
