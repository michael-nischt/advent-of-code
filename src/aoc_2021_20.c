#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct aoc20_algorithm {
  uint64_t data[8];
} aoc20_algorithm_t;

inline static int get_algorithm_bit(const aoc20_algorithm_t *algorithm, int bit) {
  const int stride = sizeof(algorithm->data[0]) * 8;
  size_t i = bit / stride;
  bit %= stride;
  return 1 & (algorithm->data[i] >> bit);
}

inline static void set_algorithm_bit(aoc20_algorithm_t *algorithm, int bit) {
  const int stride = sizeof(algorithm->data[0]) * 8;
  size_t i = bit / stride;
  bit %= stride;
  algorithm->data[i] |= 1L << bit;
}

inline static void clear_algorithm_bit(aoc20_algorithm_t *algorithm, int bit) {
  const int stride = sizeof(algorithm->data[0]) * 8;
  size_t i = bit / stride;
  bit %= stride;
  algorithm->data[i] &= ~(1L << bit);
}

inline static int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

static int enhance_image(const aoc20_algorithm_t *algorithm, int max_pixels, uint8_t image[max_pixels], int *width,
                         int *height, bool infinity) {
  int w = *width, h = *height;
  *width += 2;
  *height += 2;

  int num_input_pixels = w * h;
  int num_pixels = *width * *height;
  size_t mem = num_pixels + num_input_pixels + 1;
  if (max_pixels < mem) {
    return -1;
  }

  // copy image to input (double buffering)
  uint8_t *input_image = &image[num_pixels];
  memcpy(input_image, image, sizeof(uint8_t) * num_input_pixels);
  input_image[num_input_pixels] = infinity; // set infinite pixel

  for (int i = 0; i < num_pixels; i++) {
    int x = (i % *width) - 1;
    int y = (i / *width) - 1;

    int bit = 0;
    bit |= input_image[image_index(w, h, x - 1, y - 1)] << 8;
    bit |= input_image[image_index(w, h, x, y - 1)] << 7;
    bit |= input_image[image_index(w, h, x + 1, y - 1)] << 6;
    bit |= input_image[image_index(w, h, x - 1, y)] << 5;
    bit |= input_image[image_index(w, h, x, y)] << 4;
    bit |= input_image[image_index(w, h, x + 1, y)] << 3;
    bit |= input_image[image_index(w, h, x - 1, y + 1)] << 2;
    bit |= input_image[image_index(w, h, x, y + 1)] << 1;
    bit |= input_image[image_index(w, h, x + 1, y + 1)] << 0;

    image[i] = get_algorithm_bit(algorithm, bit);
  }
  return num_pixels;
}

static void print_algorithm(const aoc20_algorithm_t *algorithm) {
  for (int i = 0; i < 512; i++) {
    printf("%c", get_algorithm_bit(algorithm, i) == 0 ? '.' : '#');
  }
  printf("\n");
}

static void print_image(int width, int height, const uint8_t pixels[width * height]) {
  int num_pixels = width * height;
  for (int i = 0; i < num_pixels; i++) {
    if ((i % width == 0) & (i / width > 0)) {
      printf("\n");
    }
    printf("%c", pixels[i] == 0 ? '.' : '#');
  }
  printf("\n");
}

static int read_algorithm(aoc20_algorithm_t *algorithm) {
  int on = 0, c;
  for (int bit = 0; bit < 512; bit++) {
    if ((c = fgetc(stdin)) == EOF) {
      return false;
    }
    if (c == '#') {
      set_algorithm_bit(algorithm, bit);
      on++;
    } else if (c == '.') {
      clear_algorithm_bit(algorithm, bit);
    } else {
      fprintf(stderr, "Error: Illegal algorithm symbol\n");
      return -1;
    }
  }
  return on;
}

static int read_image(int max_pixels, uint8_t pixels[max_pixels], int *width, int *height) {
  int num_pixels = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (num_pixels != 0) {
        if (*width == 0) {
          *width = num_pixels;
        }
        if (num_pixels % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c == '.') | (c == '#')) {
      space = false;
      if (num_pixels >= max_pixels) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      pixels[num_pixels++] = c != '.';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return num_pixels;
}

static long run(int max_pixels, uint8_t pixels[max_pixels], int steps) {
  aoc20_algorithm_t algorithm;
  int on = read_algorithm(&algorithm);
  if (on < 0) {
    return EXIT_FAILURE;
  }

  // // prevent over saturation
  bool swap = on > 256; // NOTE(micha): is this the right indicator?

  int width, height;
  int num_pixels = read_image(max_pixels, pixels, &width, &height);
  if (num_pixels < 0) {
    return EXIT_FAILURE;
  }

  bool infinity = false;
  while (steps-- > 0) {
    if (enhance_image(&algorithm, max_pixels, pixels, &width, &height, infinity) < 0) {
      fprintf(stderr, "Error: image buffer limit exceeded during enhancement (double buffering)\n");
      return EXIT_FAILURE;
    }
    if (swap) {
      infinity = !infinity;
    }
  }

  long lit = 0;
  num_pixels = width * height;
  for (int i = 0; i < num_pixels; i++) {
    lit += pixels[i];
  }

  printf("%ld\n", lit);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_20(bool first) {
  const int MAX_PIXELS = 512 * 512;
  uint8_t *pixels /*[MAX_PIXELS]*/;              // 256 KB
  pixels = malloc(sizeof(uint8_t) * MAX_PIXELS); // free'd at EXIT

  return first ? run(MAX_PIXELS, pixels, 2 /*steps*/) : run(MAX_PIXELS, pixels, 50 /*steps*/);
}

#define AOC_RUN_DAY aoc_run_2021_day_20
#include "main.c"
