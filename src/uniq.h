#ifndef UNIQ_H
#define UNIQ_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef UNIQ_STATIC
#define UNIQ_API static
#else
#define UNIQ_API extern
#endif

// return the number of unique items in a sorted
// moves those to the front; remaining elements are undefined
size_t suniq(void *base, size_t nitems, size_t size);

#ifdef UNIQ_IMPL

UNIQ_API size_t suniq(void *base, size_t nitems, size_t size) {
  uint8_t *previous = (uint8_t *) base, *current;
  uint8_t *end = previous + size * nitems, *dupl = previous + size;
  while ((current = previous + size) < end) {
    if (memcmp(previous, current, size) != 0) {
      memcpy(dupl, current, size);
      dupl += size;
    }
    previous += size;
  }
  return (dupl - (uint8_t *) base) / size;
}

#endif // UNIQ_IMPL

#ifdef __cplusplus
}
#endif

#endif // UNIQ_H
