#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void simulate_player_universes(int position, int score, long universes, int turns, long turn_results[turns][2]) {
  if (turns-- <= 0) {
    return;
  }
  long *results = turn_results++[0];

  const static int NO_WIN = 0, WIN = 1;
  const static int WIN_SCORE = 21;
  const static int FIELDS = 10;
  // only simulate the distinc die rolls and split as many times these occur; instead of all 27
  const static int DIE_3X3_SCORES[7] = {3, 4, 5, 6, 7, 8, 9}; // distinct values
  const static int DIE_3X3_COUNTS[7] = {1, 3, 6, 7, 6, 3, 1}; // counts sum up to 3*3*3 == 27
  for (int i = 0; i < 7; i++) {
    int new_position = position + DIE_3X3_SCORES[i];
    new_position = 1 + ((new_position - 1) % FIELDS);
    long new_score = score + new_position;
    long new_universes = universes * DIE_3X3_COUNTS[i]; // split universes
    if (new_score >= WIN_SCORE) {
      results[WIN] += new_universes;
    } else {
      results[NO_WIN] += new_universes;
      simulate_player_universes(new_position, new_score, new_universes, turns, turn_results);
    }
  }
}

static bool read_starting_spaces(int positions[2]) {
  if (scanf("Player 1 starting position: %d\n", &positions[0]) != 1) {
    fprintf(stderr, "Error: invalid player 1 starting position format\n");
    return false;
  }
  if ((positions[0] < 1) | (positions[0] > 10)) {
    fprintf(stderr, "Error: invalid player 1 starting position\n");
    return false;
  }
  if (scanf("Player 2 starting position: %d\n", &positions[1]) != 1) {
    fprintf(stderr, "Error: invalid player 2 starting position format\n");
    return false;
  }
  if ((positions[1] < 1) | (positions[1] > 10)) {
    fprintf(stderr, "Error: invalid player 2 starting position\n");
    return false;
  }
  return true;
}

static int run01() {
  int positions[2];
  if (!read_starting_spaces(positions)) {
    return EXIT_FAILURE;
  }

  const int WIN_SCORE = 1000;
  const int DIE_VALUES = 100;
  const int FIELDS = 10;
  int turns = 0, roll = 0, steps, i = -1;
  int scores[2] = {0, 0};
  while ((scores[0] < WIN_SCORE) & (scores[1] < WIN_SCORE)) {
    i = (turns++ % 2);
    steps = 0;
    steps += ++roll % DIE_VALUES;
    steps += ++roll % DIE_VALUES;
    steps += ++roll % DIE_VALUES;
    steps %= FIELDS; // just to keep the next intermediate value small
    positions[i] += steps;
    positions[i] = 1 + ((positions[i] - 1) % FIELDS);
    scores[i] += positions[i];
  }

  int result = scores[(i + 1) % 2] * roll;
  printf("%d\n", result);
  return EXIT_SUCCESS;
}

static int run02() {
  int positions[2];
  if (!read_starting_spaces(positions)) {
    return EXIT_FAILURE;
  }

  // We count the possible wins & no wins per round _per player_
  // and combine (multiply) those with the other player's rounds afterwards.
  // Saves 27-splits _each round_!
  const int NO_WIN = 0, WIN = 1;
  const int MAX_TURNS = 14; // max 2 * 21/3=7 turns for each players (ignoring the position part of the score)
  long p0_turn_results[MAX_TURNS][2 /*NO_WIN=0;WIN=1*/];
  long p1_turn_results[MAX_TURNS][2 /*NO_WIN=0;WIN=1*/];
  memset(p0_turn_results, 0, sizeof(p0_turn_results));
  memset(p1_turn_results, 0, sizeof(p1_turn_results));
  simulate_player_universes(positions[0], 0, 1, MAX_TURNS, p0_turn_results);
  simulate_player_universes(positions[1], 0, 1, MAX_TURNS, p1_turn_results);

  long p0_wins = 0, p1_wins = 0; // it's impossible that any players wins after its first turn
  for (int turn = 1; turn < MAX_TURNS; turn++) {
    p0_wins += p0_turn_results[turn][WIN] * p1_turn_results[turn - 1][NO_WIN];
    p1_wins += p1_turn_results[turn][WIN] * p0_turn_results[turn][NO_WIN];
  }

  long result = p0_wins > p1_wins ? p0_wins : p1_wins;
  printf("%ld\n", result);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_21(bool first) { return first ? run01() : run02(); }

#define AOC_RUN_DAY aoc_run_2021_day_21
#include "main.c"
