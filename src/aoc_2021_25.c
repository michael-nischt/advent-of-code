#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static int step_image_x(int width, int height, uint8_t image[width * height]) {
  int moves = 0;

  int i, j;
  bool free, skip;
  for (int y = 0; y < height; y++) {
    free = image[width * y] == 0;
    skip = false;

    for (int x = 1; x < width; x++) {
      if (skip) {
        skip = false;
        continue;
      }
      j = width * y + x;
      i = j - 1;
      if ((image[i] == 1) & (image[j] == 0)) {
        image[j] = image[i];
        image[i] = 0;
        skip = true;
        moves++;
      }
    }

    i = width * y + (width - 1);
    if ((image[i] == 1) & free & !skip) {
      j = width * y;
      image[j] = image[i];
      image[i] = 0;
      moves++;
    }
  }

  return moves;
}

static int step_image_y(int width, int height, uint8_t image[width * height]) {
  int moves = 0;

  int i, j;
  bool free, skip;
  for (int x = 0; x < width; x++) {
    free = image[x] == 0;
    skip = false;

    for (int y = 1; y < height; y++) {
      if (skip) {
        skip = false;
        continue;
      }
      j = width * y + x;
      i = j - width;
      if ((image[i] == 2) & (image[j] == 0)) {
        image[j] = image[i];
        image[i] = 0;
        skip = true;
        moves++;
      }
    }

    i = width * (height - 1) + x;
    if ((image[i] == 2) & free & !skip) {
      j = x;
      image[j] = image[i];
      image[i] = 0;
      moves++;
    }
  }

  return moves;
}

static void print_image(int width, int height, uint8_t image[width * height]) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      uint8_t p = image[i++];
      printf("%c", p == 2 ? 'v' : p == 1 ? '>' : '.');
    }
    printf("\n");
  }
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height) {
  int image_size = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c == 'v') | (c == '>') | (c == '.')) {
      space = false;
      if (image_size == max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      image[image_size++] = (c == 'v') ? 2 : (c == '>') ? 1 : 0;
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run(int image_size, uint8_t image[image_size]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  bool unstable;
  int steps = 0;
  do {
    steps++;
    unstable = step_image_x(width, height, image) | step_image_y(width, height, image);
  } while (unstable);

  printf("%d\n", steps);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_25(bool first) {
  const int IMAGE_MAX_WIDTH = 160, IMAGE_MAX_HEIGHT = 160;
  uint8_t image[IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT]; // 25 KB

  return run(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image);
}

#define AOC_RUN_DAY aoc_run_2021_day_25
#include "main.c"
