#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct vec2 {
  int x, y;
} vec2_t;

typedef struct line2 {
  vec2_t from, to;
} line2_t;

static void print_image(int width, int height, int image[width * height]) {
  for (int i = 0, y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      printf("%d", image[i++]); // assuming no pixels > 9
    }
    printf("\n");
  }
}

static int image_histogram(int width, int height, int image[width * height], int threshold) {
  int danger = 0;
  for (int i = width * height - 1; i >= 0; i--) {
    danger += image[i] > threshold;
  }
  return danger;
}

inline static void clear_image(int width, int height, int image[width * height]) {
  memset(image, 0, sizeof(int) * width * height);
}

static void draw_image_lines(int num_lines, const line2_t lines[num_lines], int width, int height,
                             int image[width * height], bool diagonal) {
  for (int i_line = 0; i_line < num_lines; i_line++) {
    line2_t line = lines[i_line];

    int x, y, d, tmp;
    if (line.from.x == line.to.x) { // vertical
      if (line.from.y > line.to.y) {
        tmp = line.from.y;
        line.from.y = line.to.y;
        line.to.y = tmp;
      }
      x = line.from.x;
      for (y = line.from.y; y <= line.to.y; y++) {
        image[width * y + x]++;
      }
    } else if (line.from.y == line.to.y) { // horizontal
      if (line.from.x > line.to.x) {
        tmp = line.from.x;
        line.from.x = line.to.x;
        line.to.x = tmp;
      }
      y = line.from.y;
      for (x = line.from.x; x <= line.to.x; x++) {
        image[width * y + x]++;
      }
    } else if (diagonal) {
      if (line.from.x > line.to.x) {
        tmp = line.from.x;
        line.from.x = line.to.x;
        line.to.x = tmp;
        tmp = line.from.y;
        line.from.y = line.to.y;
        line.to.y = tmp;
      }
      d = (line.to.y - line.from.y) / (line.to.x - line.from.x);
      y = line.from.y;
      for (x = line.from.x; x <= line.to.x; x++) {
        image[width * y + x]++;
        y += d;
      }
    }
  }
}

static int assess_danger(int num_lines, const line2_t lines[num_lines], int width, int height,
                         int image[width * height], int safe, bool diagonal) {
  clear_image(width, height, image);
  draw_image_lines(num_lines, lines, width, height, image, diagonal);
  return image_histogram(width, height, image, safe);
}

static int read_lines(int max_lines, line2_t lines[max_lines]) {
  int num_lines = 0;

  line2_t line;
  int read;
  while ((read = scanf("%d,%d -> %d,%d", &line.from.x, &line.from.y, &line.to.x, &line.to.y)) != EOF) {
    if (read != 4) {
      fprintf(stderr, "Error: illegal line format\n");
      return -1;
    }
    // copy line
    if (num_lines >= max_lines) {
      fprintf(stderr, "Error: line limit exceeded\n");
      return -1;
    }
    lines[num_lines++] = line;
  }

  return num_lines;
}

static int run(int num_lines, line2_t lines[num_lines], int grid_x, int grid_y, int grid[grid_x * grid_y],
               bool diagonal) {
  if ((num_lines = read_lines(num_lines, lines)) < 0) {
    return EXIT_FAILURE;
  }

  int danger = assess_danger(num_lines, lines, grid_x, grid_y, grid, 1, diagonal);
  printf("%d\n", danger);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_05(bool first) {
  const int MAX_LINES = 512;
  const int MAX_GRID_X = 1024;
  const int MAX_GRID_Y = 1024;
  line2_t lines[MAX_LINES];                               // 8 KB
  int *grid /*[MAX_GRID_X * MAX_GRID_Y]*/;                // 4 MB
  grid = malloc(sizeof(int) * (MAX_GRID_X * MAX_GRID_Y)); // free'd at EXIT

  return first ? run(MAX_LINES, lines, MAX_GRID_X, MAX_GRID_Y, grid, false /*axis-only*/)
               : run(MAX_LINES, lines, MAX_GRID_X, MAX_GRID_Y, grid, true /*diagonal*/);
}

#define AOC_RUN_DAY aoc_run_2021_day_05
#include "main.c"
