#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <string.h>
#include <ctype.h>


static void print_image(int width, int height, const uint8_t image[width * height]) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      printf("%d", image[i++]);
    }
    printf("\n");
  }
}

static inline int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height) {
  int image_size = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c >= '0') & (c <= '9')) {
      space = false;
      if (image_size >= max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      image[image_size++] = c - '0';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run_01(int image_size, uint8_t image[image_size + 1]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int score = 0;
  // O(image_size) since inner loops have a maximum of 9 iterations
  for (int i=0; i<image_size; i++) {
    int x = i % width, y = i / width;
    bool visible;

    // top
    visible = true;
    for (int y_prev=y-1; (visible) & (y_prev >= 0); y_prev--) {
      int j = image_index(width, height, x, y_prev);
      visible = image[j] < image[i];
    }
    if (visible) {
      score++;
      continue;
    }
    // left
    visible = true;
    for (int x_prev=x-1; (visible) & (x_prev >= 0); x_prev--) {
      int j = image_index(width, height, x_prev, y);
      visible = image[j] < image[i];
    }
    if (visible) {
      score++;
      continue;
    }
    // down
    visible = true;
    for (int y_next=y+1; (visible) & (y_next < height); y_next++) {
      int j = image_index(width, height, x, y_next);
      visible = image[j] < image[i];
    }
    if (visible) {
      score++;
      continue;
    }
    // right
    visible = true;
    for (int x_next=x+1; (visible) & (x_next < width); x_next++) {
      int j = image_index(width, height, x_next, y);
      visible = image[j] < image[i];
    }
    if (visible) {
      score++;
      continue;
    }
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02(int image_size, uint8_t image[image_size + 1]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int score = 0;
  // O(image_size) since inner loops have a maximum of 9 iterations
  for (int i=0; i<image_size; i++) {
    int x = i % width, y = i / width;

    int top = 0;
    for (int y_prev = y-1; y_prev >= 0; y_prev--) {
      int j = image_index(width, height, x, y_prev);
      top = y - y_prev;
      if (image[j] >= image[i]) {
        break;
      }
    }
    int left = 0;
    for (int x_prev = x-1; x_prev >= 0; x_prev--) {
      int j = image_index(width, height, x_prev, y);
      left = x - x_prev;
      if (image[j] >= image[i]) {
        break;
      }
    }
    int bot = 0;
    for (int y_next = y+1; y_next < height; y_next++) {
      int j = image_index(width, height, x, y_next);
      bot = y_next - y;
      if (image[j] >= image[i]) {
        break;
      }
    }
    int right = 0;
    for (int x_next = x+1; x_next < width; x_next++) {
      int j = image_index(width, height, x_next, y);
      right = x_next - x;
      if (image[j] >= image[i]) {
        break;
      }
    }

    int val = left*right*top*bot;
    if (val > score) {
      score = val;
    }
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_08(bool first) {
  const int IMAGE_MAX_WIDTH = 100, IMAGE_MAX_HEIGHT = 100;
  uint8_t *image /*[IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT + 1]*/;
  image = malloc(sizeof(uint8_t) * (IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT + 1)); // free'd at EXIT

  return first ? run_01(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image) : run_02(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image);
}

#define AOC_RUN_DAY aoc_run_2022_day_08
#include "main.c"
