#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct vec2 {
  int x, y;
} vec2_t;

typedef struct aabox2 {
  vec2_t min, max;
} aabox2_t;

static bool hits_target(aabox2_t target, vec2_t vel) {
  vec2_t pos = {0, 0};
  int x_dir = (pos.x < target.min.x) - (pos.x > target.max.x);
  bool hit = false;
  while (!hit & (pos.y > target.min.y)) {
    pos.x += vel.x;
    pos.y += vel.y;
    vel.x -= x_dir;
    x_dir *= vel.x != 0;
    vel.y--;
    hit = (target.min.x <= pos.x) & (pos.x <= target.max.x) & (target.min.y <= pos.y) & (pos.y <= target.max.y);
  }
  return hit;
}

static int read_target_area(aabox2_t *area) {
  if (scanf("target area: x=%d..%d, y=%d..%d", &area->min.x, &area->max.x, &area->min.y, &area->max.y) != 4) {
    fprintf(stderr, "Error: target area format invalid\n");
    return -1;
  }
  return (1 + area->max.x - area->min.x) * (1 + area->max.y - area->min.y);
}

static int run01() {
  aabox2_t target;
  if (read_target_area(&target) < 1) {
    return EXIT_FAILURE;
  }

  int n = -target.min.y - 1; // steps above start=0
  n = (n * (n + 1)) / 2;     // sum_over(n)

  printf("%d\n", n);
  return EXIT_SUCCESS;
}

static int run02() {
  aabox2_t target;
  if (read_target_area(&target) < 1) {
    return EXIT_FAILURE;
  }

  long hits = 0;
  vec2_t vel;
  for (vel.y = -target.min.y; vel.y >= target.min.y; vel.y--) {
    for (vel.x = target.min.x; vel.x < 0; vel.x++) {
      hits += hits_target(target, vel);
    }
    for (vel.x = 0; vel.x <= target.max.x; vel.x++) {
      hits += hits_target(target, vel);
    }
  }

  printf("%ld\n", hits);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_17(bool first) { return first ? run01() : run02(); }

#define AOC_RUN_DAY aoc_run_2021_day_17
#include "main.c"
