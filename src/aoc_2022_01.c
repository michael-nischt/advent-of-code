#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static int run_01() {
  int max = 0, elf = 0, food;
  char buffer[8];
  bool line;

  do {
    line = fgets(buffer, sizeof(buffer), stdin) != NULL;
    if (line && sscanf(buffer, "%d\n", &food) == 1) {
      elf += food;
    } else {
      if (elf > max) {
        max = elf;
      }
      elf = 0;
    }
  } while(line);

  printf("%d\n", max);
  return EXIT_SUCCESS;
}

static int run_02() {
  int top[3] = { 0 };
  int elf = 0, food;
  char buffer[8];
  bool line;

  do {
    line = fgets(buffer, sizeof(buffer), stdin) != NULL;
    if (line && (sscanf(buffer, "%d\n", &food) == 1)) {
      elf += food;
    } else {
      if (elf > top[0]) {
        top[2] = top[1];
        top[1] = top[0];
        top[0] = elf;
      } else if (elf > top[1]) {
        top[2] = top[1];
        top[1] = elf;
      } else if (elf > top[2]) {
        top[2] = elf;
      }
      elf = 0;
    }
  } while(line);

  printf("%d\n", top[0] + top[1] + top[2]);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_01(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_01
#include "main.c"
