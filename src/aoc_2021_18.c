#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct number {
  int left, right; // leaf-node if left == right
} number_t;

typedef struct damage {
  int left, right, depth;
} damage_t;

typedef struct sum {
  int capacity, count, free, root;
  number_t *numbers;
  int *stack; // re-use deleted; count still includes these to keep region
} sum_t;

inline static void set_number(number_t *num, int val) { num->right = num->left = val; }
inline static void clear_number(number_t *num) { set_number(num, 0); }
inline static void add_number(number_t *num, int val) { num->right = num->left += val; }
inline static bool is_zero(const number_t *num) { return (num->left == 0) & (num->right == 0); }
inline static void set_link(number_t *num, int left, int right) {
  num->left = left;
  num->right = right;
}

static int find_left_number(number_t *numbers, int index) {
  while (numbers[index].left != numbers[index].right) {
    index = numbers[index].left;
  }
  return index;
}
static int find_right_number(number_t *numbers, int index) {
  while (numbers[index].left != numbers[index].right) {
    index = numbers[index].right;
  }
  return index;
}
static int find_explosion(number_t *numbers, int index, int depth, damage_t *dmg) {
  if (numbers[index].left == numbers[index].right) {
    return -1;
  }
  if (depth >= 4) {
    dmg->depth = depth;
    return index;
  }

  int res;
  if ((res = find_explosion(numbers, numbers[index].left, 1 + depth, dmg)) >= 0) {
    if (depth == dmg->depth) {
      dmg->right = numbers[index].right;
    } else if (dmg->right < 0) {
      dmg->right = find_left_number(numbers, numbers[index].right);
    }
    return res;
  }
  if ((res = find_explosion(numbers, numbers[index].right, 1 + depth, dmg)) >= 0) {
    if (depth == dmg->depth) {
      dmg->left = numbers[index].left;
    } else if (dmg->left < 0) {
      dmg->left = find_right_number(numbers, numbers[index].left);
    }
    return res;
  }
  return -1;
}

static int find_split(struct number *numbers, int index) {
  if ((numbers[index].left == numbers[index].right)) {
    return (numbers[index].left > 9) ? index : -1;
  }

  int res;
  if ((res = find_split(numbers, numbers[index].left)) >= 0) {
    return res;
  }
  if ((res = find_split(numbers, numbers[index].right)) >= 0) {
    return res;
  }
  return -1;
}

static int copy_sum(int n, number_t numbers[n], int offset, int dst, int src) {
  if (dst >= n) {
    return -1;
  }
  numbers[dst].left = numbers[src].left;
  numbers[dst].right = numbers[src].right;
  if (numbers[dst].left == numbers[dst].right) {
    return 1;
  }
  numbers[dst].left += offset;
  numbers[dst].right += offset;
  int left, right;
  if ((left = copy_sum(n, numbers, offset, numbers[dst].left, numbers[src].left)) < 0) {
    return -1;
  }
  if ((right = copy_sum(n, numbers, offset, numbers[dst].right, numbers[src].right)) < 0) {
    return -1;
  }
  return 1 + left + right;
}

static long number_magnitude(number_t *numbers, int index) {
  if (numbers[index].left == numbers[index].right) {
    return numbers[index].left;
  }
  long l = number_magnitude(numbers, numbers[index].left);
  long r = number_magnitude(numbers, numbers[index].right);
  return 3 * l + 2 * r;
}

inline static int new_number(sum_t *sum, int val) {
  if (sum->count - sum->free >= sum->capacity) {
    return -1;
  }
  int index = sum->free > 0 ? sum->stack[--sum->free] : sum->count++;
  set_number(&sum->numbers[index], val);
  return index;
}
inline static int new_link(sum_t *sum, int left, int right) {
  if (sum->count - sum->free >= sum->capacity) {
    return -1;
  }
  int index = sum->free > 0 ? sum->stack[--sum->free] : sum->count++;
  set_link(&sum->numbers[index], left, right);
  return index;
}
inline static bool delete_number(sum_t *sum, int index) {
  if (!is_zero(&sum->numbers[sum->numbers[index].left]) | !is_zero(&sum->numbers[sum->numbers[index].right])) {
    return false;
  }
  sum->stack[sum->free++] = sum->numbers[index].left;
  sum->stack[sum->free++] = sum->numbers[index].right;
  clear_number(&sum->numbers[index]);
  return true;
}

static bool explode_sum(sum_t *sum) {
  damage_t dmg = {.left = -1, .right = -1};
  int i = find_explosion(sum->numbers, sum->root, 0, &dmg);
  if (i < 0) {
    return false;
  }

  // apply damage
  if (dmg.left >= 0) {
    add_number(&sum->numbers[dmg.left], sum->numbers[sum->numbers[i].left].left);
  }
  if (dmg.right >= 0) {
    add_number(&sum->numbers[dmg.right], sum->numbers[sum->numbers[i].right].right);
  }
  // clear explosion
  clear_number(&sum->numbers[sum->numbers[i].left]);
  clear_number(&sum->numbers[sum->numbers[i].right]);
  delete_number(sum, i);
  return true;
}

static int split_sum(sum_t *sum) {
  int i = find_split(sum->numbers, sum->root);
  if (i < 0) {
    return 0;
  }

  int val = sum->numbers[i].left;
  sum->numbers[i].left = new_number(sum, val / 2);
  if (sum->numbers[i].left < 0) {
    return -1;
  }
  sum->numbers[i].right = new_number(sum, val / 2 + val % 2);
  if (sum->numbers[i].right < 0) {
    return -1;
  }
  return 1;
}

static int reduce_sum(sum_t *sum) {
  int n = 0, res;
  while (true) {
    if (explode_sum(sum)) {
      n++;
      continue;
    }
    if ((res = split_sum(sum)) < 0) {
      return -1;
    }
    if (res == 0) {
      break;
    }
    n++;
  }
  return n;
}

static int read_expression(int max_numbers, number_t numbers[max_numbers], int stack[max_numbers], int start) {
  int n = 0, m = 0, c;
  bool digit = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (c == '[') {
      if (n == max_numbers) {
        return -1;
      }
      stack[m++] = n++; // push
      digit = false;
    } else if (c == ']') {
      --m; // pop
      digit = false;
    } else if (c == ',') {
      c = stack[m - 1];
      numbers[c].left = c + 1 + start;
      numbers[c].right = n + start; // next node
      digit = false;
    } else if ((c >= '0') & (c <= '9')) {
      if (digit) {
        set_number(&numbers[n - 1], 10 * numbers[n - 1].left + c - '0');
      } else {
        if (n == max_numbers) {
          return -1;
        }
        set_number(&numbers[n++], c - '0');
        digit = true;
      }
    } else if (isspace(c)) {
      return n;
    } else {
      fprintf(stderr, "Error: invalid expression symbol\n");
      return -1;
    }
  }
  return n;
}

static void print_numbers(const number_t *numbers, int index) {
  if (numbers[index].left == numbers[index].right) {
    printf("%d", numbers[index].left);
    return;
  }
  printf("[");
  print_numbers(numbers, numbers[index].left);
  printf(",");
  print_numbers(numbers, numbers[index].right);
  printf("]");
}

static int run01(int max_numbers, number_t numbers[max_numbers], int stack[max_numbers]) {
  sum_t sum = {.capacity = max_numbers, .numbers = numbers, .stack = stack};
  int num_numbers, left = 0;
  while ((num_numbers = read_expression(max_numbers - sum.count, &numbers[sum.count], &stack[sum.free], sum.count)) >
         0) {
    int left = sum.root;
    sum.root = sum.count;
    sum.count += num_numbers;
    if (sum.count == num_numbers) {
      continue;
    }
    sum.root = new_link(&sum, left, sum.root);
    if (sum.root < 0) {
      return EXIT_FAILURE;
    }
    if (reduce_sum(&sum) < 0) {
      return EXIT_FAILURE;
    }
  }
  if (num_numbers < 0) {
    return EXIT_FAILURE;
  }

  printf("%ld\n", number_magnitude(sum.numbers, sum.root));
  return EXIT_SUCCESS;
}

static int run02(int max_numbers, number_t numbers[max_numbers], int stack[max_numbers], int max_sums,
                 int sums[max_sums]) {
  int num_sums = 0, total_numbers = 0, num_numbers;
  while ((num_numbers = read_expression(max_numbers - total_numbers, &numbers[total_numbers], stack, total_numbers)) >
         0) {
    sums[num_sums++] = total_numbers;
    total_numbers += num_numbers;
  }
  if (num_numbers < 0) {
    return -1;
  }

  long result = 0;
  for (int i = 0; i < num_sums; i++) {
    for (int j = 0; j < num_sums; j++) { // snail number addition is not commutative
      if (i == j) {
        continue;
      }

      int m, n;
      if ((m = copy_sum(max_numbers, numbers, total_numbers - sums[i], total_numbers, sums[i])) < 0) {
        return -1;
      }
      if ((n = copy_sum(max_numbers, numbers, total_numbers + m - sums[j], total_numbers + m, sums[j])) < 0) {
        return -1;
      }
      int root = total_numbers + m + n;
      if (root >= max_numbers) {
        return -1;
      }

      sum_t sum = {.capacity = max_numbers, .count = root + 1, .root = root, .numbers = numbers, .stack = stack};
      numbers[root].left = total_numbers;
      numbers[root].right = total_numbers + m;
      if (reduce_sum(&sum) < 0) {
        return -1;
      }

      long mag = number_magnitude(sum.numbers, sum.root);
      if (mag > result) {
        result = mag;
      }
    }
  }
  printf("%ld\n", result);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_18(bool first) {
  const int MAX_NUMBERS = 4096;
  const int MAX_SUMS = 128;
  number_t numbers[MAX_NUMBERS]; // 32 KB
  int stack[MAX_NUMBERS];        // 16 KB
  int sums[MAX_SUMS];            // 512 B

  return first ? run01(MAX_NUMBERS, numbers, stack) : run02(MAX_NUMBERS, numbers, stack, MAX_SUMS, sums);
}

#define AOC_RUN_DAY aoc_run_2021_day_18
#include "main.c"
