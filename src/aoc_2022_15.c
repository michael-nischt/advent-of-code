#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define UNIQ_IMPL
#include "uniq.h"

struct range {
  int from, to; // inclusive
};

inline static int cmp_int(int a, int b) {
  return (a > b) - (a < b);
}

static int cmp_position_fun(const void* a, const void* b) {
  int x = *(const int*) a;
  int y = *(const int*) b;
  return cmp_int(x, y);
}

static int cmp_range_fun(const void* a, const void* b) {
  const struct range *x = a, *y = b;
  return 10 * cmp_int(x->from, y->from) + cmp_int(x->to, y->to);
}

static int read_scan(int sensor[2], int beacon[2]) {
  int read = scanf("Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d\n",
                  &sensor[0], &sensor[1], &beacon[0], &beacon[1]);
  if (read == EOF) {
    return 0;
  }
  if (read != 4) {
    fprintf(stderr, "Error: expected sensor and beacon positions.\n");
    return -1;
  }
  return 1;
}

static int run_01(int height) {
  static const int MAX_SENSORS = 32;
  static const int MAX_BEACONS = 16;
  struct range ranges[MAX_SENSORS]; // 256B
  int beacons[MAX_BEACONS];         //  64B

  int num_sensors = 0, num_beacons = 0;

  int sensor[2], beacon[2], read;
  while ((read = read_scan(sensor, beacon)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    if (height == beacon[1]) {
      if (num_beacons >= MAX_BEACONS) {
        fprintf(stderr, "Error: Beacon limit exceeded\n.");
        return EXIT_FAILURE;
      }

      beacons[num_beacons++] = beacon[0];
    }

    // beacon range in reach direction from its positions
    int rx = abs(beacon[0] - sensor[0]);
    int ry = abs(beacon[1] - sensor[1]);
    int r = rx+ry; // manhattan range
    int dy = abs(height - sensor[1]); // y distance: sendor <-> beacon

    if (dy <= r) {
      if (num_sensors >= MAX_SENSORS) {
        fprintf(stderr, "Error: Sensor limit exceeded\n.");
        return EXIT_FAILURE;
      }

      int ext = (r - dy);
      ranges[num_sensors++] = (struct range) {
        .from = sensor[0] - ext, .to = sensor[0] + ext,
      };
    }
  }

  qsort(ranges, num_sensors, sizeof(struct range), cmp_range_fun);
  qsort(beacons, num_beacons, sizeof(int), cmp_position_fun);
  num_beacons = suniq(beacons, num_beacons, sizeof(int));

  int free = 0;
  for(int last = INT_MIN, b=0, i=0; i<num_sensors; i++) {
    int start = ranges[i].from;
    int end = 1 + ranges[i].to;
    if (last > start) {
      start = last;
    }
    if (end > last) {
      last = end;
    }
    if (start >= end) {
      continue; // empty range
    }

    free += (end - start);
    for(int j=b; j<num_beacons; j++) {
      if ((start <= beacons[j]) & (beacons[j] < end)) {
        free--; // not a free space
        b++; // don't loop over passed beacons twice
      }
    }
  }

  printf("%d\n", free);
  return EXIT_SUCCESS;
}

static int run_02(int bounds) {
  // If there's only a single free cells it just outside some sensors bounds.
  // Each sensor range forms a diamond with 4 equal sides.
  // We can extends the range by 1 and represent it by 4 lines (segments).
  // The intersection of two lines is not within any sensors bounds is free.
  // In the slope intercept form `y = m*x + d`,
  // there are two lines with slope `m=1` and two with slope `m=-1`.
  // The intersection for the different slopes is:
  //   `y = +x + d_0`
  //   `y = -x + d_1`
  //   `x = (d_1 - d_0) / 2`
  //   `y = (d_0 + d_1) / 2`
  // He hence we only need the slope type and theirs intercepts (height at x=0).

  static const int MAX_SENSORS = 32;

  int sensors[MAX_SENSORS][2];              // 256B
  int radius[MAX_SENSORS];                  // 128B
  int pos_slope_intercepts[2*MAX_SENSORS];  // 256B
  int neg_slope_intercepts[2*MAX_SENSORS];  // 256B

  int num_sensors = 0;
  int num_slopes = 0;

  int sensor[2], beacon[2], read;
  while ((read = read_scan(sensor, beacon)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    if (num_sensors >= MAX_SENSORS) {
      fprintf(stderr, "Error: Sensor limit exceeded\n.");
      return EXIT_FAILURE;
    }

    // beacon range in reach direction from its positions
    int rx = abs(beacon[0] - sensor[0]);
    int ry = abs(beacon[1] - sensor[1]);
    int r = rx+ry; // manhattan range

    sensors[num_sensors][0] = sensor[0];
    sensors[num_sensors][1] = sensor[1];
    radius[num_sensors] = r;
    num_sensors++;

    r++; // line just outside the range
    pos_slope_intercepts[num_slopes] = sensor[1] - sensor[0] + r;
    neg_slope_intercepts[num_slopes] = sensor[0] + sensor[1] + r;
    num_slopes++;
    pos_slope_intercepts[num_slopes] = sensor[1] - sensor[0] - r;
    neg_slope_intercepts[num_slopes] = sensor[0] + sensor[1] - r;
    num_slopes++;
  }

  for(int i=0; i<num_slopes; i++) {
    for(int j=0; j<num_slopes; j++) {
      // line intersection
      int x = (neg_slope_intercepts[j] - pos_slope_intercepts[i]) / 2;
      int y = (pos_slope_intercepts[i] + neg_slope_intercepts[j]) / 2;

      if ((x < 0) | (x >= bounds) | (y < 0) | (y >= bounds)) {
        continue;
      }

      bool ok = true;
      for(int k=0; k<num_sensors; k++) {
        int dx = abs(sensors[k][0] - x);
        int dy = abs(sensors[k][1] - y);
        int d = dx+dy;
        ok &= (d > radius[k]);
      }
      if (ok) {
        printf("%ld\n", 4000000L * x + y);
        return EXIT_SUCCESS;
      }
    }
  }

  fprintf(stderr, "Error: No free space found\n.");
  return EXIT_FAILURE;
}

int aoc_run_2022_day_15(bool first) {
  static const bool TEST = false;
  return first ? run_01(TEST ? 10 : 2000000)
               : run_02(TEST ? 20 : 4000000);
}

#define AOC_RUN_DAY aoc_run_2022_day_15
#include "main.c"
