#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HEAP_IMPL
#include "heap.h"

typedef struct node {
  int priority, index;
} node_t;

inline static int cmp_nodes(const node_t *a, const node_t *b) {
  return ((a->priority > b->priority) - (a->priority < b->priority));
}

inline static int cmp_node_ptrs(const void *a, const void *b) { return cmp_nodes(a, b); }

static void print_image(int width, int height, uint8_t image[width * height]) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      printf("%d", image[i++]);
    }
    printf("\n");
  }
}

static inline int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

static int tile_image(int width, int height, uint8_t s, uint8_t t, uint8_t image[s * width * t * height]) {

  int sw = s * width, th = t * height;

  for (int y = height - 1; y >= 0; y--) {
    for (int x = width - 1; x >= 0; x--) {
      int src = image_index(width, height, x, y);
      int dst = image_index(sw, th, x, y);
      image[dst] = image[src];
    }
  }

  for (int8_t i_t = 0; i_t < t; i_t++) {
    for (int8_t i_s = 0; i_s < s; i_s++) {
      int add = i_s + i_t;
      if (add == 0) {
        continue;
      }
      for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
          int src = image_index(sw, th, x, y);
          int dst = image_index(sw, th, x + width * i_s, y + height * i_t);
          int val = image[src] + add;
          val = (val - 1) % 9 + 1;
          image[dst] = val;
        }
      }
    }
  }

  return sw * th;
}

static int find_shortest_image_distance(int width, int height, uint8_t image[width * height],
                                        int distances[1 + width * height], node_t nodes[1 + width * height],
                                        int start, int end) {
  const static int links[4][2] = {{0, -1}, {0, +1}, {-1, 0}, {+1, 0}};

  int n = width * height;

  for (int i = 0; i < n; i++) {
    distances[i] = INT_MAX;
  }
  distances[start] = 0;
  distances[n] = INT_MIN;

  nodes[0] = (node_t){.index = 0, .priority = 0};
  struct heap heap = {
      .size = 1,
      .block = sizeof(node_t),
      .capacity = sizeof(node_t) * n,
      .memory = nodes,
      .cmp = cmp_node_ptrs,
  };

  node_t current;
  while (heap_pop(&heap, &current)) {
    if (distances[current.index] < 0) {
      continue; // already visited
    }

    int dist = distances[current.index];
    distances[current.index] = -dist - 1; // mark visited
    if (current.index == end) {
      break;
    }

    int x = current.index % width;
    int y = current.index / width;

    int i, d;
    for (int l = 0; l < 4; l++) {
      i = image_index(width, height, x + links[l][0], y + links[l][1]);
      d = dist + image[i];
      if (distances[i] > d) {
        distances[i] = d;
        if (heap_push(&heap, &(node_t){.index = i, .priority = d}) >= heap.size) {
          return -1;
        }
      }
    }
  }

  return -(distances[end] + 1); // reconstruct distance
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height) {
  int image_size = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c >= '0') & (c <= '9')) {
      space = false;
      if (image_size == max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      image[image_size++] = c - '0';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run(int num_nodes, uint8_t image[num_nodes], int distances[1 + num_nodes], node_t nodes[1 + num_nodes],
               uint8_t tile) {
  int width = 0, height = 0;
  if ((num_nodes = read_image(num_nodes, image, &width, &height)) < 0) {
    return -1;
  }

  if (tile > 1) {
    num_nodes = tile_image(width, height, tile, tile, image);
    width *= tile;
    height *= tile;
  }

  int distance = find_shortest_image_distance(width, height, image, distances, nodes, 0, num_nodes - 1);
  if (distance < 0) {
    fprintf(stderr, "Error: heap size limit exceeded\n");
    return EXIT_FAILURE;
  }
  printf("%d\n", distance);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_15(bool first) {
  const uint8_t MAX_TILE = 5;
  const int MAX_IMAGE_SIZE = 128 * 128 * MAX_TILE * MAX_TILE;
  uint8_t *image /*[MAX_IMAGE_SIZE]*/;     // 400 KB
  int *distances /*[1 + MAX_IMAGE_SIZE]*/; // < 2 MB
  node_t *nodes /*[1 + MAX_IMAGE_SIZE]*/;  // < 4 MB
  {                                        // free'd at EXIT
    void *arena = malloc(MAX_IMAGE_SIZE + sizeof(int) * (1 + MAX_IMAGE_SIZE) + sizeof(node_t) * (1 + MAX_IMAGE_SIZE));
    image = (uint8_t *)arena;
    distances = (int *)&image[MAX_IMAGE_SIZE];
    nodes = (node_t *)&distances[1 + MAX_IMAGE_SIZE];
  }

  return first ? run(MAX_IMAGE_SIZE, image, distances, nodes, 1 /*tiling*/)
               : run(MAX_IMAGE_SIZE, image, distances, nodes, 5 /*tiling*/);
}

#define AOC_RUN_DAY aoc_run_2021_day_15
#include "main.c"
