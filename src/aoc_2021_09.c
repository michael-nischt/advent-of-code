#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static void print_image(int width, int height, uint8_t image[width * height]) {
  int i = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      printf("%d", image[i++]);
    }
    printf("\n");
  }
}

static inline int image_index(int width, int height, int x, int y) {
  bool valid = (x >= 0) & (x < width) & (y >= 0) & (y < height);
  return valid * (width * y + x) + !valid * (width * height);
}

static int measure_image_basin_at(int width, int height, uint8_t image[(width * height) + 1], int x, int y) {
  int ndx = image_index(width, height, x, y);
  int val = image[ndx];
  image[ndx] = 9;

  int sum = val < 9, con;

  con = image[image_index(width, height, x, y - 1)];
  if ((con < 9) & (con >= val)) {
    sum += measure_image_basin_at(width, height, image, x, y - 1);
  }
  con = image[image_index(width, height, x, y + 1)];
  if ((con < 9) & (con >= val)) {
    sum += measure_image_basin_at(width, height, image, x, y + 1);
  }
  con = image[image_index(width, height, x - 1, y)];
  if ((con < 9) & (con >= val)) {
    sum += measure_image_basin_at(width, height, image, x - 1, y);
  }
  con = image[image_index(width, height, x + 1, y)];
  if ((con < 9) & (con >= val)) {
    sum += measure_image_basin_at(width, height, image, x + 1, y);
  }

  return sum;
}

static int read_image(int max_image_size, uint8_t image[max_image_size], int *width, int *height) {
  int image_size = 0;
  *width = 0;
  *height = 0;

  int c;
  bool space = false;
  while ((c = fgetc(stdin)) != EOF) {
    if (isspace(c) != 0) {
      if (space) {
        continue;
      }
      space = true;
      if (image_size != 0) {
        if (*width == 0) {
          *width = image_size;
        }
        if (image_size % *width == 0) {
          (*height)++;
        }
      }
    } else if ((c >= '0') & (c <= '9')) {
      space = false;
      if (image_size >= max_image_size) {
        fprintf(stderr, "Error: image size limit exceeded\n");
        return -1;
      }
      image[image_size++] = c - '0';
    } else {
      fprintf(stderr, "Error: image pixel type invalid\n");
      return -1;
    }
  }
  *height += !space; // support missing empty line
  return image_size;
}

static int run_01(int image_size, uint8_t image[image_size + 1]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int sum = 0;

  image[width * height] = 9; // outside marker for safe access to border pixel(s)
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      uint8_t val = image[image_index(width, height, x, y)];
      bool low =
          (val < image[image_index(width, height, x, y - 1)]) & (val < image[image_index(width, height, x, y + 1)]) &
          (val < image[image_index(width, height, x - 1, y)]) & (val < image[image_index(width, height, x + 1, y)]);
      sum += low * (val + 1);
    }
  }

  printf("%d\n", sum);
  return EXIT_SUCCESS;
}

static int run_02(int image_size, uint8_t image[image_size + 1]) {
  int width, height;
  if ((image_size = read_image(image_size, image, &width, &height)) < 0) {
    return EXIT_FAILURE;
  }

  int max[3] = {0};

  image[width * height] = 9; // outside marker for safe access to border pixel(s)
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {

      uint8_t val = image[image_index(width, height, x, y)];
      bool low =
          (val < image[image_index(width, height, x, y - 1)]) & (val < image[image_index(width, height, x, y + 1)]) &
          (val < image[image_index(width, height, x - 1, y)]) & (val < image[image_index(width, height, x + 1, y)]);
      if (!low) {
        continue;
      }

      int size = measure_image_basin_at(width, height, image, x, y);
      if (size > max[2]) {
        max[0] = max[1];
        max[1] = max[2];
        max[2] = size;
      } else if (size > max[1]) {
        max[0] = max[1];
        max[1] = size;
      } else if (size > max[0]) {
        max[0] = size;
      }
    }
  }

  printf("%d\n", max[0] * max[1] * max[2]);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_09(bool first) {
  const int IMAGE_MAX_WIDTH = 100, IMAGE_MAX_HEIGHT = 100;
  uint8_t *image /*[IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT + 1]*/;
  image = malloc(sizeof(uint8_t) * (IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT + 1)); // free'd at EXIT

  return first ? run_01(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image) : run_02(IMAGE_MAX_WIDTH * IMAGE_MAX_HEIGHT, image);
}

#define AOC_RUN_DAY aoc_run_2021_day_09
#include "main.c"
