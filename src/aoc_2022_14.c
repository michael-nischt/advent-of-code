#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

static const char SYMBOLS[] = ".#O";
static const uint8_t SYMBOL_AIR = 0;
static const uint8_t SYMBOL_STONE = 1;
static const uint8_t SYMBOL_SAND = 2;

typedef struct vec2 {
  int x, y;
} vec2_t;

static void print_image(int width, int height,
                        uint8_t image[static(width * height)]) {
  for (int i = 0, y = 0; y < height; y++) {
    for (int x = 0; x < width; x++, i++) {
      int symbol = image[i];
      assert((symbol >= 0) & (symbol < sizeof(SYMBOLS)));
      printf("%c", SYMBOLS[symbol]);
    }
    printf("\n");
  }
}

inline static void clear_image(int width, int height,
                               uint8_t image[static(width * height)]) {
  memset(image, SYMBOL_AIR, sizeof(uint8_t) * width * height);
}

static bool draw_image_line(uint8_t c,
                            vec2_t from, vec2_t to,
                            int width, int height,
                            uint8_t image[static(width * height)]) {
  int x, y, d, tmp;
  if (from.x == to.x) { // vertical
    if (from.y > to.y) {
      tmp = from.y;
      from.y = to.y;
      to.y = tmp;
    }
    x = from.x;
    for (y = from.y; y <= to.y; y++) {
      image[width * y + x] = c;
    }
  } else if (from.y == to.y) { // horizontal
    if (from.x > to.x) {
      tmp = from.x;
      from.x = to.x;
      to.x = tmp;
    }
    y = from.y;
    for (x = from.x; x <= to.x; x++) {
      image[width * y + x] = c;
    }
  } else {
    return false;
  }
  return true;
}

static void draw_image_trace(int num_samples, const vec2_t samples[static(num_samples)],
                             int width, int height, uint8_t image[static(width * height)]) {
  if (num_samples == 1) {
    draw_image_line(SYMBOL_STONE, samples[0], samples[0], width, height, image);
    return;
  }
  for (int i=1; i<num_samples; i++) {
    draw_image_line(SYMBOL_STONE, samples[i-1], samples[i], width, height, image);
  }
}

static void draw_image_traces(int num_traces, const int traces[static(num_traces)],
                              const vec2_t samples[static(traces[num_traces])],
                              int width, int height, uint8_t image[static(width * height)]) {
  for (int i=0; i<num_traces; i++) {
    int off_samples = traces[i];
    int num_samples = traces[i+1] - off_samples;
    draw_image_trace(num_samples, &samples[off_samples], width, height, image);
  }
}

inline static bool outside_position(int width, int height, vec2_t pos) {
  return (pos.x < 0) | (pos.x >= width) | (pos.y < 0) | (pos.y >= height);
}

static bool simulate_single(vec2_t pos, int width, int height,
                           uint8_t image[static(width * height)]) {
  if (image[width * pos.y + pos.x] != 0) {
    return false;
  }

  while(true) {
    vec2_t rest = pos;

    pos.y++;
    if (outside_position(width, height, pos)) {
      return false; // flowing off into the abyss
    }
    if (image[width * pos.y + pos.x] == SYMBOL_AIR) {
      continue; // can flow down-straight
    }

    pos.x -= 1;
    if (outside_position(width, height, pos)) {
      return false; // flowing off into the abyss
    }
    if (image[width * pos.y + pos.x] == SYMBOL_AIR) {
      continue; // can flow down-left
    }

    pos.x += 2;
    if (outside_position(width, height, pos)) {
      return false; // flowing off into the abyss
    }
    if (image[width * pos.y + pos.x] == SYMBOL_AIR) {
      continue; // can flow down-right
    }

    image[width * rest.y + rest.x] = SYMBOL_SAND;
    return true;
  }

  return false;
}

static int simulate(vec2_t start, int width, int height,
                    uint8_t image[static(width * height)]) {

 int num_units = 0;
 while (simulate_single(start, width, height, image)) {
  num_units++;
 }
 return num_units;
}

static int print_trace(int num_samples, const vec2_t samples[static(num_samples)]) {
    for(int i=0; i<num_samples; i++) {
      if (i > 0) {
        printf(" -> ");
      }
      printf("%d,%d", samples[i].x, samples[i].y);
    }
    printf("\n");
}

static int parse_trace(const char* str, int max_samples, vec2_t samples[static(max_samples)]) {
  static const char DELIM[] = "->";

  int num_samples = 0;

  char *next;
  do {
    // x
    long x = strtoul(str, &next, 10);
    if (next <= str) {
      fprintf(stderr, "Error: Expected x coordinate.\n");
      return -1;
    }
    str = next;
    // ,
    if (*str != ',') {
      fprintf(stderr, "Error: Expected ',' between x,y coordinates\n");
      return -1;
    }
    str++;
    // y
    long y = strtoul(str, &next, 10);
    if (next <= str) {
      fprintf(stderr, "Error: Expected x coordinate.\n");
      return -1;
    }
    str = next;
    // sample
    if (num_samples >= max_samples) {
      fprintf(stderr, "Error: Sample limit exceeded.\n");
      return -1;
    }
    samples[num_samples++] = (vec2_t) { .x = x, .y = y };

    // more samples follow after `->` if present
    str = strstr(str, DELIM);
    if (str == NULL) {
      break;
    }
    str += sizeof(DELIM) - 1;
  } while(true);

  return num_samples;
}

static int read_traces(int max_traces, int traces[static(max_traces)],
                       int max_samples, vec2_t samples[static(max_samples)]) {
  int num_traces = 0;
  traces[0] = 0;

  static const int N = 340;
  char BUFFER[N+1];
  BUFFER[N] = 0;
  while (fgets(BUFFER, N+1, stdin) != NULL) {
    if ((BUFFER[N] != 0) | (BUFFER[N-1] == '\n')) {
      fprintf(stderr, "Error: Trace line exceeds buffer %d.\n", N);
      return -1;
    }
    if (num_traces >= max_traces) {
      fprintf(stderr, "Error: Trace count exceeds limit %d.\n", max_traces);
      return -1;
    }
    // printf("%d: %s", num_traces, BUFFER);

    int num_samples = traces[num_traces];
    int res = parse_trace(BUFFER, max_samples - num_samples, &samples[num_samples]);
    if (res < 0) {
      return -1;
    }

    // printf("%d: ", num_traces);
    // print_trace(res, &samples[num_samples]);

    traces[++num_traces] = num_samples + res;
  }

  return num_traces;
}

static int run(int max_cells, uint8_t image[static(max_cells)],
               int max_traces, int traces[static(1+max_traces)],
               int max_samples, vec2_t samples[static(max_samples)],
               vec2_t start, int generate_bottom) {
  int num_traces = read_traces(max_traces, traces, max_samples, samples);
  if (num_traces < 0) {
    return EXIT_FAILURE;
  }

  int num_samples = traces[num_traces];
  int width, height; {
    // calculate bound (min/max)
    vec2_t bounds[2] = { start, start };
    for(int i=0; i<num_samples; i++) {
      vec2_t sample = samples[i];
      if (sample.x < bounds[0].x) {
        bounds[0].x = sample.x;
      }
      if (sample.y < bounds[0].y) {
        bounds[0].y = sample.y;
      }
      if (sample.x > bounds[1].x) {
        bounds[1].x = sample.x;
      }
      if (sample.y > bounds[1].y) {
        bounds[1].y = sample.y;
      }
    }

    if (generate_bottom != 0) {
      bounds[1].y += generate_bottom;
      int ext = ((bounds[1].y * 3) / 2); // approx: sqrt(2) * bounds[1].y
      bounds[1].x = start.x + ext;
      bounds[0].x = start.x - ext;
    }

    for(int i=0; i<num_samples; i++) {
      samples[i].x -= bounds[0].x;
      samples[i].y -= bounds[0].y;
    }
    start.x -= bounds[0].x;
    start.y -= bounds[0].y;

    width = 1 + (bounds[1].x - bounds[0].x);
    height = 1 + (bounds[1].y - bounds[0].y);
  }

  if (width*height > max_cells) {
    fprintf(stderr, "Error: Grid cells exceeds limit %d.\n", max_cells);
    return EXIT_FAILURE;
  }

  clear_image(width, height, image);
  if (generate_bottom != 0) {
    // draw bottom
    for(int s=width*(height-1), i=0; i<width; i++, s++) {
      image[s] = SYMBOL_STONE;
    }
  }
  draw_image_traces(num_traces, traces, samples, width, height, image);

  int num_units = simulate(start, width, height, image);

  printf("%d\n", num_units);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_14(bool first) {
  // Traces and and samples can be 'freed' already once image in generated.
  //
  // We could draw them streaming with just a line (2 coords) allocated.
  // However, then we need to make sure the grid is large enough and
  // we don't have an upper bound for the height before reading all traces.
  // Worse, the memory requirements for large images would be much higher.

  const int MAX_CELLS = 160000; // e.g. 800x200
  const int MAX_TRACES = 180;
  const int MAX_SAMPLES = 32 * MAX_TRACES;

  uint8_t *image/*[MAX_CELLS]*/; // ~160KB
  int *traces/*[1+MAX_TRACES]*/; // ~724B
  vec2_t *samples/*[MAX_SAMPLES]*/;   //  ~45KB
  { // free'd at EXIT
    void *arena = malloc(MAX_CELLS * sizeof(uint8_t)
                      + (MAX_TRACES+1) * sizeof(int)
                      + MAX_SAMPLES * sizeof(vec2_t));
    image = (uint8_t*) arena;
    traces = (int*) (image + MAX_CELLS);
    samples = (vec2_t*) (traces + (MAX_TRACES+1));
  }

  return run(MAX_CELLS, image,
             MAX_TRACES, traces,
             MAX_SAMPLES, samples,
             (vec2_t) { .x = 500, .y = 0 },  // start
             first ? 0 : 2); // if > 0 then place line below deepest stone.
}

#define AOC_RUN_DAY aoc_run_2022_day_14
#include "main.c"
