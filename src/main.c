#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[argc + 1]) {
  if (argc != 2) {
    printf("Usage: %s -part=1|2'\n", argv[0]);
    return EXIT_SUCCESS;
  }
  int part = 0;
  sscanf(argv[1], "-part=%1d", &part);
  if ((part == 1) | (part == 2)) {
    return AOC_RUN_DAY(part == 1);
  }
  return main(1, argv);
}
