#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static int read_strategy(char *x, char *y) {
  int read = scanf("%c %c\n", x, y);
  if (read == EOF) {
    return 0;
  }
  if (read != 2) {
    fprintf(stderr, "Error: expected encrypted strategy with two characters\n");
    return -1;
  }
  if ((*x < 'A') | (*x > 'C')) {
    fprintf(stderr, "Error: expected encryption part 1 of 'A', 'B' or 'C'\n");
    return -1;
  }
  if ((*y < 'X') | (*y > 'Z')) {
    fprintf(stderr, "Error: expected encryption part 2 of 'X', 'Y' or 'Z'\n");
    return -1;
  }
  *x -= 'A';
  *y -= 'X';
  return 1;
}

static int run_01() {
  int score = 0, read;
  char opp, you, res;

  while ((read = read_strategy(&opp, &you)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }
    // res: loss=0, draw=0, win=2
    res = 1 + (you != opp) -2 * (((3 + you - opp) % 3) == 2);
    score += (1 + you) + (res * 3);
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02() {
  int score = 0, read;
  char opp, you, res;

  while ((read = read_strategy(&opp, &res)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }
    if (res == 0) { // loss
      you = (opp + 2) % 3;
    } else if (res == 1) { // draw
      you = opp;
    } else { // win
      you = (opp + 1) % 3;
    }
    score += (1 + you) + (res * 3);
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_02(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_02
#include "main.c"
