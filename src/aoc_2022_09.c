#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct area {
  int origin[2];
  int size[2];
};

static void area_init(struct area *area, int n) {
  area->origin[0] = ++n;
  area->origin[1] = n--;
  n = n*2 + 1;
  area->size[0] = n;
  area->size[1] = n;
}

static int area_index(const struct area *area, int x, int y) {
  x += area->origin[0];
  y += area->origin[1];
  return area->size[0] * y + x;
}

static int read_pull_motion(int delta[2]) {
  char dir;
  int steps;
  int read = scanf("%c %u\n", &dir, (unsigned int*) &steps);
  if (read == EOF) {
    return 0;
  }
  if (read != 2) {
    fprintf(stderr, "Error: expected direction and steps.\n");
    return -1;
  }
  if (steps < 0) {
    fprintf(stderr, "Error: number of steps is too large.\n");
    return -1;
  }

  if (dir == 'R') {
    delta[0] = steps;
    delta[1] = 0;
    return 1;
  }
  if (dir == 'U') {
    delta[0] = 0;
    delta[1] = steps;
    return 1;
  }
  if (dir == 'L') {
    delta[0] = -steps;
    delta[1] = 0;
    return 1;
  }
  if (dir == 'D') {
    delta[0] = 0;
    delta[1] = -steps;
    return 1;
  }
  fprintf(stderr, "Error: unknown direction.\n");
  return -1;
}

static int run(int knots, const struct area * area, char *cells) {
  int steps = 1;

  int rope[knots][2];
  memset(rope, 0, sizeof(rope));
  memset(cells, 0, area->size[0]*area->size[1]);

  int delta[2], distance[2], direction[2], read;
  while ((read = read_pull_motion(delta)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    int k = knots - 1;

    // update head position with pull motion
    rope[k][0] += delta[0];
    rope[k][1] += delta[1];

    while(k > 0) {
      int motion = 0;

      for(k=knots-2; k>=0; k--) {
        // delta from tail to head
        delta[0] = rope[k+1][0] - rope[k][0];
        delta[1] = rope[k+1][1] - rope[k][1];
        // distance along x and y
        distance[0] = abs(delta[0]);
        distance[1] = abs(delta[1]);

        if ((distance[0] <= 1) & (distance[1] <= 1)) {
          break; // no pull on this not and hence none of the ones behind
        }

        motion++;
        // signed step
        direction[0] = delta[0] == 0 ? 0 : (delta[0] / distance[0]);
        direction[1] = delta[1] == 0 ? 0 : (delta[1] / distance[1]);
        // apply step
        rope[k][0] += direction[0];
        rope[k][1] += direction[1];

        if (k != 0) {
          continue;
        }

        int index = area_index(area, rope[k][0], rope[k][1]);
        if (index >= area->size[0]*area->size[1]) {
          fprintf(stderr, "Error: too large area (%d %d | %d).\n", rope[k][0], rope[k][1], index);
          return EXIT_FAILURE;
        }

        steps += cells[index] == 0;
        cells[index] = 1;
      }

      k = motion;
    }
  }

  printf("%d\n", steps);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_09(bool first) {
  struct area area;
  area_init(&area, 400);
  char *cells = malloc(area.size[0] * area.size[1]); // free'd at EXIT
  return first ? run(2, &area, cells) : run(10, &area, cells); }

#define AOC_RUN_DAY aoc_run_2022_day_09
#include "main.c"
