#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// VECMATH

typedef struct vec3 {
  int x, y, z;
} vec3_t;

typedef struct mat3x3 {
  int _00, _01, _02; // row 0
  int _10, _11, _12; // row 1
  int _20, _21, _22; // row 2
} mat3x3_t;

inline static vec3_t set3(int val) { return (vec3_t){.x = val, .y = val, .z = val}; }
inline static bool eq3(vec3_t a, vec3_t b) { return (a.x == b.x) & (a.y == b.y) & (a.z == b.z); }
inline static vec3_t add3(vec3_t a, vec3_t b) { return (vec3_t){.x = a.x + b.x, .y = a.y + b.y, .z = a.z + b.z}; }
inline static vec3_t sub3(vec3_t a, vec3_t b) { return (vec3_t){.x = a.x - b.x, .y = a.y - b.y, .z = a.z - b.z}; }
inline static vec3_t abs3(vec3_t v) {
  return (vec3_t){
      .x = (v.x >= 0) * v.x + (v.x < 0) * -v.x,
      .y = (v.y >= 0) * v.y + (v.y < 0) * -v.y,
      .z = (v.z >= 0) * v.z + (v.z < 0) * -v.z,
  };
}
inline static int lensq3(vec3_t a) { return a.x * a.x + a.y * a.y + a.z * a.z; }
inline static int distsq3(vec3_t a, vec3_t b) { return lensq3(sub3(a, b)); }
inline static int mdistsq3(vec3_t a, vec3_t b) {
  vec3_t d = abs3(sub3(a, b));
  return d.x + d.y + d.z;
}

inline static mat3x3_t transpose3x3(mat3x3_t m) {
  return (mat3x3_t){
      ._00 = m._00,
      ._01 = m._10,
      ._02 = m._20,
      ._10 = m._01,
      ._11 = m._11,
      ._12 = m._21,
      ._20 = m._02,
      ._21 = m._12,
      ._22 = m._22,
  };
}
inline static mat3x3_t mul3x3(mat3x3_t a, mat3x3_t b) {
  return (mat3x3_t){
      // row 0
      ._00 = a._00 * b._00 + a._01 * b._10 + a._02 * b._20,
      ._01 = a._00 * b._01 + a._01 * b._11 + a._02 * b._21,
      ._02 = a._00 * b._02 + a._01 * b._12 + a._02 * b._22,
      // row 1
      ._10 = a._10 * b._00 + a._11 * b._10 + a._12 * b._20,
      ._11 = a._10 * b._01 + a._11 * b._11 + a._12 * b._21,
      ._12 = a._10 * b._02 + a._11 * b._12 + a._12 * b._22,
      // row 2
      ._20 = a._20 * b._00 + a._21 * b._10 + a._22 * b._20,
      ._21 = a._20 * b._01 + a._21 * b._11 + a._22 * b._21,
      ._22 = a._20 * b._02 + a._21 * b._12 + a._22 * b._22,
  };
}
inline static vec3_t xform3x3(mat3x3_t m, vec3_t v) {
  return (vec3_t){
      .x = m._00 * v.x + m._01 * v.y + m._02 * v.z,
      .y = m._10 * v.x + m._11 * v.y + m._12 * v.z,
      .z = m._20 * v.x + m._21 * v.y + m._22 * v.z,
  };
}

static void generate_cardinal_rotations(mat3x3_t rot[24]) {
  // 0
  rot[0] = (mat3x3_t){
      +1, 0, 0, 0, +1, 0, 0, 0, +1, // I
  };
  // 1
  rot[1] = (mat3x3_t){
      +1, 0, 0, 0, 0, -1, 0, +1, 0, // X
  };
  rot[2] = (mat3x3_t){
      0, 0, +1, 0, +1, 0, -1, 0, 0, // Y
  };
  // 2
  rot[3] = mul3x3(rot[1], rot[1]); // XX = YXXY
  rot[4] = mul3x3(rot[1], rot[2]); // XY
  rot[5] = mul3x3(rot[2], rot[1]); // YX
  rot[6] = mul3x3(rot[2], rot[2]); // YY = XYYX
  // 3
  rot[7] = mul3x3(rot[1], rot[3]);  // XXX = XYXXY = YXXYX = YXYXY = YYXYY
  rot[8] = mul3x3(rot[1], rot[4]);  // XXY = YXXYY = YYYXX
  rot[9] = mul3x3(rot[1], rot[5]);  // XYX = YXY
  rot[10] = mul3x3(rot[1], rot[6]); // XYY = XXYYX = YYXXX
  rot[11] = mul3x3(rot[2], rot[3]); // YXX = XXYYY = YYXXY
  rot[12] = mul3x3(rot[2], rot[5]); // YYX = XXXYY = XYYXX
  rot[13] = mul3x3(rot[2], rot[6]); // YYY = XXYXX = XYXYX = XYYXY = YXYYX
  // 4
  rot[14] = mul3x3(rot[1], rot[8]);  // XXXY
  rot[15] = mul3x3(rot[1], rot[9]);  // XXYX = XYXY = YXYY
  rot[16] = mul3x3(rot[1], rot[10]); // XXYY = YYXX
  rot[17] = mul3x3(rot[1], rot[11]); // XYXX = YXYX = YYXY
  rot[18] = mul3x3(rot[1], rot[13]); // XYYY
  rot[19] = mul3x3(rot[2], rot[7]);  // YXXX
  rot[20] = mul3x3(rot[2], rot[12]); // YYYX
  // 5
  rot[21] = mul3x3(rot[1], rot[15]); // XXXYX = XXYXY = XYXYY = YXYYY
  rot[22] = mul3x3(rot[1], rot[19]); // XYXXX = YXYXX = YYXYX = YYYXY
  rot[23] = mul3x3(rot[1], rot[20]); // XYYYX = YXXXY
}

// SCANNERS & BEACONS

inline static int cmp_distances(int a, int b) { return (a > b) - (a < b); }
inline static int cmp_distance_ptrs(const void *a, const void *b) { return cmp_distances(*((int *)a), *((int *)b)); }

static void sort_distances(int n, int distances[n]) { qsort(distances, n, sizeof(int), cmp_distance_ptrs); }

static bool match_sorted_distances(int m, int a[m], int n, int b[n], int threshold) {
  int res = 0, i = 0, j = 0;
  while (i < m && j < n) {
    int cmp = cmp_distances(a[i], b[j]);
    res += cmp == 0;
    i += cmp <= 0;
    j += cmp >= 0;
    if (res >= threshold) {
      return true;
    }
  }
  return false;
}

inline static int cmp_beacons(const vec3_t *a, const vec3_t *b) {
  return ((a->x > b->x) - (a->x < b->x)) * 100 + ((a->y > b->y) - (a->y < b->y)) * 10 + ((a->z > b->z) - (a->z < b->z));
}
inline static int cmp_beacon_ptrs(const void *a, const void *b) { return cmp_beacons(a, b); }

static void sort_beacons(int n, vec3_t beacons[n]) { qsort(beacons, n, sizeof(vec3_t), cmp_beacon_ptrs); }

static bool match_sorted_beacons(int m, vec3_t a[m], int n, vec3_t b[n], int threshold) {
  int res = 0, i = 0, j = 0;
  while (i < m && j < n) {
    int cmp = cmp_beacons(&a[i], &b[j]);
    res += cmp == 0;
    i += cmp <= 0;
    j += cmp >= 0;
    if (res >= threshold) {
      return true;
    }
  }
  return false;
}

static int count_unique_sorted_beacons(int n, vec3_t beacons[n]) {
  int duplicates = 0;
  for (int m = n - 1, i = 0; i < n; i++) {
    while ((i < m) && eq3(beacons[i], beacons[i + 1])) {
      i++;
      duplicates++;
    }
  }
  return n - duplicates;
}

static int measure_beacon_distances(int num_beacons, vec3_t beacons[num_beacons], int max_distances,
                                    int distances[max_distances]) {
  if (num_beacons * num_beacons > max_distances) {
    return -1;
  }
  int num_distances = 0;
  for (int i = 0; i < num_beacons; i++) {
    for (int j = 0; j < num_beacons; j++) {
      distances[num_distances++] = distsq3(beacons[i], beacons[j]);
    }
    sort_distances(num_beacons, &distances[num_beacons * i]);
  }
  return num_distances;
}

static int measure_scanner_distances(int num_scanners, int scanned[1 + num_scanners],
                                     vec3_t beacons[scanned[num_scanners]], int max_distances,
                                     int distance_offsets[num_scanners], int distances[max_distances]) {
  if ((distance_offsets == NULL) | (distances == NULL)) {
    return -1;
  }
  int num_distances = 0, scn_distances;
  for (int i = 0; i < num_scanners; i++) {
    scn_distances = measure_beacon_distances(scanned[i + 1] - scanned[i], &beacons[scanned[i]],
                                             max_distances - num_distances, &distances[num_distances]);
    if (scn_distances < 0) {
      return -1;
    }
    distance_offsets[i] = num_distances;
    num_distances += scn_distances;
  }
  return num_distances;
}

static bool can_match_scan_pair(int num_scanners, int scanned[1 + num_scanners], int distance_offsets[num_scanners],
                                int distances[distance_offsets[num_scanners]], int i_scn, int j_scn, int criteria) {
  int m = scanned[i_scn + 1] - scanned[i_scn];
  int n = scanned[j_scn + 1] - scanned[j_scn];
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      bool candidate = match_sorted_distances(m, &distances[distance_offsets[i_scn] + m * i], n,
                                              &distances[distance_offsets[j_scn] + n * j], criteria);
      if (candidate) {
        return true;
      }
    }
  }
  return false;
}

static bool match_scan_pair(int num_scanners, int scanned[1 + num_scanners], vec3_t *beacons, int i_scn, int j_scn,
                            mat3x3_t rot[24], int criteria, vec3_t *offset) {

  for (int r = 23; r >= 0; r--) {
    mat3x3_t xform = rot[r];
    if (r < 23) { // undo last rotation
      xform = mul3x3(xform, transpose3x3(rot[r + 1]));
    }
    for (int j = scanned[j_scn]; j < scanned[j_scn + 1]; j++) {
      beacons[j] = xform3x3(xform, beacons[j]);
    }
    sort_beacons(scanned[j_scn + 1] - scanned[j_scn], &beacons[scanned[j_scn]]);

    for (int i = scanned[i_scn]; i < scanned[i_scn + 1]; i++) {
      for (int j = scanned[j_scn]; j < scanned[j_scn + 1]; j++) {
        vec3_t delta = sub3(beacons[j], beacons[i]);

        for (int k = scanned[j_scn]; k < scanned[j_scn + 1]; k++) {
          beacons[k] = sub3(beacons[k], delta);
        }

        if (match_sorted_beacons(scanned[i_scn + 1] - scanned[i_scn], &beacons[scanned[i_scn]],
                                 scanned[j_scn + 1] - scanned[j_scn], &beacons[scanned[j_scn]], criteria)) {
          if (offset != NULL) {
            *offset = delta;
          }
          return true;
        }

        if (offset != NULL) {
          // undo last offset; only need for original offset
          // other drift is not really an issue
          for (int k = scanned[j_scn]; k < scanned[j_scn + 1]; k++) {
            beacons[k] = add3(beacons[k], delta);
          }
        }
      }
    }
  }

  // no final rotation undo because rot[0] = I;
  // even otherwise a rotation each iteration would case drift
  return false;
}

static bool match_scans(int num_scanners, int scanned[1 + num_scanners], int map[num_scanners],
                        vec3_t beacons[scanned[num_scanners]], vec3_t offsets[num_scanners], int num_distances,
                        int distance_offsets[num_scanners], int distances[num_distances]) {
  // could done be compile time/ using code generation
  mat3x3_t rot[24];
  generate_cardinal_rotations(rot);

  const int REQ_MATCHES = 12;

  num_distances = measure_scanner_distances(num_scanners, scanned, beacons, num_distances, distance_offsets, distances);

  for (int i = 0; i < num_scanners; i++) {
    map[i] = i;
  }
  if (offsets) {
    offsets[0] = set3(0);
  }

  sort_beacons(scanned[1], beacons);
  int num_matched = 1;

  for (int i = 0; i < num_matched; i++) {
    int i_scn = map[i];
    for (int j = num_matched; j < num_scanners; j++) {
      int j_scn = map[j];
      if (num_distances > 0) {
        if (!can_match_scan_pair(num_scanners, scanned, distance_offsets, distances, i_scn, j_scn, REQ_MATCHES)) {
          continue;
        }
      }
      bool match = match_scan_pair(num_scanners, scanned, beacons, i_scn, j_scn, rot, REQ_MATCHES,
                                   offsets != NULL ? &offsets[i_scn] : NULL);
      if (match) {
        if (offsets != NULL) {
          offsets[j_scn] = add3(offsets[i_scn], offsets[j_scn]);
        }
        if (j > num_matched) {
          int tmp = map[num_matched];
          map[num_matched++] = j_scn;
          map[j--] = tmp;
        } else {
          num_matched++;
        }
        if (num_matched == num_scanners) {
          return true;
        }
      }
    }
  }
  return false;
}

static void print_scans(int num_scanners, int scanned[1 + num_scanners], vec3_t beacons[scanned[num_scanners]]) {
  for (int i_scn = 0; i_scn < num_scanners; i_scn++) {
    printf("--- scanner %d ---\n", i_scn);
    for (int i_bcn = scanned[i_scn]; i_bcn < scanned[i_scn + 1]; i_bcn++) {
      printf("%d,%d,%d\n", beacons[i_bcn].x, beacons[i_bcn].y, beacons[i_bcn].z);
    }
    printf("\n");
  }
}

static int read_scans(int max_beacons, vec3_t beacons[max_beacons], int max_scanners, int scanned[1 + max_scanners]) {
  int num_scanners = 0, num_beacons = 0;

  int scn_id;
  vec3_t beacon;

  char line[32];
  size_t n = sizeof(line);
  while (fgets(line, sizeof(line), stdin) != NULL) {
    if ((line[0] == '\0') | isspace(line[0])) { // empty
      continue;
    }
    if (sscanf(line, "--- scanner %d ---\n", &scn_id) == 1) {
      if (scn_id != num_scanners) {
        fprintf(stderr, "Error: scanner id is not incrementing\n");
        return -1;
      }
      if (num_scanners >= max_scanners) {
        fprintf(stderr, "Error: scanner limit exceeded\n");
        return -1;
      }
      scanned[num_scanners++] = num_beacons;
    } else if (sscanf(line, "%d,%d,%d\n", &beacon.x, &beacon.y, &beacon.z) == 3) {
      if (num_beacons >= max_beacons) {
        fprintf(stderr, "Error: beacon limit exceeded\n");
        return -1;
      }
      beacons[num_beacons++] = beacon;
    } else {
      fprintf(stderr, "Error: expected scanner or beacon\n");
      return -1;
    }
  }

  scanned[num_scanners] = num_beacons;
  return num_scanners;
}

static int run01(int num_beacons, vec3_t beacons[num_beacons], int num_scanners, int scanned[1 + num_scanners],
                 int map[num_scanners], int num_distances, int distance_offsets[num_scanners],
                 int distances[num_distances]) {
  if ((num_scanners = read_scans(num_beacons, beacons, num_scanners, scanned)) < 0) {
    return EXIT_FAILURE;
  }

  if (!match_scans(num_scanners, scanned, map, beacons, NULL, num_distances, distance_offsets, distances)) {
    fprintf(stderr, "Error: scan matching failed\n");
    return EXIT_FAILURE;
  }

  sort_beacons(scanned[num_scanners], beacons);
  int unique = count_unique_sorted_beacons(scanned[num_scanners], beacons);

  printf("%d\n", unique);
  return EXIT_SUCCESS;
}

static int run02(int num_beacons, vec3_t beacons[num_beacons], int num_scanners, int scanned[1 + num_scanners],
                 int map[num_scanners], vec3_t offsets[num_scanners], int num_distances,
                 int distance_offsets[num_scanners], int distances[num_distances]) {
  if ((num_scanners = read_scans(num_beacons, beacons, num_scanners, scanned)) < 0) {
    return EXIT_FAILURE;
  }

  if (!match_scans(num_scanners, scanned, map, beacons, offsets, num_distances, distance_offsets, distances)) {
    fprintf(stderr, "Error: scan matching failed\n");
    return EXIT_FAILURE;
  }

  int max_dist = 0;
  for (int i = 0; i < num_scanners; i++) {
    vec3_t a = offsets[i];
    for (int j = i + 1; j < num_scanners; j++) {
      int dist = mdistsq3(a, offsets[j]);
      if (dist > max_dist) {
        max_dist = dist;
      }
    }
  }

  printf("%d\n", max_dist);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_19(bool first) {
  const int MAX_BEACONS = 1024;
  const int MAX_SCANNERS = 32;
  vec3_t beacons[MAX_BEACONS];   // 12 KB
  int scanned[1 + MAX_SCANNERS]; // 132 B
  int map[MAX_SCANNERS];         // 128 B
  vec3_t offsets[MAX_SCANNERS];  // 384 B
  // optional: 10-20x acceleration (distances are rotation invariant; allowing discard impossible matching early)
  const int MAX_DISTANCES = MAX_SCANNERS * 32 * 32; // MAX_SCANNERS * MAX_BEACONS_PER_SCANNER * MAX_BEACONS_PER_SCANNER
  int distance_offsets[MAX_SCANNERS];               // 128 B
  int *distances /*[MAX_DISTANCES]*/;               // 128 KB
  distances = malloc(sizeof(int) * MAX_DISTANCES);  // free'd at EXIT

  return first ? run01(MAX_BEACONS, beacons, MAX_SCANNERS, scanned, map, MAX_DISTANCES, distance_offsets, distances)
               : run02(MAX_BEACONS, beacons, MAX_SCANNERS, scanned, map, offsets, MAX_DISTANCES, distance_offsets,
                       distances);
}

#define AOC_RUN_DAY aoc_run_2021_day_19
#include "main.c"
