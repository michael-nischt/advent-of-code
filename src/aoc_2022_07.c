#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define CMD_CD_ROOT   -1
#define CMD_CD_PARENT -2
#define CMD_CD_CHILD  -3
#define CMD_LS        -4
#define DIR_ID        -5

static int read_log_entry(long *info) {
  const int N = 31;
  char line[N+1];
  line[N] = 0;
  if (fgets(line, N, stdin) == NULL) {
    return 0;
  }
  if (line[N] != 0) {
    fprintf(stderr, "Error: Expected logh entry with less than %d characters\n", N);
    return -1;
  }

  if ((strncmp(line, "$ ls", 4) == 0) & (isspace(line[4]) != 0)) {
    *info = CMD_LS;
    return 1;
  }
  if ((strncmp(line, "$ cd", 4) == 0) & (isspace(line[4]) != 0)) {
    if ((line[5] == '/') & (isspace(line[6]) != 0)) {
      *info = CMD_CD_ROOT;
    } else if ((line[5] == '.') & (line[6] == '.') & (isspace(line[7]) != 0)) {
      *info = CMD_CD_PARENT;
    } else {
      *info = CMD_CD_CHILD;
    }
    return 1;
  }
  if ((strncmp(line, "dir", 3) == 0) & (isspace(line[3]) != 0)) {
    *info = DIR_ID;
    return 1;
  }
  // file
  long size = (long) strtoul (line, NULL, 0);
  if ((size > 0) | ((line[0] == 0) & (isspace(line[1]) != 0))) {
    *info = size;
    return 1;
  }

  fprintf(stderr, "Error: File size too large: %s", line);
  return -1;
}

struct dir_walker {
  int depth;
  long stack[64];
  bool read;
};

static int walk_directory(struct dir_walker *walker, long *size) {
  while (!walker->read) {
    long info;
    int res = read_log_entry(&info);
    if (res < 0) {
      return res;
    }
    walker->read = res == 0;
    if (walker->read) {
      break;
    }

    const int depth_limit = sizeof(walker->stack)/sizeof(walker->stack[0]);
    switch (info) {
        case CMD_CD_ROOT:
          if (walker->depth != 0) {
            fprintf(stderr, "Error: Change to root directory needs to be the first command.\n");
            return -1;
          }
          walker->stack[walker->depth++] = 0;
          break;
        case CMD_CD_PARENT:
          if (walker->depth <= 0)  {
            fprintf(stderr, "Error: Expected parent directory\n");
            return -1;
          }
          *size = walker->stack[--walker->depth];
          if (walker->depth > 0) {
            walker->stack[walker->depth-1] += *size;
          }
          return 1;
        case CMD_CD_CHILD:
          if (walker->depth >= depth_limit) {
            fprintf(stderr, "Error: Expected at most %d nested directories.\n", depth_limit);
            return -1;
          }
          walker->stack[walker->depth++] = 0;
          break;
        case CMD_LS:
        case DIR_ID:
          break;
        default: // file size
          if (walker->depth <= 0) {
            fprintf(stderr, "Error: File without directory.\n");
            return -1;
          }
          walker->stack[walker->depth-1] += info;
          break;
      }
  }

  if (walker->depth > 0) {
    *size = walker->stack[--walker->depth];
    if (walker->depth > 0) {
      walker->stack[walker->depth-1] += *size;
    }
    return 1;
  }

  return 0;
}

static int run_01() {
  const long MAX_SIZE = 100000;
  long score = 0;

  int res;
  long size;
  struct dir_walker walker = { 0 };
  while ((res = walk_directory(&walker, &size)) != 0) {
    if (res < 0) {
      return EXIT_FAILURE;
    }
    if (size <= MAX_SIZE) {
      score += size;
    }
  }

  printf("%ld\n", score);
  return EXIT_SUCCESS;
}

static int run_02() {
  const int MAX_DIRS = 256;
  int dir_count = 0;
  long dir_sizes[MAX_DIRS];

  int res;
  long size;
  struct dir_walker walker = { 0 };
  while ((res = walk_directory(&walker, &size)) != 0) {
    if (res < 0) {
      return EXIT_FAILURE;
    }
    if (dir_count >= MAX_DIRS) {
      fprintf(stderr, "Error: Expected at most %d directories.\n", MAX_DIRS);
      return EXIT_FAILURE;
    }
    dir_sizes[dir_count++] = size;
  }

  const long TOTAL_SIZE = 70000000;
  const long REQUIRED_SIZE = 30000000;
  long score = 0;

  if (dir_count > 0) {
    long total = dir_sizes[dir_count - 1];
    long needed = REQUIRED_SIZE - (TOTAL_SIZE - total);
    if (needed > 0) {
      long best = REQUIRED_SIZE;
      for(int i=0; i<dir_count; i++) {
        if ((dir_sizes[i] >= needed) & (dir_sizes[i] < best)) {
          best = dir_sizes[i];
        }
      }
      score = best;
    }
  }

  printf("%ld\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_07(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_07
#include "main.c"
