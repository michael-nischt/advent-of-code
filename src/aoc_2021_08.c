#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct entry {
  uint8_t signals[10];
  uint8_t digits[4];
} entry_t;

inline static int count_bits8(int8_t val) {
#if __has_builtin(__builtin_popcount)
  return __builtin_popcount(val);
#else
  int n = 0;
  for (int i = 0; i < 8; i++) {
    n += ((val >> i) & 1) == 1;
  }
  return n;
#endif
}

inline static void swap_signal7_bits(uint8_t signal[10], int i, int j) {
  uint8_t tmp = signal[i];
  signal[i] = signal[j];
  signal[j] = tmp;
}

static int match_signal7(uint8_t signals[10], bool(predicate)(uint8_t, uint8_t[10])) {
  for (int i = 0; i < 10; i++) {
    if (predicate(signals[i], signals)) {
      return i;
    }
  }
  return -1;
}

inline static bool is_signal7_match_1(uint8_t code, uint8_t digits[10]) { return count_bits8(code) == 2; }
inline static bool is_signal7_match_7(uint8_t code, uint8_t digits[10]) { return count_bits8(code) == 3; }
inline static bool is_signal7_match_4(uint8_t code, uint8_t digits[10]) { return count_bits8(code) == 4; }
inline static bool is_signal7_match_8(uint8_t code, uint8_t digits[10]) { return count_bits8(code) == 7; }

inline static bool is_signal7_match_3(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 5) & ((code & digits[7]) == digits[7]);
}
inline static bool is_signal7_match_6(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 6) & ((code & digits[1]) != digits[1]);
}

inline static bool is_signal7_match_5(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 5) & ((code & digits[6]) == code);
}
inline static bool is_signal7_match_9(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 6) & ((code & digits[3]) == digits[3]);
}

inline static bool is_signal7_match_2(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 5) & (code != digits[3]) & (code != digits[5]);
}
inline static bool is_signal7_match_0(uint8_t code, uint8_t digits[10]) {
  return (count_bits8(code) == 6) & (code != digits[6]) & (code != digits[9]);
}

static bool decode_signal7(uint8_t signals[10]) {
  // decode 1,4,7,8 based on bit counts
  // decode 3, 6 based on bit counts and previous matches
  // decode 5, 9 based on bit counts and previous matches
  // decode 2, 0 based on bit counts and previous matches
  static const int bits[] = {1, 4, 7, 8, 3, 6, 5, 9, 2, 0};
  static const bool (*is_bit_match[])(uint8_t, uint8_t[10]) = {
      is_signal7_match_1, is_signal7_match_4, is_signal7_match_7, is_signal7_match_8, is_signal7_match_3,
      is_signal7_match_6, is_signal7_match_5, is_signal7_match_9, is_signal7_match_2, is_signal7_match_0,
  };

  int match;
  for (int i = 0; i < 9; i++) { // last one is not needed
    if ((match = match_signal7(signals, is_bit_match[i])) < 0) {
      return false;
    }
    swap_signal7_bits(signals, bits[i], match);
  }
  return true;
}

static bool parse_signal7(const char *digit, uint8_t *bits) { // 0gfedcba
  *bits = 0;
  for (int i = 0; digit[i] != '\0'; i++) {
    if (i >= 7) {
      return false;
    }
    if ((digit[i] < 'a') | (digit[i] > 'g')) {
      return false;
    }
    int bit = digit[i] - 'a';
    *bits |= 1 << bit;
  }
  return true;
}

static int read_entry(entry_t *entry) {
  char signals[10][8];
  char digits[4][8];
  int read = scanf("%7s %7s %7s %7s %7s %7s %7s %7s %7s %7s | %7s %7s %7s %7s", signals[0], signals[1], signals[2],
                   signals[3], signals[4], signals[5], signals[6], signals[7], signals[8], signals[9], digits[0],
                   digits[1], digits[2], digits[3]);
  if (read == EOF) {
    return 0;
  }
  if (read != 14) {
    fprintf(stderr, "Error: 10 signal & 4 digits sequence format error\n");
    return -1;
  }
  for (int i = 0; i < 10; i++) {
    if (!parse_signal7(signals[i], &entry->signals[i])) {
      fprintf(stderr, "Error: invalid 7-bit signal\n");
      return -1;
    }
  }
  for (int i = 0; i < 4; i++) {
    if (!parse_signal7(digits[i], &entry->digits[i])) {
      fprintf(stderr, "Error: invalid 7-bit digit\n");
      return -1;
    }
  }
  return +1;
}

static int run_01() {
  entry_t entry;
  int sum = 0, read;
  while ((read = read_entry(&entry)) > 0) {
    // sum of 1, 4, 7, 8 digits in the output values;
    for (int i = 0; i < 4; i++) {
      int bits = count_bits8(entry.digits[i]);
      sum += (bits == 2) | (bits == 3) | (bits == 4) | (bits == 7);
    }
  }
  if (read < 0) {
    return EXIT_FAILURE;
  }
  printf("%d\n", sum);
  return EXIT_SUCCESS;
}

static int run_02() {
  entry_t entry;
  long sum = 0;
  int read;
  while ((read = read_entry(&entry)) > 0) {
    if (!decode_signal7(entry.signals)) {
      fprintf(stderr, "Error: failed to decode signal7\n");
      return EXIT_FAILURE;
    }

    for (int i = 0; i < 10; i++) {
      sum += (entry.signals[i] == entry.digits[0]) * i * 1000;
      sum += (entry.signals[i] == entry.digits[1]) * i * 100;
      sum += (entry.signals[i] == entry.digits[2]) * i * 10;
      sum += (entry.signals[i] == entry.digits[3]) * i * 1;
    }
  }
  if (read < 0) {
    return EXIT_FAILURE;
  }
  printf("%ld\n", sum);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_08(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2021_day_08
#include "main.c"
