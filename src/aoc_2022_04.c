#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


static int read_assignments_pair(unsigned int ap[2][2]) {
  int read = scanf("%u-%u,%u-%u\n", &ap[0][0], &ap[0][1], &ap[1][0], &ap[1][1]);
  if (read == EOF) {
    return 0;
  }
  if (read != 4) {
    fprintf(stderr, "Error: expected assignments_pair in the form `a-b,c-d`\n");
    return -1;
  }
  return 1;
}

static int run_01() {
  unsigned int ap[2][2];
  int result;
  int score = 0;

  while ((result = read_assignments_pair(ap)) != 0) {
    if (result < 0) {
      return EXIT_FAILURE;
    }
    // either is fully contained within the other
    score += ((ap[0][0] <= ap[1][0]) & (ap[0][1] >= ap[1][1])) |
             ((ap[0][0] >= ap[1][0]) & (ap[0][1] <= ap[1][1]));
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02() {
  unsigned int ap[2][2];
  int result;
  int score = 0;

  while ((result = read_assignments_pair(ap)) != 0) {
    if (result < 0) {
      return EXIT_FAILURE;
    }
    // overlap
    score += ((ap[0][1] >= ap[1][0]) & (ap[0][0] <= ap[1][1]));
  }

  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_04(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_04
#include "main.c"
