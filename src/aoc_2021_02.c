#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int run_01() {
  int position = 0, depth = 0;

  char direction[8];
  int amount, read;
  while ((read = scanf("%7s %d\n", direction, &amount)) != EOF) {
    if (read != 2) {
      fprintf(stderr, "Error: invalid command'\n");
      return EXIT_FAILURE;
    }
    if (strcmp("up", direction) == 0) {
      depth -= amount;
    } else if (strcmp("down", direction) == 0) {
      depth += amount;
    } else if (strcmp("forward", direction) == 0) {
      position += amount;
    } else {
      fprintf(stderr, "Error: invalid direction '%s'\n", direction);
      return EXIT_FAILURE;
    }
  }

  printf("%d\n", position * depth);
  return EXIT_SUCCESS;
}

static int run_02() {
  int position = 0, depth = 0, aim = 0;

  char direction[8];
  int amount, read;
  while ((read = scanf("%7s %d\n", direction, &amount)) != EOF) {
    if (read != 2) {
      fprintf(stderr, "Error: expected direction string and amount integer\n");
      return EXIT_FAILURE;
    }
    if (strcmp("up", direction) == 0) {
      aim -= amount;
    } else if (strcmp("down", direction) == 0) {
      aim += amount;
    } else if (strcmp("forward", direction) == 0) {
      position += amount;
      depth += aim * amount;
    } else {
      fprintf(stderr, "Error: invalid direction '%s'\n", direction);
      return EXIT_FAILURE;
    }
  }

  printf("%d\n", position * depth);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_02(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2021_day_02
#include "main.c"
