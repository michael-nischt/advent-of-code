#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static int read_instruction(int *add) {
  int N = 16;
  char line[N+1];
  line[N] = 0;
  if (fgets(line, N, stdin) == NULL) {
    return 0;
  }

  if (strncmp(line, "noop", 4) == 0) {
    *add = 0;
    return 1;
  }
  if (sscanf(line, "addx %d", add) == 1) {
    return 1;
  }

  fprintf(stderr, "Error: unsupported instruction.\n");
  return -1;
}

static int run_01() {
  const int FREQ = 40;
  int measure = 20;

  long int sum = 0, val = 1;

  int run = 0, add, read;
  while ((read = read_instruction(&add)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    long int sig = val * measure;

    val += add;
    int cycles = 1 + (add != 0);
    run += cycles;

    if (run >= measure) {
      measure += FREQ;
      sum += sig;
    }
  }

  printf("%ld\n", sum);
  return EXIT_SUCCESS;
}


static int run_02() {
  const int W = 40, H = 6;
  char img[W*H];
  memset(img, '.', sizeof(img));

  long int val = 1;

  int run = 0, add, read;
  while ((read = read_instruction(&add)) != 0) {
    if (read < 0) {
      return EXIT_FAILURE;
    }

    int cycles = 1 + (add != 0);

    for(int c=0; c<cycles; c++) {
      int x = run % W;
      int pixel = run % (W*H);

      if (abs(val - x) <= 1) {
        img[pixel] = '#';
      }

      run++;
    }

    val += add;
  }

  // draw
  for(int i=0, y=0; y<H; y++) {
    for(int x=0; x<W; x++, i++) {
      printf("%c", img[i]);
    }
    printf("\n");
  }
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_10(bool first) { return first ? run_01() : run_02(); }

#define AOC_RUN_DAY aoc_run_2022_day_10
#include "main.c"
