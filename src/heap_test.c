#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define HEAP_IMPL
#include "heap.h"

static int cmp_int_ptrs(const void *a, const void *b) {
  int l = *((int *)a), r = *((int *)b);
  return ((l > r) - (l < r));
}

static bool test_heap_update(const int N, const int at) {
  int array[N];

  struct heap heap = {
      .block = sizeof(int),
      .capacity = sizeof(array),
      .memory = array,
      .cmp = cmp_int_ptrs,
  };

  for (int i = 1; i <= N; i++) {
    if (heap_push(&heap, &i) >= heap.size) {
      fprintf(stderr, "Error: buffer limit exceeded\n");
      return false;
    }
  }

  int val = N+1;
  heap_update(&heap, &val, at);

  for (int i = 0; i < N; i++) {
    int value;
    if (!heap_pop(&heap, &value)) {
      return false;
    }
  }

  return true;
}

static int test_heap(const int N) {
  int array[N];

  struct heap heap = {
      .block = sizeof(int),
      .capacity = sizeof(array),
      .memory = array,
      .cmp = cmp_int_ptrs,
  };

  // test ascending
  for (int i = 0; i < N; i++) {
    if (heap_push(&heap, &i) >= heap.size) {
      fprintf(stderr, "Error: buffer limit exceeded\n");
      return false;
    }
  }
  for (int i = 0; i < N; i++) {
    int value;
    if (!heap_pop(&heap, &value)) {
      fprintf(stderr, "Error: heap empty\n");
      return false;
    }
    if (value != i) {
      fprintf(stderr, "Error: expected %d but was %d\n", i, value);
      return false;
    }
  }
  if (heap.size != 0) {
    fprintf(stderr, "Error: heap not empty\n");
    return false;
  }
  // test descending
  for (int i = N - 1; i >= 0; i--) {
    if (heap_push(&heap, &i) >= heap.size) {
      fprintf(stderr, "Error: buffer limit exceeded\n");
      return false;
    }
  }
  for (int i = 0; i < N; i++) {
    int value;
    if (!heap_pop(&heap, &value)) {
      fprintf(stderr, "Error: heap empty\n");
      return false;
    }
    if (value != i) {
      fprintf(stderr, "Error: expected %d but was %d\n", i, value);
      return false;
    }
  }
  if (heap.size != 0) {
    fprintf(stderr, "Error: heap not empty\n");
    return false;
  }

  return true;
}

int main(int argc, char *argv[argc + 1]) {
  int run = 0, success = 0;

  run++;
  success += test_heap(7);

  run++;
  success += test_heap_update(7, 3);

  printf("%d / %d tests succeeded\n", success, run);
  return success == run ? EXIT_SUCCESS : EXIT_FAILURE;
}
