#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef union bingo_board {
  int _5x5[5][5];
  int _25[25];
} bingo_board_t;

static bool mark_bingo_board(bingo_board_t *board, int number) {
  for (int i = 0; i < 25; i++) {
    if (board->_25[i] == number) {
      board->_25[i] = -board->_25[i] - 1; // mark
      return true;
    }
  }
  return false;
}

static int score_bingo_board(bingo_board_t *board, int number) {
  int sum = 0;
  for (int i = 0; i < 25; i++) {
    if (board->_25[i] >= 0) {
      sum += board->_25[i];
    }
  }
  return number * sum;
}

static bool is_bingo_board_winner(bingo_board_t *board) {
  for (int r = 0; r < 5; r++) {
    int count = 0;
    for (int c = 0; c < 5; c++) {
      count += board->_5x5[r][c] < 0;
    }
    if (count == 5) {
      return true;
    }
  }
  for (int c = 0; c < 5; c++) {
    int count = 0;
    for (int r = 0; r < 5; r++) {
      count += board->_5x5[r][c] < 0;
    }
    if (count == 5) {
      return true;
    }
  }
  return false;
}

static int find_bingo_winner_score(int len_sequence, const int sequence[len_sequence], int num_boards,
                                   bingo_board_t boards[num_boards], int winner) {
  for (int i_seq = 0; i_seq < len_sequence; i_seq++) {
    int number = sequence[i_seq];

    for (int i_board = 0; i_board < num_boards; i_board++) {
      bingo_board_t *board = &boards[i_board];

      if (!mark_bingo_board(board, number)) {
        continue; // skip, nothing changed
      }

      if (!is_bingo_board_winner(board)) {
        continue;
      }

      if (num_boards > 1 && winner-- > 0) {
        bingo_board_t tmp = boards[i_board];
        boards[i_board--] = boards[--num_boards];
        boards[num_boards] = tmp;
        continue;
      }
      return score_bingo_board(board, number);
    }
  }

  return 0; // nobody wins -> no score
}

static int read_bingo_numbers(int max_sequence, int sequence[max_sequence]) {
  int len_sequence = 0;

  int number;
  char sep;
  while (scanf("%d", &number) == 1) {
    if (len_sequence >= max_sequence) {
      fprintf(stderr, "Error: bingo number limit exceeded\n");
      return -1;
    }
    sequence[len_sequence++] = number;
    if ((scanf("%c", &sep) != 1) || (sep != ',')) {
      if (isspace(sep)) {
        break;
      }
      fprintf(stderr, "Error: bingo number sequence format error\n");
      return -1;
    }
  }

  return len_sequence;
}

static int read_bingo_boards(int max_boards, bingo_board_t boards[max_boards]) {
  int num_boards = 0;

  bingo_board_t board;
  while (true) {
    int read = scanf("%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d\n%d %d %d %d %d",
                     &board._5x5[0][0], &board._5x5[0][1], &board._5x5[0][2], &board._5x5[0][3], &board._5x5[0][4],
                     &board._5x5[1][0], &board._5x5[1][1], &board._5x5[1][2], &board._5x5[1][3], &board._5x5[1][4],
                     &board._5x5[2][0], &board._5x5[2][1], &board._5x5[2][2], &board._5x5[2][3], &board._5x5[2][4],
                     &board._5x5[3][0], &board._5x5[3][1], &board._5x5[3][2], &board._5x5[3][3], &board._5x5[3][4],
                     &board._5x5[4][0], &board._5x5[4][1], &board._5x5[4][2], &board._5x5[4][3], &board._5x5[4][4]);
    if (read == EOF) {
      return num_boards;
    }
    if (read != 25) {
      fprintf(stderr, "Error: bingo board must be 5x5 numbers\n");
      return -1;
    }
    if (num_boards >= max_boards) {
      fprintf(stderr, "Error: bingo board limit exceeded\n");
      return -1;
    }
    boards[num_boards++] = board;
  }

  return num_boards;
}

static int run_01(int len_sequence, int sequence[len_sequence], int num_boards, bingo_board_t boards[num_boards]) {
  if ((len_sequence = read_bingo_numbers(len_sequence, sequence)) < 0) {
    return EXIT_FAILURE;
  }
  if ((num_boards = read_bingo_boards(num_boards, boards)) < 0) {
    return EXIT_FAILURE;
  }
  int score = find_bingo_winner_score(len_sequence, sequence, num_boards, boards, 0 /*first winner*/);
  printf("%d\n", score);
  return EXIT_SUCCESS;
}

static int run_02(int len_sequence, int sequence[len_sequence], int num_boards, bingo_board_t boards[num_boards]) {
  if ((len_sequence = read_bingo_numbers(len_sequence, sequence)) < 0) {
    return EXIT_FAILURE;
  }
  if ((num_boards = read_bingo_boards(num_boards, boards)) < 0) {
    return EXIT_FAILURE;
  }

  int score = find_bingo_winner_score(len_sequence, sequence, num_boards, boards, num_boards - 1 /*last winner*/);
  printf("%d\n", score);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_04(bool first) {
  const int MAX_SEQUENCE = 128;
  const int MAX_BOARDS = 256;
  int sequence[MAX_SEQUENCE];       // 512 B
  bingo_board_t boards[MAX_BOARDS]; // 25 KB

  return first ? run_01(MAX_SEQUENCE, sequence, MAX_BOARDS, boards)
               : run_02(MAX_SEQUENCE, sequence, MAX_BOARDS, boards);
}

#define AOC_RUN_DAY aoc_run_2021_day_04
#include "main.c"
