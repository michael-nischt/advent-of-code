#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void print_graph_edges(int num_vertices, int edge_counts[1 + num_vertices], int edges[edge_counts[num_vertices]],
                              char labels[num_vertices][8]) {
  for (int from = 0; from < num_vertices; from++) {
    for (int n = edge_counts[from + 1], i = edge_counts[from]; i < n; i++) {
      int to = edges[i];
      if (from < to) {
        printf("%s -> %s\n", labels[from], labels[to]);
      }
    }
  }
}

static int count_graph_paths(int num_vertices, const int edge_counts[1 + num_vertices],
                             const int edges[edge_counts[num_vertices]], int visited[num_vertices], int start, int end,
                             int from, int special) {
  if (from == end) {
    return 1;
  }

  int new_special = special;

  if (visited[from] > 0) {
    if ((new_special >= 0) | (from == start)) {
      return 0; // already visited small cave once or special twice
    }
    new_special = from;
  }
  visited[from] += (visited[from] >= 0);

  int paths = 0;

  for (int n = edge_counts[from + 1], i = edge_counts[from]; i < n; i++) {
    paths += count_graph_paths(num_vertices, edge_counts, edges, visited, start, end, edges[i], new_special);
  }

  visited[from] -= (visited[from] > 0);

  return paths;
}

static int find_graph_label(int n, char labels[n][8], const char *s) {
  for (int i = 0; i < n; i++) {
    if (strcmp(s, labels[i]) == 0) {
      return i;
    }
  }
  return n;
}

static int read_graph_edges(int max_vertices, int edge_counts[1 + max_vertices],
                            int edges[(max_vertices * (max_vertices + 1)) / 2], char labels[max_vertices][8],
                            int *start, int *end) {
  memset(edge_counts, 0, sizeof(int) * (max_vertices + 1));

  char from[8], to[8];
  int num_vertices = 0, i, j, read;
  while ((read = scanf("%7[^-]-%7s\n", from, to)) != EOF) {
    if (read != 2) {
      fprintf(stderr, "Error: invalid connection\n");
      return -1;
    }

    i = find_graph_label(num_vertices, labels, from);
    if (i == num_vertices) {
      if (num_vertices >= max_vertices) {
        fprintf(stderr, "Error: cave limit exceeded\n");
        return -1;
      }
      strcpy(labels[num_vertices++], from);
    }
    j = find_graph_label(num_vertices, labels, to);
    if (j == num_vertices) {
      if (num_vertices >= max_vertices) {
        fprintf(stderr, "Error: cave limit exceeded\n");
        return -1;
      }
      strcpy(labels[num_vertices++], to);
    }

    edges[max_vertices * i + edge_counts[i + 1]++] = j;
    edges[max_vertices * j + edge_counts[j + 1]++] = i;
  }

  // compact edges & accumulate counts
  for (int i = 0; i < num_vertices; i++) {
    int count = edge_counts[i + 1];
    for (int j = 0; j < count; j++) {
      edges[edge_counts[i] + j] = edges[max_vertices * i + j];
    }
    edge_counts[i + 1] += edge_counts[i];
  }

  if ((*start = find_graph_label(num_vertices, labels, "start")) == num_vertices) {
    fprintf(stderr, "Error: missing start cave'\n");
    return -1;
  }
  if ((*end = find_graph_label(num_vertices, labels, "end")) == num_vertices) {
    fprintf(stderr, "Error: missing end cave\n");
    return -1;
  }
  return num_vertices;
}

static int run_01(int num_vertices, int edge_counts[1 + num_vertices],
                  int edges[(num_vertices * (num_vertices + 1)) / 2], int visited[num_vertices],
                  char labels[num_vertices][8]) {
  int start, end;
  if ((num_vertices = read_graph_edges(num_vertices, edge_counts, edges, labels, &start, &end)) < 0) {
    return EXIT_FAILURE;
  }

  // mark big-caves to allow unlimited visits
  for (int i = 0; i < num_vertices; i++) {
    visited[i] = -1 * ((labels[i][0] >= 'A') & (labels[i][0] <= 'Z')); // big-cave: -1, small-cave: 0
  }

  // no caves are special and visited twice (num_vertices doesn't match any)
  int paths = count_graph_paths(num_vertices, edge_counts, edges, visited, start, end, start, num_vertices);
  printf("%d\n", paths);
  return EXIT_SUCCESS;
}

static int run_02(int num_vertices, int edge_counts[1 + num_vertices],
                  int edges[(num_vertices * (num_vertices + 1)) / 2], int visited[num_vertices],
                  char labels[num_vertices][8]) {
  int start, end;
  if ((num_vertices = read_graph_edges(num_vertices, edge_counts, edges, labels, &start, &end)) < 0) {
    return EXIT_FAILURE;
  }

  // mark big-caves to allow unlimited visits
  for (int i = 0; i < num_vertices; i++) {
    visited[i] = -1 * ((labels[i][0] >= 'A') & (labels[i][0] <= 'Z')); // big-cave: -1, small-cave: 0
  }

  // big caves are special and visited twice
  int paths = count_graph_paths(num_vertices, edge_counts, edges, visited, start, end, start, -1);
  printf("%d\n", paths);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_12(bool first) {
  const int MAX_VERTICES = 32;
  int edge_counts[1 + MAX_VERTICES];                  // 132 B
  int edges[(MAX_VERTICES * (MAX_VERTICES + 1)) / 2]; // 2112 B
  int visited[MAX_VERTICES];                          // 128 B
  char labels[MAX_VERTICES][8];                       // 1 KB

  return first ? run_01(MAX_VERTICES, edge_counts, edges, visited, labels)
               : run_02(MAX_VERTICES, edge_counts, edges, visited, labels);
}

#define AOC_RUN_DAY aoc_run_2021_day_12
#include "main.c"
