#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UNIQ_IMPL
#include "uniq.h"

typedef union vec2 {
  struct {
    int x, y;
  };
  int vec[2];
} vec2_t;

static int print_sorted_dots(int num_dots, const vec2_t dots[num_dots], vec2_t size) {
  int i = 0;
  for (int y = 0; y < size.y; y++) {
    for (int x = 0; x < size.x; x++) {
      int dot = (i < num_dots) && ((dots[i].x == x) & (dots[i].y == y));
      i += dot;
      printf("%c ", dot ? '#' : '.');
    }
    printf("\n");
  }
}
inline static int cmp_coords(const vec2_t *a, const vec2_t *b) {
  return ((a->y > b->y) - (a->y < b->y)) * 10 + ((a->x > b->x) - (a->x < b->x));
}
inline static int cmp_coord_ptrs(const void *a, const void *b) { return cmp_coords(a, b); }

static int fold_paper(int n, vec2_t dots[n], vec2_t *size, int axis, int at) {
  for (int i = 0; i < n; i++) {
    if (dots[i].vec[axis] >= at) {
      dots[i].vec[axis] = 2 * at - dots[i].vec[axis];
    }
  }
  qsort(dots, n, sizeof(vec2_t), cmp_coord_ptrs);
  n = suniq(dots, n, sizeof(vec2_t));
  size->vec[axis] /= 2;
  return n;
}

static int read_paper(int max_dots, vec2_t dots[max_dots], vec2_t *size) {
  int num_dots = 0;
  *size = (vec2_t){.x = 0, .y = 0};

  vec2_t dot;
  int read;
  while ((read = scanf("%d,%d", &dot.x, &dot.y)) == 2) {
    if (num_dots >= max_dots) {
      fprintf(stderr, "Error: dot limit exceeded\n");
      return -1;
    }
    dots[num_dots++] = dot;
    size->x = size->x * (dot.x < size->x) + dot.x * (size->x < dot.x);
    size->y = size->y * (dot.y < size->y) + dot.y * (size->y < dot.y);
  }

  return num_dots;
}

static int read_fold(int *axis, int *at) {
  char c;
  int read = scanf("fold along %c=%d\n", &c, at);
  if (read == EOF) {
    return 0;
  }
  if (read != 2) {
    fprintf(stderr, "Error: invalid fold format\n");
    return -1;
  }
  if ((c != 'x') & (c != 'y')) {
    fprintf(stderr, "Error: invalid fold axis\n");
    return -1;
  }
  *axis = c - 'x';
  return 1;
}

static int run_01(int num_dots, vec2_t dots[num_dots]) {
  vec2_t size;
  if ((num_dots = read_paper(num_dots, dots, &size)) < 0) {
    return EXIT_FAILURE;
  }

  int axis, at;
  int read;
  if ((read = read_fold(&axis, &at)) > 0) {
    num_dots = fold_paper(num_dots, dots, &size, axis, at);
  }
  if (read < 0) {
    return EXIT_FAILURE;
  }
  printf("%d\n", num_dots);
  return EXIT_SUCCESS;
}

static int run_02(int num_dots, vec2_t dots[num_dots]) {
  vec2_t size;
  if ((num_dots = read_paper(num_dots, dots, &size)) < 0) {
    return EXIT_FAILURE;
  }

  int axis, at;
  int read;
  while ((read = read_fold(&axis, &at)) > 0) {
    num_dots = fold_paper(num_dots, dots, &size, axis, at);
  }
  if (read < 0) {
    return EXIT_FAILURE;
  }

  print_sorted_dots(num_dots, dots, size);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_13(bool first) {
  const int MAX_DOTS = 1024;
  vec2_t dots[MAX_DOTS]; // 8 KB

  return first ? run_01(MAX_DOTS, dots) : run_02(MAX_DOTS, dots);
}

#define AOC_RUN_DAY aoc_run_2021_day_13
#include "main.c"
