#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define PART_IMPL
#include "part.h"

inline static int cmp_ints(int a, int b) { return (a > b) - (a < b); }
inline static int cmp_int_ptrs(const void *a, const void *b) { return cmp_ints(*((const int *)a), *((const int *)b)); }

static int median(int n, int numbers[n]) {
  // partition numbers such as:
  // all numbers[<k] are leq to numbers[k]
  // all numbers[>k] are qeq to numbers[k]
  int k = (n & 1) ? (n / 2) : (n / 2 - 1);
  qpart(numbers, n, sizeof(int), k, cmp_int_ptrs);
  return numbers[k];
}

static int average(int n, const int numbers[n]) {
  // safe avarage without overflow in case of many large integers
  int average = 0, remainder = 0, q, r;
  for (int i = 0; i < n; i++) {
    q = numbers[i] / n;
    r = numbers[i] % n;
    average += q;
    remainder += r;
    if (remainder >= n) {
      remainder -= n;
      average++;
    }
  }
  return average;
}

static long count_moves_const(int dst, int num_crabs, const int positions[num_crabs]) {
  long moves = 0;
  for (int d, i = 0; i < num_crabs; i++) {
    d = positions[i] - dst;
    d = -d * (d < 0) + d * (d >= 0); // abs(d)
    moves += d;
  }
  return moves;
}

static long count_moves_incr(int dst, int num_crabs, const int positions[num_crabs]) {
  long moves = 0;
  for (int d, i = 0; i < num_crabs; i++) {
    d = positions[i] - dst;
    d = -d * (d < 0) + d * (d >= 0); // abs(d)
    d = (d * (d + 1)) / 2;           // sum_over(d)
    moves += d;
  }
  return moves;
}

static int read_crab_positions(int max_crabs, int positions[max_crabs]) {
  int num_crabs = 0;

  char sep;
  int number, read;
  while ((read = scanf("%d", &number)) != EOF) {
    if (read != 1) {
      fprintf(stderr, "Error: crab position integer expected\n");
      return -1;
    }
    if (num_crabs >= max_crabs) {
      fprintf(stderr, "Error: crab position limit exceeded\n");
      return -1;
    }
    positions[num_crabs++] = number;
    if ((scanf("%c", &sep) != 1) || (sep != ',')) {
      if (isspace(sep)) {
        break;
      }
      fprintf(stderr, "Error: crab position sequence format error\n");
      return -1;
    }
  }

  return num_crabs;
}

static int run_01(int num_crabs, int positions[num_crabs]) {
  if ((num_crabs = read_crab_positions(num_crabs, positions)) < 0) {
    return EXIT_FAILURE;
  }

  int med = median(num_crabs, positions);
  int moves = count_moves_const(med, num_crabs, positions);
  printf("%d\n", moves);
  return EXIT_SUCCESS;
}

static int run_02(int num_crabs, int positions[num_crabs]) {
  if ((num_crabs = read_crab_positions(num_crabs, positions)) < 0) {
    return EXIT_FAILURE;
  }

  int min = INT_MAX, max = INT_MIN;
  for (int i = 0; i < num_crabs; i++) {
    if (positions[i] < min) {
      min = positions[i];
    }
    if (positions[i] > max) {
      max = positions[i];
    }
  }
  int avg = average(num_crabs, positions);

  // avarage should be closer the global minimum than any local (proof?)
  const bool GLOBAL = false; // search for global minimum or just downhill (local)

  bool left = true, right = true;

  long moves = count_moves_incr(avg, num_crabs, positions), next;
  for (int l, r, i = 1; left | right; i++) {
    l = avg - i;
    r = avg + i;
    if ((l < 0) | (r >= num_crabs)) {
      break;
    }
    if (left & (min <= l)) {
      next = count_moves_incr(l, num_crabs, positions);
      if (next < moves) {
        moves = next;
      } else {
        left = GLOBAL;
      }
    }
    left &= r < num_crabs;
    if (right & (r <= max)) {
      next = count_moves_incr(r, num_crabs, positions);
      if (next < moves) {
        moves = next;
      } else {
        right = GLOBAL;
      }
    }
  }

  printf("%ld\n", moves);
  return EXIT_SUCCESS;
}

int aoc_run_2021_day_07(bool first) {
  const int MAX_CRABS = 1024;
  int positions[MAX_CRABS]; // 4 KB

  return first ? run_01(MAX_CRABS, positions) : run_02(MAX_CRABS, positions);
}

#define AOC_RUN_DAY aoc_run_2021_day_07
#include "main.c"
