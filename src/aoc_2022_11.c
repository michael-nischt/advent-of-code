#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

enum monkey_op {
  MONKEY_OP_INVALID = 0,
  MONKEY_OP_ADD = 1,
  MONKEY_OP_MUL = 2,
};

struct monkey_info {
  // inventory
  int items;
  long worry_levels[32];
  // adaptation
  enum monkey_op op;
  int arg; // use 'old' value if zero
  // decision
  int test, risk, safe;
  // inspection
  int inspections;
};

static int worry_levels(const char *str, int max, long values[static max]) {
  int n = 0;

  const char *start = str;
  char *end = NULL;
  long val;
  while(n < max) {
    // parse value
    val = strtoul(start, &end, 10);
    if ((val == 0) & str[0] != '0') {
      break;
    }
    if (val < 0) {
      return -1; // to large value
    }
    start = end;
    // store value
    if (n >= max) {
      return -1;
    }
    values[n++] = val;
    // more are separated by ','
    start = strchr(start, ',');
    if (start++ == NULL) {
      break;
    }
  }
  return n;
}

static int read_monkey_infos(int max_monkeys, struct monkey_info monkeys[static max_monkeys]) {
  static const int N = 63;
  char buffer[N + 1];
  buffer[N] = 63;

  struct monkey_info *monkey = NULL;
  int num_monkeys = 0, arg;

  char *line;
  while ((line = fgets(buffer, N, stdin)) != NULL) {
    // trim start and to safe empty line
    while(isspace(*line)) {
      line++;
    }
    if (line[0] == 0) {
      continue;
    }
    // new monkey
    if (sscanf(line, "Monkey %u:", (unsigned int*) &arg) == 1) {
      if (arg >= max_monkeys) {
        fprintf(stderr, "Error: expected at most %d monkeys.\n", max_monkeys);
        return -1;
      }
      if (arg != num_monkeys) {
        fprintf(stderr, "Error: expected monkeys listed in order.\n");
        return -1;
      }
      num_monkeys++;
      monkey = &monkeys[arg];
      *monkey = (struct monkey_info) { 0 }; // clear
    }
    // inventory
    if ((line = fgets(buffer, N, stdin)) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected starting items.\n");
        return -1;
    }
    if ((line = strstr(line, "Starting items:")) == NULL) {
        fprintf(stderr, "Error: invalid monkey info; expected starting items.\n");
        return -1;
    }
    line += sizeof("Starting items:");

    int max = sizeof(monkey->worry_levels)/sizeof(monkey->worry_levels[0]);
    monkey->items = worry_levels(line, max, monkey->worry_levels);
    if (monkey->items < 0) {
        fprintf(stderr, "Error: invalid monkey info; too many starting items.\n");
        return -1;
    }
    // operation
    if ((line = fgets(buffer, N, stdin)) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected operation.\n");
        return -1;
    }
    if ((line = strstr(line, "Operation:")) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected operation.\n");
        return -1;
    }
    line += sizeof("Operation:");
    if (strstr(line, "new = old + old") != NULL) {
      monkey->op = MONKEY_OP_ADD;
      monkey->arg = 0;
    } else if (strstr(line, "new = old * old") != NULL) {
      monkey->op = MONKEY_OP_MUL;
      monkey->arg = 0;
    } else if (sscanf(line, "new = old + %d\n", &arg) == 1) {
      monkey->op = MONKEY_OP_ADD;
      monkey->arg = arg;
    } else if (sscanf(line, "new = old * %d\n", &arg) == 1) {
      monkey->op = MONKEY_OP_MUL;
      monkey->arg = arg;
    } else {
        fprintf(stderr, "Error: invalid monkey info; unsupported operation.\n'%s'", line);
        return -1;
    }
    // test
    if ((line = fgets(buffer, N, stdin)) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected test.\n");
        return -1;
    }
    if ((line = strstr(line, "Test:")) == NULL) {
        fprintf(stderr, "Error: invalid monkey info; expected test.\n");
        return -1;
    }
    line += sizeof("Test:");
    if (sscanf(line, "divisible by %d\n", &arg) != 1) {
        fprintf(stderr, "Error: invalid monkey info; unsupported test.\n");
        return -1;
    }
    monkey->test = arg;
    // if true: risk
    if ((line = fgets(buffer, N, stdin)) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected true case.\n");
        return -1;
    }
    if ((line = strstr(line, "If true:")) == NULL) {
        fprintf(stderr, "Error: invalid monkey info; expected true case.\n");
        return -1;
    }
    line += sizeof("If true:");
    if (sscanf(line, "throw to monkey %d\n", &arg) != 1) {
        fprintf(stderr, "Error: invalid monkey info; unsupported true case.\n");
        return -1;
    }
    monkey->risk = arg;
    // if false: safe
    if ((line = fgets(buffer, N, stdin)) == NULL) {
        fprintf(stderr, "Error: incomplete monkey info; expected false case.\n");
        return -1;
    }
    if ((line = strstr(line, "If false:")) == NULL) {
        fprintf(stderr, "Error: invalid monkey info; expected false case.\n");
        return -1;
    }
    line += sizeof("If false:");
    if (sscanf(line, "throw to monkey %d\n", &arg) != 1) {
        fprintf(stderr, "Error: invalid monkey info; unsupported false case.\n");
        return -1;
    }
    monkey->safe = arg;
  }

  return num_monkeys;
}

static int run(int rounds, int relief) {
  struct monkey_info monkeys[8];

  int num_monkeys = read_monkey_infos(sizeof(monkeys)/sizeof(monkeys[0]), monkeys);
  if (num_monkeys < 0) {
    return EXIT_FAILURE;
  }

  long mod = 1;
  for(int m=0; m<num_monkeys; m++) {
    mod *= monkeys[m].test;
  }

  for(int r=0; r<rounds; r++) {
    for(int m=0; m<num_monkeys; m++) {
      // update worry levels
      for(int i=0; i<monkeys[m].items; i++) {
        // stress
        int arg = monkeys[m].arg != 0 ? monkeys[m].arg : monkeys[m].worry_levels[i];
        if (monkeys[m].op == MONKEY_OP_ADD) {
          monkeys[m].worry_levels[i] += arg;
        } else if (monkeys[m].op == MONKEY_OP_MUL) {
          monkeys[m].worry_levels[i] *= arg;
        } else {
          return EXIT_FAILURE;
        }
        // relief
        monkeys[m].worry_levels[i] /= relief;
        // shared test bounds
        monkeys[m].worry_levels[i] %= mod;
      }
      // throws
      for(int i=0; i<monkeys[m].items; i++) {
        int target = ((monkeys[m].worry_levels[i] % monkeys[m].test) == 0)
                   ? monkeys[m].risk : monkeys[m].safe;
        monkeys[target].worry_levels[monkeys[target].items++] = monkeys[m].worry_levels[i];
        monkeys[m].inspections++;
      }
      monkeys[m].items = 0;
    }
  }

  long max[2] = { 0, 0 };
  for(int m=0; m<num_monkeys; m++) {
    if (monkeys[m].inspections > max[0]) {
      max[1] = max[0];
      max[0] = monkeys[m].inspections;
    } else if (monkeys[m].inspections > max[1]) {
      max[1] = monkeys[m].inspections;
    }
  }

  printf("%ld\n", max[0]*max[1]);
  return EXIT_SUCCESS;
}

int aoc_run_2022_day_11(bool first) { return first ? run(20, 3) : run(10000, 1); }

#define AOC_RUN_DAY aoc_run_2022_day_11
#include "main.c"
