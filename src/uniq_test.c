#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define UNIQ_IMPL
#include "uniq.h"


static bool uniq_test_ints(int m, int n, int *items /*[n]*/) {
  n = suniq(items, n, sizeof(int));
  if (n != m) {
    return false;
  }
  for(int i=1; i<n; i++) {
    if (items[i-1] == items[i]) {
      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[argc + 1]) {
  int run = 0, success = 0;

  // single element array
  run++;
  success += uniq_test_ints(1, 1, (int[1]){1});

  // odd array length
  run++;
  success += uniq_test_ints(3, 5, (int[7]){3, 3, 3, 4, 6, 6});

  // even array length
  run++;
  success += uniq_test_ints(4, 6, (int[8]){ 1, 3, 3, 3, 4, 6, 6});

  printf("%d / %d tests succeeded\n", success, run);
  return success == run ? EXIT_SUCCESS : EXIT_FAILURE;
}
