#ifndef HEAP_H
#define HEAP_H

#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef HEAP_STATIC
#define HEAP_API static
#else
#define HEAP_API extern
#endif

struct heap {
  size_t size;
  size_t block, capacity;
  void *memory;
  int (*cmp)(const void *, const void *);
};

// O(1)
HEAP_API bool heap_peek(struct heap *heap, void *node);
// O(log(N))
HEAP_API bool heap_pop(struct heap *heap, void *node);
HEAP_API size_t heap_push(struct heap *heap, const void *node);
HEAP_API size_t heap_update(struct heap *heap, void *node, size_t index);

#ifdef HEAP_IMPL

static size_t heap_shift_up(struct heap *heap, size_t index) {
  uint8_t node[heap->block];
  uint8_t* i = (uint8_t*) heap->memory + heap->block * index, *j;
  memcpy(node, i, heap->block);
  while (index > 0) {
    size_t parent = (index - 1) >> 1;
    j = (uint8_t*) heap->memory + heap->block * parent;
    if (heap->cmp(node, j) >= 0) {
      break;
    }
    memcpy(i, j, heap->block);
    index = parent;
    i = (uint8_t*) heap->memory + heap->block * index;
  }
  memcpy((uint8_t*) heap->memory + heap->block * index, node, heap->block);
  return index;
}
static size_t heap_shift_down(struct heap *heap, size_t index) {
  uint8_t node[heap->block];
  uint8_t* i = (uint8_t*) heap->memory + heap->block * index, *j;
  memcpy(node, i, heap->block);

  size_t child = 2 * index + 1;
  while (child < heap->size) {
    j = (uint8_t*) heap->memory + heap->block * child;
    { // choose bigger child if there are two
      size_t other = child + 1;
      if ((other < heap->size) && (heap->cmp(j, j + heap->block) > 0)) {
        child = other;
        j += heap->block;
      }
    }
    if (heap->cmp(node, j) <= 0) {
      break;
    }
    memcpy(i, j, heap->block);
    index = child;
    i = (uint8_t*) heap->memory + heap->block * index;
    child = 2 * child + 1;
  }
  memcpy((uint8_t*) heap->memory + heap->block * index, node, heap->block);
  return index;
}

HEAP_API bool heap_peek(struct heap *heap, void *node) {
  if (heap->size == 0) {
    return false;
  }
  memcpy(node, heap->memory, heap->block);
  return true;
}

HEAP_API bool heap_pop(struct heap *heap, void *node) {
  if (heap->size == 0) {
    return false;
  }
  --heap->size;
  memcpy(node, heap->memory, heap->block);
  // if (heap->size > 0) {
  uint8_t * i = (uint8_t *) heap->memory + heap->block * heap->size;
  memcpy(heap->memory, i, heap->block);
  heap_shift_down(heap, 0);
  // }
  return true;
}

HEAP_API size_t heap_push(struct heap *heap, const void *node) {
  if ((heap->size + 1) * heap->block > heap->capacity) {
    return heap->size;
  }
  size_t index = heap->size++;
  uint8_t * i = (uint8_t *) heap->memory + heap->block * index;
  memcpy(i, node, heap->block);
  return heap_shift_up(heap, index);
}

HEAP_API size_t heap_update(struct heap *heap, void *node, size_t index) {
  uint8_t * i = (uint8_t *) heap->memory + heap->block * index;
  if (memcmp(node, i, heap->block) == 0) {
    return index;
  }
  int cmp = heap->cmp(node, i);
  memcpy(i, node, heap->block);
  if (cmp < 0) {
    return heap_shift_up(heap, index);
  } else if (cmp > 0) {
    return heap_shift_down(heap, index);
  }
  return index;
}

#endif // HEAP_IMPL

#ifdef __cplusplus
}
#endif

#endif // HEAP_H
