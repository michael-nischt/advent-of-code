# Advent of Code

A handmade [Advent](https://adventofcode.com/)(ure) in plain C.

## Motivation

All solutions in modern C. Only depending on [libc](https://en.wikipedia.org/wiki/C_standard_library), the C standard library.

Favoring readability over compactness. Plus a few more restraints:

### 1. Fast Runtime

Goal: Less than 1 second execution time with compiler optimizations enabled.

All solutions run ~10-100x faster on a good notebook CPU, released in in 2013: [i7-4900MQ](https://www.intel.com/content/www/us/en/products/sku/75131/)

More than fast enough to match the [intended solutions](https://adventofcode.com/2021/about):

> every problem has a solution that completes in at most 15 seconds on ten-year-old hardware.

### 2. Fixed Memory Budget

The most allocated memory for a solution is ~8MB. Fitting into the L2 cache of most CPUs; Using [glibc](https://www.gnu.org/software/libc/) even into the main's threads memory stack. Only 64 KB is assumed though for cross-platform compatibility.

Aiming to minimize memory fragmentation and maximize cache coherency:

- No dynamic heap memory allocations; just an initial arena.
- No unnecessary copying data, e.g. (sub-)strings.

### 3. Simple & Minimal

Array based arrangement of data, whenever possible.
So far, no hash-sets or hash-maps. Which are both amazing but often miss- and overused.

## Usage

There are two `make` compilation targets `debug` and `release` (switching requires `clean`) for all days. A single one can build manually, like this example for day 1:

```sh
mkdir bin # create if not yet exists
gcc src/aoc_2021_01.c -o bin/aoc_2021_01
```

Each has day _two_ parts and reads all data `stdin`. The command to running part 1 would be:

```sh
./bin/aoc_2021_01 -part=1 < res/2021_01.txt
```

## Disclaimer

In sum it's a decent amount of code. Chances are there are bugs and some correct but stupid parts. I'm happy to hear about any such and any general ideas on how to improve the code and learn.

## License

All code is released as Public Domain ([www.unlicense.org](https://unlicense.org/)).

## Acknowledgments

Many thanks go to:

- [Eric Wastl](http://was.tl/) for providing the _Advent of Code_ challenge.
- [Rainer Bernhardt](https://www.linkedin.com/in/plucked/) for the motivation boosts.\
 (Sharing great, often unorthodox, solutions shared at the end of each day.)
- [Roman Hwang](https://www.linkedin.com/in/hwangroman) for reminding me how powerful `scanf` is as well as stack memory platform differences.
